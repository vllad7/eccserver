local cfg = {}

cfg.items = {
  ["AK47"] = {"AK47", "Arma de guerra: AK47.", nil, 0.01}, -- no choices
  ["M4A1"] = {"M4A1", "Arma de guerra: M4A1.", nil, 0.01}, -- no choices
  ["Cartão Clonado"] = {"Cartão Clonado", "Cartão de crédito clonado.", nil, 0.01}, -- no choices
  ["Dinheiro Bancário"] = {"Dinheiro bancario", "Dinheiro que pertence ao banco.", nil, 0}, -- no choices
  ["Folha de Coca"] = {"Folha de Coca", "Folha da Cocaína", nill, 0.10}, --no choices
  ["Cannabis"] = {"Cannabis", "Sementes da Maconha.", nil, 0.10}, -- no choices
  ["Pistola Enferrujada"] = {"Pistola Enferrujada", "Essa pistola faz pow pow pow não men.", nil, 0.15}, -- no choices
  ["SMG Enferrujada"] = {"SMG Enferrujada", "Essa SMG faz pow pow pow não men.", nil, 0.15}, -- no choices
  ["Cobre"] = {"Cobre", "", nil, 0.25}, -- no choices
  ["Aço"] = {"Aço", "", nil, 0.25}, -- no choices
  ["Diamante"] = {"Diamante", "", nil, 0.25},
  ["Motor"] = {"Motor", "", nil, 0.25},
  ["Parachoque"] = {"Parachoque", "", nil, 0.25},
  ["Capo"] = {"Capo", "", nil, 0.25},
  ["Rodas"] = {"Rodas", "", nil, 0.25},
  ["Volante"] = {"Volante", "", nil, 0.25},
  ["Retrovisor"] = {"Retrovisor", "", nil, 0.25},
  ["Banco"] = {"Banco", "", nil, 0.01},
  ["tartaruga"] = {"Tartaruga", "", nil, 0.01},
  ["sardinha"] = {"sardinha", "", nil, 0.01},
  ["peixe-gato"] = {"peixe-gato", "", nil, 0.01},
  ["salmao"] = {"salmao", "", nil, 0.01},
  ["corvina"] = {"corvina", "", nil, 0.01},
  ["colete"] = {"Colete", "", nil, 5.00},
  ["GPS"] = {"GPS", "", nil, 0.01}, -- ITEM PARA SCRIPT DE GPS QUE É VENDIDO SEPARADO DA BASE (CASO NÃO TENHA O SCRIPT NÃO É NECESSARIO MANTER)
  ["Carne"] = {"Carne", "", nil, 0.01}, -- ITEM PARA SCRIPT DE GPS QUE É VENDIDO SEPARADO DA BASE (CASO NÃO TENHA O SCRIPT NÃO É NECESSARIO MANTER)
  ["Nitro"] = {"Nitro", "", nil, 0.01} -- ITEM PARA SCRIPT DE GPS QUE É VENDIDO SEPARADO DA BASE (CASO NÃO TENHA O SCRIPT NÃO É NECESSARIO MANTER)
}

-- load more items function
local function load_item_pack(name)
  local items = module("cfg/item/"..name)
  if items then
    for k,v in pairs(items) do
      cfg.items[k] = v
    end
  else

  end
end

-- PACKS
load_item_pack("required")
load_item_pack("food")
load_item_pack("drugs")
load_item_pack("police")

return cfg
