-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- VARIAVEIS LOCAIS DE CONEXÃO COM O VRP
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- EVENTOS SE COMUNICANDO COM O CLIENT
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
RegisterServerEvent("zezao:municaodp")
AddEventHandler("zezao:municaodp", function(item)
    local sourcePlayer = tonumber(source)
    local user_id = vRP.getUserId(source)

        if vRP.hasPermission(user_id, "policial.pegar.municao") then
            if item.name == "municao-pistola-dp" then
                vRP.giveInventoryItem(user_id, "wammo|WEAPON_PISTOL", 12)
                TriggerClientEvent("pNotify:SendNotification", sourcePlayer, {
                    text = "Compra efetuada com sucesso",
                    type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                   })
             end

            if item.name == "municao-pistola-sns-dp" then
                vRP.giveInventoryItem(user_id, "wammo|WEAPON_PISTOL", 6)
                TriggerClientEvent("pNotify:SendNotification", sourcePlayer, {
                    text = "Compra efetuada com sucesso",
                    type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end
  

            if item.name == "municao-12-dp" then
                vRP.giveInventoryItem(user_id, "wammo|WEAPON_PUMPSHOTGUN", 8)
                TriggerClientEvent("pNotify:SendNotification", sourcePlayer, {
                    text = "Compra efetuada com sucesso",
                    type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end

            if item.name == "municao-smg-dp" then
                vRP.giveInventoryItem(user_id, "wammo|WEAPON_SMG", 30)
                TriggerClientEvent("pNotify:SendNotification", sourcePlayer, {
                    text = "Compra efetuada com sucesso",
                    type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end

            if item.name == "municao-AK-47-dp" then
                vRP.giveInventoryItem(user_id, "wammo|WEAPON_ASSAULTRIFLE", 30)
                TriggerClientEvent("pNotify:SendNotification", sourcePlayer, {
                    text = "Compra efetuada com sucesso",
                    type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end

            if item.name == "municao-m4-dp" then
                vRP.giveInventoryItem(user_id, "wammo|WEAPON_ASSAULTRIFLE", 30)
                TriggerClientEvent("pNotify:SendNotification", sourcePlayer, {
                    text = "Compra efetuada com sucesso",
                    type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end
        else
            TriggerClientEvent("pNotify:SendNotification", sourcePlayer, {
                text = "Você não é um policial",
                type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
            })
        end
end)