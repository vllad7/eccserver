local options = {
    x = 0.8, 
    y = 0.080, 
    width = 0.2, 
    height = 0.04, 
    scale = 0.4, 
    font = 0, 
    color_r = 255, 
    color_g = 124, 
    color_b = 173, 
}

function DisplayHelpText(str)
    SetTextComponentFormat("STRING")
    AddTextComponentString(str)
    DisplayHelpTextFromStringLabel(0, 0, 1, -1)
end

function drawTxt(text, font, centre, x, y, scale, r, g, b, a)
    SetTextFont(font)
    SetTextProportional(0)
    SetTextScale(scale, scale)
    SetTextColour(r, g, b, a)
    SetTextDropShadow(0, 0, 0, 0, 255)
    SetTextEdge(1, 0, 0, 0, 255)
    SetTextDropShadow()
    SetTextOutline()
    SetTextCentre(centre)
    SetTextEntry("STRING")
    AddTextComponentString(text)
    DrawText(x, y)
end

function drawTxt2(x, y, width, height, scale, text, r, g, b, a, outline)
    SetTextFont(0)
    SetTextProportional(0)
    SetTextScale(scale, scale)
    SetTextColour(r, g, b, a)
    SetTextDropShadow(0, 0, 0, 0, 255)
    SetTextEdge(1, 0, 0, 0, 255)
    SetTextDropShadow()
    if(outline)then
        SetTextOutline()
    end
    SetTextEntry("STRING")
    AddTextComponentString(text)
    DrawText(x - width / 2, y - height / 2 + 0.005)
end

function Main()
    ClearMenu()
    Menu.addButton("Porte Intermediário", "level2M", nil)
    Menu.addButton("Porte Avançado", "level3M", nil)
    Menu.addButton("Porte Especial", "level4M", nil)
end

function level2M()
    ClearMenu()
    Menu.previous = "Main"
    Menu.addButton("Pistola", "portedois", {name = "municao-pistola"})
    Menu.addButton("Pistola SNS", "portedois", {name = "municao-pistola-sns"})
end

function level3M() 
    ClearMenu()
    Menu.previous = "Main" 
    Menu.addButton("12", "portetres", {name = "municao-12"}) 
    Menu.addButton("Rifle Compacto", "portetres", {name = "municao-rifle-compacto"})
    Menu.addButton("SMG", "portetres", {name = "municao-smg"})
end

function level4M()
    ClearMenu()
    Menu.previous = "Main"
    Menu.addButton("AK-47", "portequatro", {name = "municao-ak-47"})
    Menu.addButton("M4", "portequatro", {name = "municao-m4"})
    Menu.addButton("Sniper", "portequatro", {name = "municao-sniper"})
end 

function portedois(item)
    TriggerServerEvent("zezao:doisM", item)
end

function portetres(item)
    TriggerServerEvent("zezao:tresM", item)
end

function portequatro(item)
    TriggerServerEvent("zezao:quatroM", item)
end
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- MENU PARA COMPRAR
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
municao = {
    {name = "Comprar Munição (Sul)", colour = 1, id = 110, x = 812.50, y = -2153.32, z = 28.64}, 
    {name = "Comprar Munição (Concessionária)", colour = 1, id = 110, x = 18.59, y = -1109.87, z = 28.85}, 
    {name = "Comprar Munição (Posto de Gasolina)", colour = 1, id = 110, x = 844.60, y = -1029.71, z = 27.25}, 
}

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)
        local pos = GetEntityCoords(GetPlayerPed(-1), true)
        for k, v in ipairs(municao) do
            if(Vdist(pos.x, pos.y, pos.z, v.x, v.y, v.z) < 15.0)then
                DrawMarker(27, v.x, v.y, v.z, 0, 0, 0, 0, 0, 0, 0.9, 0.9, 0.6, 255, 124, 173, 255, 0, 0, 0, 0)
                if(Vdist(pos.x, pos.y, pos.z, v.x, v.y, v.z) < 1.2)then
                    DisplayHelpText("Pressione ~INPUT_CONTEXT~ para comprar munição")
                    if IsControlJustReleased(1, 51) then
                        Main()
                        Menu.selection = 1
                        Menu.hidden = not Menu.hidden -- Hide/Show the menu
                    end
                    Menu.renderGUI(options) -- Draw menu on each tick if Menu.hidden = false
                elseif(Vdist(pos.x, pos.y, pos.z, v.x, v.y, v.z) > 1.0)then
                    Menu.hidden = true
                end
            end
        end
    end
end)
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--Help messages
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)
        if IsEntityDead(GetPlayerPed(-1)) then
            Menu.hidden = true
            Citizen.Wait(5000)
        end
    end
end)
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- MENSAGENS
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function TextoDIVTitulo(x, y, width, height, scale, text, r, g, b, a, outline)
    SetTextFont(1)
    SetTextProportional(0)
    SetTextScale(scale, scale)
    SetTextColour(r, g, b, a)
    SetTextDropShadow(0, 0, 0, 0, 255)
    SetTextEdge(1, 0, 0, 0, 255)
    SetTextDropShadow()
    if(outline)then
        SetTextOutline()
    end
    SetTextEntry("STRING")
    AddTextComponentString(text)
    DrawText(x - width / 2, y - height / 2 + 0.005)
end

function TextoDIV(x, y, width, height, scale, text, r, g, b, a, outline)
    SetTextFont(4)
    SetTextProportional(0)
    SetTextScale(scale, scale)
    SetTextColour(r, g, b, a)
    SetTextDropShadow(0, 0, 0, 0, 255)
    SetTextEdge(1, 0, 0, 0, 255)
    SetTextDropShadow()
    if(outline)then
        SetTextOutline()
    end
    SetTextEntry("STRING")
    AddTextComponentString(text)
    DrawText(x - width / 2, y - height / 2 + 0.005)
end

function DrawR(xMin, xMax, yMin, yMax, color1, color2, color3, color4)
    DrawRect(xMin, yMin, xMax, yMax, color1, color2, color3, color4); 
end

Citizen.CreateThread(function()
    while true do
        if not Menu.hidden then 
            TextoDIVTitulo(1.225, 0.632, 1.0, 1.0, 0.8, "Ammu Nation", 255, 255, 255, 255, true)
            --[[
            DrawRect(0.80, 0.554, 0.3, 0.16, 0, 0, 0, 150)
            DrawRect(0.80, 0.450, 0.3, 0.05, 255, 124, 173, 150)
            TextoDIV(1.24, 0.9425, 1.0, 1.05, 0.8, "Valor dos Pentes", 255, 255, 255, 255, true)
            TextoDIV(1.16, 0.97, 1.0, 1.0, 0.5, "Pistola: ~g~R$ ~r~360,00", 255, 255, 255, 255, true)
            TextoDIV(1.16, 1.00, 1.0, 1.0, 0.5, "Pistola SNS: ~g~R$ ~r~180,00", 255, 255, 255, 255, true)
            TextoDIV(1.16, 1.03, 1.0, 1.0, 0.5, "12: ~g~R$ ~r~1600,00", 255, 255, 255, 255, true)
            TextoDIV(1.31, 0.97, 1.0, 1.0, 0.5, "SMG: ~g~R$ ~r~3000,00", 255, 255, 255, 255, true) 
            TextoDIV(1.31, 1.00, 1.0, 1.0, 0.5, "AK-47: ~g~R$ ~r~5000,00", 255, 255, 255, 255, true)
            TextoDIV(1.31, 1.03, 1.0, 1.0, 0.5, "M4: ~g~R$ ~r~6000,00", 255, 255, 255, 255, true) 
            TextoDIV(1.23, 1.095, 1.0, 1.0, 0.5, "OBS: Valores sujeito a mudanças", 255, 255, 255, 255, true)
            ]]
        end
        Citizen.Wait(0)
    end
end)
Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)
        if IsEntityDead(GetPlayerPed(-1)) then
            Menu.hidden = true
            Citizen.Wait(5000)
        end
    end
end)