local lang = vRP.lang
local cfg = module("cfg/homes")

vRP.prepare("vRP/home_tables", [[
CREATE TABLE IF NOT EXISTS vrp_user_homes(
  user_id INTEGER,
  home VARCHAR(100),
  number INTEGER,
  CONSTRAINT pk_user_homes PRIMARY KEY(user_id),
  CONSTRAINT fk_user_homes_users FOREIGN KEY(user_id) REFERENCES vrp_users(id) ON DELETE CASCADE,
  UNIQUE(home,number)
);
]])

vRP.prepare("vRP/get_address","SELECT home, number FROM vrp_user_homes WHERE user_id = @user_id")
vRP.prepare("vRP/get_home_owner","SELECT user_id FROM vrp_user_homes WHERE home = @home AND number = @number")
vRP.prepare("vRP/rm_address","DELETE FROM vrp_user_homes WHERE user_id = @user_id")
vRP.prepare("vRP/set_address","REPLACE INTO vrp_user_homes(user_id,home,number) VALUES(@user_id,@home,@number)")

async(function()
    vRP.execute("vRP/home_tables")
end)

local components = {}
function vRP.getUserAddress(user_id, cbr)
    local rows = vRP.query("vRP/get_address", {user_id = user_id})
    return rows[1]
end

function vRP.setUserAddress(user_id,home,number)
    vRP.execute("vRP/set_address", {user_id = user_id, home = home, number = number})
end

function vRP.removeUserAddress(user_id)
    vRP.execute("vRP/rm_address", {user_id = user_id})
end

function vRP.getUserByAddress(home,number,cbr)
    local rows = vRP.query("vRP/get_home_owner", {home = home, number = number})
    if #rows > 0 then
        return rows[1].user_id
    end
end

function vRP.findFreeNumber(home,max,cbr)
    local i = 1
    while i <= max do
        if not vRP.getUserByAddress(home,i) then
            return i
        end
        i = i+1
    end
end

function vRP.defHomeComponent(name, oncreate, ondestroy)
    components[name] = {oncreate,ondestroy}
end

function vRP.getHomeSlotPlayers(stype, sid)
end

local uslots = {}
for k,v in pairs(cfg.slot_types) do
    uslots[k] = {}
    for l,w in pairs(v) do
        uslots[k][l] = {used=false}
    end
end

function vRP.getHomeSlotPlayers(stype, sid)
    local slot = uslots[stype][sid]
    if slot and slot.used then
        return slot.players
    end
end

local function allocateSlot(stype)
    local slots = cfg.slot_types[stype]
    if slots then
        local _uslots = uslots[stype]
        for k,v in pairs(slots) do
            if _uslots[k] and not _uslots[k].used then
                _uslots[k].used = true
                return k
            end
        end
    end

    return nil
end

local function freeSlot(stype, id)
    local slots = cfg.slot_types[stype]
    if slots then
        uslots[stype][id] = {used = false}
    end
end

local function getAddressSlot(home_name,number)
    for k,v in pairs(uslots) do
        for l,w in pairs(v) do
            if w.home_name == home_name and tostring(w.home_number) == tostring(number) then
                return k,l
            end
        end
    end

    return nil,nil
end

local function is_empty(table)
    for k,v in pairs(table) do
        return false
    end

    return true
end

local function leave_slot(user_id,player,stype,sid)
    print(user_id.." leave slot "..stype.." "..sid)
    local slot = uslots[stype][sid]
    local home = cfg.homes[slot.home_name]
    local tmp = vRP.getUserTmpTable(user_id)
    if tmp then
        tmp.home_stype = nil
        tmp.home_sid = nil
    end
    vRPclient._teleport(player, table.unpack(home.entry_point))
    slot.players[user_id] = nil
    for k,v in pairs(cfg.slot_types[stype][sid]) do
        local name,x,y,z = table.unpack(v)
        if name == "entry" then
            local nid = "vRP:home:slot"..stype..sid
            vRPclient._removeNamedMarker(player,nid)
            vRP.removeArea(player,nid)
        else
            local component = components[v[1]]
            if component then
                local data = slot.components[k]
                if not data then
                    data = {}
                    slot.components[k] = data
                end
                component[2](slot.owner_id, stype, sid, k, v._config or {}, data, x, y, z, player)
            end
        end
    end
    if is_empty(slot.players) then
        print("free slot "..stype.." "..sid)
        freeSlot(stype,sid)
    end
end

local function enter_slot(user_id,player,stype,sid)
    print(user_id.." enter slot "..stype.." "..sid)
    local slot = uslots[stype][sid]
    local home = cfg.homes[slot.home_name]
    local tmp = vRP.getUserTmpTable(user_id)
    if tmp then
        tmp.home_stype = stype
        tmp.home_sid = sid
    end
    slot.players[user_id] = player
    local menu = {name=slot.home_name,css={header_color="rgba(255,128,0,0.99)"}}
    menu["Sair"] = {function(player,choice)
        leave_slot(user_id,player,stype,sid)
    end}
    local address = vRP.getUserAddress(user_id)
    if address and address.home == slot.home_name and tostring(address.number) == slot.home_number then
        menu["Mandar todos embora"] = {function(player,choice)
            local copy = {}
            for k,v in pairs(slot.players) do
                copy[k] = v
            end
            for k,v in pairs(copy) do
                leave_slot(k,v,stype,sid)
            end
        end}
    end
    local function entry_enter(player,area)
        vRP.openMenu(player,menu)
    end
    local function entry_leave(player,area)
        vRP.closeMenu(player)
    end
    for k,v in pairs(cfg.slot_types[stype][sid]) do
        local name,x,y,z = table.unpack(v)
        if name == "entry" then
            vRPclient._teleport(player, x,y,z)
            local nid = "vRP:home:slot"..stype..sid
            vRPclient._setNamedMarker(player,nid,x,y,z-1,0.7,0.7,0.5,0,255,125,125,150)
            vRP.setArea(player,nid,x,y,z,1,1.5,entry_enter,entry_leave)
        else
            local component = components[v[1]]
            if component then
                local data = slot.components[k]
                if not data then
                    data = {}
                    slot.components[k] = data
                end
                component[1](slot.owner_id, stype, sid, k, v._config or {}, data, x, y, z, player)
            end
        end
    end
end

function vRP.accessHome(user_id, home, number)
    local _home = cfg.homes[home]
    local stype,slotid = getAddressSlot(home,number)
    local player = vRP.getUserSource(user_id)

    local owner_id = vRP.getUserByAddress(home,number)
    if _home ~= nil and player ~= nil then
        if stype == nil then
            stype = _home.slot
            slotid = allocateSlot(_home.slot)
            if slotid ~= nil then
                local slot = uslots[stype][slotid]
                slot.home_name = home
                slot.home_number = number
                slot.owner_id = owner_id
                slot.players = {}
                slot.components = {}
            end
        end
        if slotid ~= nil then
            enter_slot(user_id,player,stype,slotid)
            return true
        end
    end
end

local function build_entry_menu(user_id, home_name)
    local home = cfg.homes[home_name]
    local menu = {name=home_name,css={header_color="rgba(255,128,0,0.99)"}}
    menu["Campainha"] = {function(player,choice)
        local number = vRP.prompt(player, "Qual o número da residencia?", "")
        number = parseInt(number)
        local huser_id = vRP.getUserByAddress(home_name,number)
        if huser_id then
            if huser_id == user_id then
                if not vRP.accessHome(user_id, home_name, number) then
                    TriggerClientEvent("pNotify:SendNotification", player, {
                        text = "Não disponivel",
                        type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                        animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                    })
            	end
            else
                local hplayer = vRP.getUserSource(huser_id)
                if hplayer ~= nil then
                    local who = vRP.prompt(player,"Quem está tocando a campainha?","")
                    TriggerClientEvent("pNotify:SendNotification", player, {
                        text = "Tocando campainha",
                        type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                        animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                    })
                    if vRP.request(hplayer, who .. " está tocando sua campainha", 15) then
                        vRP.accessHome(user_id, home_name, number)
                    else
                        TriggerClientEvent("pNotify:SendNotification", player, {
                            text = "Sua entrada foi negada",
                            type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                            animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                        })
                    end
                else
                    TriggerClientEvent("pNotify:SendNotification", player, {
                        text = "Sua entrada foi negada",
                        type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                        animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                    })
                end
            end
        else
            TriggerClientEvent("pNotify:SendNotification", player, {
                text = "Não encontrado",
                type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
            })
        end
    end}

    menu["Comprar Ímovel"] = {function(player,choice)
        local address = vRP.getUserAddress(user_id)
        if not address then
            local number = vRP.findFreeNumber(home_name, home.max)
            if number then
                if vRP.tryPayment(user_id, home.buy_price) then
                    vRP.setUserAddress(user_id, home_name, number)
                    TriggerClientEvent("pNotify:SendNotification", player, {
                        text = "Você comprou este ímovel",
                        type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                        animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                    })
                else
                    TriggerClientEvent("pNotify:SendNotification", player, {
                        text = "Você não possui dinheiro suficiente",
                        type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                        animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                    })
                end
            else
                TriggerClientEvent("pNotify:SendNotification", player, {
                    text = "Este ímovel já está em sua capacidade máxima",
                    type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end
        else
            TriggerClientEvent("pNotify:SendNotification", player, {
                text = "Você já possui uma casa",
                type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
            })
        end
    end, lang.home.buy.description({home.buy_price})}

    menu["Vender Ímovel"] = {function(player,choice)
        local address = vRP.getUserAddress(user_id)
        if address and address.home == home_name then
            vRP.giveMoney(user_id, home.sell_price)
            vRP.removeUserAddress(user_id)
            TriggerClientEvent("pNotify:SendNotification", player, {
                text = "Você vendeu este ímovel",
                type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
            })
        else
            TriggerClientEvent("pNotify:SendNotification", player, {
                text = "Você não possui um ímovel",
                type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
            })
        end
    end, lang.home.sell.description({home.sell_price})}
    return menu
end

local function build_client_homes(source)
    local user_id = vRP.getUserId(source)
    if user_id then
        for k,v in pairs(cfg.homes) do
            local x,y,z = table.unpack(v.entry_point)
            local function entry_enter(player,area)
                local user_id = vRP.getUserId(player)
                if user_id and vRP.hasPermissions(user_id,v.permissions or {}) then
                    vRP.openMenu(source,build_entry_menu(user_id, k))
                end
            end
            local function entry_leave(player,area)
                vRP.closeMenu(player)
            end
            vRPclient._addBlip(source,x,y,z,v.blipid,v.blipcolor,k)
            vRPclient._addMarker(source,x,y,z-1,0.7,0.7,0.5,0,255,125,125,150)
            vRP.setArea(source,"vRP:home"..k,x,y,z,1,1.5,entry_enter,entry_leave)
        end
    end
end

AddEventHandler("vRP:playerSpawn",function(user_id, source, first_spawn)
    if first_spawn then
        build_client_homes(source)
    else
        local tmp = vRP.getUserTmpTable(user_id)
        if tmp and tmp.home_stype then
            leave_slot(user_id, source, tmp.home_stype, tmp.home_sid)
        end
    end
end)

AddEventHandler("vRP:playerLeave",function(user_id, player)
    local tmp = vRP.getUserTmpTable(user_id)
    if tmp and tmp.home_stype then
        leave_slot(user_id, player, tmp.home_stype, tmp.home_sid)
    end
end)