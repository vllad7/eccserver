local blips = {
    {nome = "Ammu Nation",                cor = 1,  id = 156, x = 18.494960784912,    y = -1109.8464355469,  z = 29.797029495239 },
    {nome = "Ammu Nation",                cor = 1,  id = 156, x = 812.21673583984,    y = -2153.23828125,    z = 29.618995666504 },
    {nome = "Ammu Nation",                cor = 1,  id = 156, x = 843.78131103516,    y = -1025.6691894531,  z = 28.194860458374 },
    {nome = "Campo de Maconha",           cor = 25, id = 496, x = 2222.228515625,     y = 5577.2719726563,   z = 52.840530395508 },
    {nome = "Campo de Cocaina",           cor = 4,  id = 501, x = 1998.6602783203,    y = 4839.1049804688,   z = 43.548267364502 },
    {nome = "Laboratório de Cocaína",     cor = 4,  id = 501, x = 3573.9326171875,    y = 3734.1953125,      z = 36.642639160156 },
    {nome = "Laboratório de Maconha",     cor = 25, id = 496, x = 3526.1618652344,    y = 3734.4936523438,   z = 36.637866973877 },
    {nome = "Coleta de Armas",            cor = 40, id = 478, x = 138.78096008301,    y = -3110.8371582031,  z = 5.8963084220886 },
    {nome = "Aprimoramento de Armas",     cor = 40, id = 150, x = -52.597068786621,   y = -2523.1508789063,  z = 7.4011702537537 },
    {nome = "Biqueira (Maconha)",         cor = 2,  id = 140, x = 159.83004760742,    y = -1221.6595458984,  z = 29.540601730347 },
    {nome = "Biqueira (Cocaína)",         cor = 4,  id = 354, x = -1811.5197753906,   y = -127.2709274292,   z = 77.786514282227 },
    {nome = "Lavanderia (Dinheiro Sujo)", cor = 40, id = 500, x = -559.01287841797,   y = -1803.5979003906,  z = 22.608966827393 },
    {nome = "Escavação (Cobre)",          cor = 21, id = 89,  x = -494.81127929688,   y = -1752.521484375,   z = 18.316606521606 },
    {nome = "Fundição (Aço)",             cor = 62, id = 76,  x = 1114.9693603516,    y = -2004.0101318359,  z = 35.439395904541 },
    {nome = "Fabricação de Munição",      cor = 17, id = 311, x = 2330.2780761719,    y = 2572.0107421875,   z = 46.679504394531 },
    {nome = "Academia",                   cor = 59, id = 436, x = -1202.9625244146,   y = -1566.14086914063, z = 4.6104063987731 },
	{nome = "Coletar Tartaruga",          cor = 28, id = 365, x = -2118.327148437,    y = -1218.7764892578,  z = -141.2243804916 },
    {nome = "Vender Tartaruga",           cor = 1, id = 197,  x = -1816.2939453125,	  y = -1192.9519042969,	 z = 14.305061340332 }
}

Citizen.CreateThread(function()
    for _, info in pairs(blips) do
        info.blip = AddBlipForCoord(info.x, info.y, info.z)
        SetBlipSprite(info.blip, info.id)
        SetBlipDisplay(info.blip, 4)
        SetBlipScale(info.blip, 0.9)
        SetBlipColour(info.blip, info.cor)
        SetBlipAsShortRange(info.blip, true)
        BeginTextCommandSetBlipName("STRING")
        AddTextComponentString(info.nome)
        EndTextCommandSetBlipName(info.blip)
    end
end)

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- THREAD PARA COLOCAR DRAWMARKER ONDE QUISER A PARTIR DE UMA ARRAY (MARCAS PEQUENAS)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
local marcacoes = {
    -- O ultimo é sempre sem virgula, só para lembrar...
    {x = 1273.5339355469, y = -1711.4639892578, z = 54.771507263184} -- Hacker (Dinheiro Sujo)
}

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)
        for k in pairs(marcacoes) do
            DrawMarker(27, marcacoes[k].x, marcacoes[k].y, marcacoes[k].z, 0, 0, 0, 0, 0, 0, 1.001, 1.0001, 0.5001, 255, 255, 255, 155, 0, 0, 0, 0)
        end
    end
end)

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- THREAD PARA COLOCAR DRAWMARKER ONDE QUISER A PARTIR DE UMA ARRAY (MARCAS GRANDES)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
local marcacoes = {
    -- O ultimo é sempre sem virgula, só para lembrar...
    --{x = 215.124,            y = -791.377,          z = 29.90 }

    }

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)
        for k in pairs(marcacoes) do
            DrawMarker(27, marcacoes[k].x, marcacoes[k].y, marcacoes[k].z, 0, 0, 0, 0, 0, 0, 3.001, 3.0001, 0.5001, 255, 255, 255, 155, 0, 0, 0, 0)
        end
    end
end)
