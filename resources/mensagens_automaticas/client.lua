local m = {}
m.delay = 10

m.prefix = '^0[Evercast Crew] '
m.suffix = ''
m.messages = {
    '^1Não utilize qualquer tipo de glitch, hack, etc, IREMOS TE BANIR DE FORMA IMEDIATA E NÃO SERÁ REVOGADO!',
    '^1Não pratique copbaiting.',
    '^1Sempre RP de forma realista - use seu senso comum!',
    '^1Nenhum spam de qualquer tipo!',
    '^1Respeite todos os jogadores e siga as ordens dos administradores!',
    '^1Sem matar por vingança caso morra (lembre-se, isto simula RP, você não tem recordações pós-morte!)',
    '^1Informe todos os erros que você encontrar para um administrador o mais rápido possível!',
    '^1Informe qualquer problema com outros jogadores / policiais para um administrador.',
}

m.ignorelist = {

}

local playerIdentifiers
local enableMessages = true
local timeout = m.delay * 1000 * 60
local playerOnIgnoreList = false
RegisterNetEvent('va:setPlayerIdentifiers')
AddEventHandler('va:setPlayerIdentifiers', function(identifiers)
    playerIdentifiers = identifiers
end)
Citizen.CreateThread(function()
    while playerIdentifiers == {} or playerIdentifiers == nil do
        Citizen.Wait(1000)
        TriggerServerEvent('va:getPlayerIdentifiers')
    end
    for iid in pairs(m.ignorelist) do
        for pid in pairs(playerIdentifiers) do
            if m.ignorelist[iid] == playerIdentifiers[pid] then
                playerOnIgnoreList = true
                break
            end
        end
    end
    if not playerOnIgnoreList then
        while true do
            for i in pairs(m.messages) do
                if enableMessages then
                    chat(i)

                end
                Citizen.Wait(timeout)
            end

            Citizen.Wait(0)
        end
    else

    end
end)
function chat(i)
    TriggerEvent('chatMessage', '', {255,255,255}, m.prefix .. m.messages[i] .. m.suffix)
end
RegisterCommand('automessage', function()
    enableMessages = not enableMessages
    if enableMessages then
        status = '^2enabled^5.'
    else
        status = '^1disabled^5.'
    end
    TriggerEvent('chatMessage', '', {255, 255, 255}, '^5[Base Zezão] as mensagens automáticas são agora ' .. status)
end, false)
--------------------------------------------------------------------------
