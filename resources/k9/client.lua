-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- VARIÁVEIS LOCAIS
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP")

local k9_name = "Zezinho"
local just_started = true
local spawned_ped = nil
local following = false
local playing_animation = false
local adestrado = 0

local animations = {
        ['Normal'] = {
            sit = {
                dict = "creatures@rottweiler@amb@world_dog_sitting@idle_a",
                anim = "idle_b"
            },
            laydown = {
                dict = "creatures@rottweiler@amb@sleep_in_kennel@",
                anim = "sleep_in_kennel"
            }
        }
    }
--]]

--[[ Tables ]]--
local language = {}
--]]

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- NUI
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function EnableMenu()
    SetNuiFocus(true, true)
    SendNUIMessage({
        type = "open_k9_menu"
    })
end
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- NUI CALLBACKS
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
RegisterNUICallback("closemenu", function(data)
    SetNuiFocus(false, false)
end)

RegisterNUICallback("updatename", function(data)
    k9_name = data.name
end)

RegisterNUICallback("Adestrar", function(data)
    TriggerEvent("K9:Adestrar", data.model)
end)

RegisterNUICallback("Abandonar", function(data)
    TriggerEvent("K9:Abandonar", data.model)
end)

    RegisterNUICallback("sit", function(data)
        if spawned_ped ~= nil then
            PlayAnimation(animations['Normal'].sit.dict, animations['Normal'].sit.anim)
        end
    end)

    RegisterNUICallback("laydown", function(data)
        if spawned_ped ~= nil then
            PlayAnimation(animations['Normal'].laydown.dict, animations['Normal'].laydown.anim)
        end
    end)

--]]


    -- Opens K9 Menu
    RegisterNetEvent("K9:OpenMenu")
    AddEventHandler("K9:OpenMenu", function(pedRestriction, SkinsBloqueadas)
        if pedRestriction then
            if not ChecarSkin(GetLocalPed(), SkinsBloqueadas) then
                EnableMenu()
            else
                TriggerEvent('chatMessage', 'K9', { 255, 0, 0 }, "Você não pode chamar seu animal enquanto estiver em serviço.")
            end
        else
            EnableMenu()
        end
    end)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- ADESTRAR
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
RegisterNetEvent("K9:Adestrar")
AddEventHandler("K9:Adestrar", function(model)
    if spawned_ped == nil and adestrado == 0 then
        adestrado = 1
        local ped = GetHashKey(model)
        RequestModel(ped)
        while not HasModelLoaded(ped) do
            Citizen.Wait(1)
            RequestModel(ped)
        end
        local plyCoords = GetOffsetFromEntityInWorldCoords(GetLocalPed(), 0.0, 2.0, 0.0)
        local dog = CreatePed(28, ped, plyCoords.x, plyCoords.y, plyCoords.z, GetEntityHeading(GetLocalPed()), 0, 1)
        spawned_ped = dog
        SetBlockingOfNonTemporaryEvents(spawned_ped, true)
        SetPedFleeAttributes(spawned_ped, 0, 0)
        SetPedRelationshipGroupHash(spawned_ped, GetHashKey("k9"))
        local blip = AddBlipForEntity(spawned_ped)
        SetBlipAsFriendly(blip, true)
        SetBlipSprite(blip, 442)
        BeginTextCommandSetBlipName("STRING")
        AddTextComponentString(tostring("K9: ".. k9_name))
        EndTextCommandSetBlipName(blip)
        NetworkRegisterEntityAsNetworked(spawned_ped)
        while not NetworkGetEntityIsNetworked(spawned_ped) do
            NetworkRegisterEntityAsNetworked(spawned_ped)
            Citizen.Wait(1)
        end
    else
        TriggerEvent('chatMessage', 'K9', { 255, 0, 0 }, "Você só pode adestrar um cachorro por vez.")
    end
end)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- ABANDONAR
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
RegisterNetEvent("K9:Abandonar")
AddEventHandler("K9:Abandonar", function(model)
    if adestrado == 1 then
        adestrado = 0
        local has_control = false
        RequestNetworkControl(function(cb)
            has_control = cb
        end)
        if has_control then
            SetEntityAsMissionEntity(spawned_ped, true, true)
            DeleteEntity(spawned_ped)
            spawned_ped = nil
            following = false
            playing_animation = false
        end
    else
        TriggerEvent('chatMessage', 'K9', { 255, 0, 0 }, "Você não possui nenhum cachorro para abandonar.")
    end
end)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- SEGUIR / PARAR DE SEGUIR
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
RegisterNetEvent("K9:Seguir")
AddEventHandler("K9:Seguir", function()
    if spawned_ped ~= nil then
        if not following then
            local has_control = false
            RequestNetworkControl(function(cb)
                has_control = cb
            end)
                
            if has_control then
                TaskFollowToOffsetOfEntity(spawned_ped, GetLocalPed(), 0.5, 0.0, 0.0, 5.0, -1, 0.0, 1)
                SetPedKeepTask(spawned_ped, true)
                following = true
                TriggerEvent('chatMessage', 'K9', { 255, 0, 0 }, k9_name .." está te acompanhando agora.")
            end
        else
            local has_control = false
            RequestNetworkControl(function(cb)
                has_control = cb
            end)
            if has_control then
                SetPedKeepTask(spawned_ped, false)
                ClearPedTasks(spawned_ped)
                following = false
                TriggerEvent('chatMessage', 'K9', { 255, 0, 0 }, k9_name .." parou de te acompanhar.")
            end
        end
    end
end)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- ENTRAR / SAIR DO VEÍCULO
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
RegisterNetEvent("K9:Veiculo")
AddEventHandler("K9:Veiculo", function(isRestricted, vehList)
    if spawned_ped ~= nil then    
        if IsPedInAnyVehicle(spawned_ped, false) then
            TaskLeaveVehicle(spawned_ped, GetVehiclePedIsIn(spawned_ped, false), 256)
            TriggerEvent('chatMessage', 'K9', { 255, 0, 0 }, k9_name .." saiu do veículo.")
        else

            if not IsPedInAnyVehicle(GetLocalPed(), false) then
                local plyCoords = GetEntityCoords(GetLocalPed(), false)
                local vehicle = GetVehicleAheadOfPlayer()
                    if isRestricted then
                        if CheckVehicleRestriction(vehicle, vehList) then
                            TriggerEvent('chatMessage', 'K9', { 255, 0, 0 }, k9_name .." entrou no veículo.")
                            SetVehicleDoorOpen(vehicle, 1, false, false)
                            Citizen.Wait(1000)
                            SetPedIntoVehicle(spawned_ped,vehicle,0)
                            Citizen.Wait(1000)
                            SetVehicleDoorShut(vehicle, 1, false)                        
                        end
                    else
                        TriggerEvent('chatMessage', 'K9', { 255, 0, 0 }, k9_name .." entrou no veículo.")
                        SetVehicleDoorOpen(vehicle, 1, false, false)
                        Citizen.Wait(1000)
                        SetPedIntoVehicle(spawned_ped,vehicle,0)
                        Citizen.Wait(1000)
                        SetVehicleDoorShut(vehicle, 1, false)    
                    end
            else
                local vehicle = GetVehiclePedIsIn(GetLocalPed(), false)
                if isRestricted then
                    if CheckVehicleRestriction(vehicle, vehList) then                    
                        TriggerEvent('chatMessage', 'K9', { 255, 0, 0 }, k9_name .." entrou no veículo.")
                        SetVehicleDoorOpen(vehicle, 1, false, false)
                        Citizen.Wait(1000)
                        SetPedIntoVehicle(spawned_ped,vehicle,0)
                        Citizen.Wait(1000)
                        SetVehicleDoorShut(vehicle, 1, false)    
                    end
                else
                    local velocidade = GetEntitySpeed(GetVehiclePedIsIn(GetPlayerPed(-1), false)) * 3.6
                    local Coordenadas = GetEntityCoords( GetPlayerPed(-1) )
                    local Distancia = GetDistanceBetweenCoords(Coordenadas.x, Coordenadas.y, Coordenadas.z, spawned_ped, true)
                    if velocidade > 0 and Distancia > 10.0 then
                        TriggerEvent('chatMessage', 'K9', { 255, 0, 0 }, "Você deve parar o veículo e chamar o dog " .. k9_name .. " entrar")
                    else
                        TriggerEvent('chatMessage', 'K9', { 255, 0, 0 }, k9_name .." entrou no veículo")
                        SetVehicleDoorOpen(vehicle, 1, false, false)
                        Citizen.Wait(1000)
                        SetPedIntoVehicle(spawned_ped,vehicle,0)
                        Citizen.Wait(1000)
                        SetVehicleDoorShut(vehicle, 1, false)  
                    end 
                end
            end
        end
    end
end)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- THREADS
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- TECLAS
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)
        if IsControlPressed(1, 19) and IsControlJustPressed(1, 213) then -- ABRIR O MENU
            TriggerServerEvent("K9:RequestOpenMenu")
        end
            
        if IsControlJustPressed(1, 288) then -- SEGUIR / PARAR DE SEGUIR
            TriggerEvent("K9:Seguir")
        end

        if IsControlJustPressed(1, 289) then -- ENTRAR / SAIR DO VEÍCULO
            TriggerEvent("K9:Veiculo")
        end
    end
end)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- CLEANER
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)
        if just_started then
            Citizen.Wait(1000)
            local resource = GetCurrentResourceName()
            SendNUIMessage({
                type = "update_resource_name",
                name = resource
            })
            just_started = false
            SetNuiFocus(false, false)
        end
    end
end)

--[[ EXTRA FUNCTIONS ]]--

-- Gets Local Ped
function GetLocalPed()
    return GetPlayerPed(PlayerId())
end

-- Gets Control Of Ped
function RequestNetworkControl(callback)
    local netId = NetworkGetNetworkIdFromEntity(spawned_ped)
    local timer = 0
    NetworkRequestControlOfNetworkId(netId)
    while not NetworkHasControlOfNetworkId(netId) do
        Citizen.Wait(1)
        NetworkRequestControlOfNetworkId(netId)
        timer = timer + 1
        if timer == 5000 then
            callback(false)
        end
    end
    callback(true)
end

-- Gets Players
function GetPlayers()
    local players = {}
    for i = 0, 32 do
        if NetworkIsPlayerActive(i) then
            table.insert(players, i)
        end
    end
    return players
end

-- Set K9 Animation (Sit / Laydown)
function PlayAnimation(dict, anim)
    RequestAnimDict(dict)
    while not HasAnimDictLoaded(dict) do
        Citizen.Wait(0)
    end

    TaskPlayAnim(spawned_ped, dict, anim, 8.0, -8.0, -1, 2, 0.0, 0, 0, 0)
end

-- Gets Player ID
function GetPlayerId(target_ped)
    local players = GetPlayers()
    for a = 1, #players do
        local ped = GetPlayerPed(players[a])
        local server_id = GetPlayerServerId(players[a])
        if target_ped == ped then
            return server_id
        end
    end
    return 0
end

-- Checks Ped Restriction
function ChecarSkin(ped, SkinsBloqueadas)
	for i = 1, #SkinsBloqueadas do
		if GetHashKey(SkinsBloqueadas[i]) == GetEntityModel(ped) then
			return true
		end
	end
	return false
end

-- Checks Vehicle Restriction
function CheckVehicleRestriction(vehicle, VehicleList)
	for i = 1, #VehicleList do
		if GetHashKey(VehicleList[i]) == GetEntityModel(vehicle) then
			return true
		end
	end
	return false
end

-- Gets Vehicle Ahead Of Player
function GetVehicleAheadOfPlayer()
    local lPed = GetLocalPed()
    local lPedCoords = GetEntityCoords(lPed, alive)
    local lPedOffset = GetOffsetFromEntityInWorldCoords(lPed, 0.0, 3.0, 0.0)
    local rayHandle = StartShapeTestCapsule(lPedCoords.x, lPedCoords.y, lPedCoords.z, lPedOffset.x, lPedOffset.y, lPedOffset.z, 1.2, 10, lPed, 7)
    local returnValue, hit, endcoords, surface, vehicle = GetShapeTestResult(rayHandle)

    if hit then
        return vehicle
    else
        return false
    end
end