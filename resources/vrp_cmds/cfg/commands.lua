cfg = {}

cfg.commands = {
    ["/mascara"] = {
        action = function(p,color,msg)
            local user_id = vRP.getUserId(p)
            CMDclient.togglePlayerMask(p)
        end
    },
    ["/placa"] = {
        action = function(p,color,msg)
            local user_id = vRP.getUserId(p)
            if vRP.hasPermission(user_id,"checar.placa") then
                if msg then
                    local user_id = vRP.getUserByRegistration(msg)
                    if user_id then
                        local identity = vRP.getUserIdentity(user_id)
                        if identity then
                            TriggerClientEvent('chatMessage', p, "LSPD", {80, 80, 255}, identity.name.." "..identity.firstname..", "..identity.age)
                        else
                            TriggerClientEvent('chatMessage', p, "LSPD", {80, 80, 255}, "Dono não localizado, veiculo roubado ou invalido!")
                        end
                    else
                        TriggerClientEvent('chatMessage', p, "LSPD", {80, 80, 255}, "Dono não localizado, veiculo roubado ou invalido!")
                    end
                else
                    TriggerClientEvent('chatMessage', p, "Sistema", {255, 0, 0}, "Use: /placa + numero da placa")
                end
            else
                TriggerClientEvent('chatMessage', p, "Sistema", {255, 0, 0}, "Você não tem permissão para usar ste comando!")
            end
        end
    },
    ["/casa"] = {
        action = function(p,color,msg)
            local user_id = vRP.getUserId(p)

            local adresses = vRPh.getUserAddresses(user_id)
            local homeless = true
            for k,address in pairs(adresses) do
                homeless = false
                TriggerClientEvent('chatMessage', p, "HOME "..k, {255, 0, 0}, address.home..", "..address.number)
            end
            if homeless then
                TriggerClientEvent('chatMessage', p, "Sistema", {255, 0, 0}, "Você não tem uma casa!")
            end
        end
    },
}

return cfg