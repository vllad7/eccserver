
local items = {}

local function play_drink(player)
  local seq = {
    {"mp_player_intdrink","intro_bottle",1},
    {"mp_player_intdrink","loop_bottle",1},
    {"mp_player_intdrink","outro_bottle",1}
  }

  vRPclient._playAnim(player,true,seq,false)
end

local pills_choices = {}
pills_choices["Tomar"] = {function(player,choice)
  local user_id = vRP.getUserId(player)
  if user_id then
    if vRP.tryGetInventoryItem(user_id,"pills",1) then
      vRPclient._varyHealth(player,25)
      TriggerClientEvent("pNotify:SendNotification", player, {
        text = "Tomando Pilulas",
        type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
        animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
      })
      play_drink(player)
      vRP.closeMenu(player)
    end
  end
end}

items["pills"] = {"Pills","",function(args) return pills_choices end,0.1}
items["Maconha"] = {"maconha", "", function(args) return smoke_choices end, 0.1}
items["cocaina"] = {"cocaina", "", function(args) return smell_choices end, 0.1}
items["LSD"] = {"LSD", "", function(args) return lsd_choices end, 0.1}
items["rebit"] = {"rebit", "", function(args) return lsd_choices end, 0.1}

return items
