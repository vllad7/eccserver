
local cfg = {}

-- example of study transformer
local itemtr_study = {
}

-- example of radio stations
local radio_stations = {
}

-- default flats positions from https://github.com/Nadochima/HomeGTAV/blob/master/List

-- define the home slots (each entry coordinate should be unique for ALL types)
-- each slots is a list of home components
--- {component,x,y,z} (optional _config)
--- the entry component is required
cfg.slot_types = {
}

-- define home clusters
cfg.homes = {
}

return cfg
