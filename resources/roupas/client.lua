local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP")
CLOserver = Tunnel.getInterface("roupas")

cmenu = {show = 0, row = 0, field = 1}
text_in = 0
draw = {0,0,0}
prop = {0,0,1}
model_id = 1
bar = {x=0.328, y=0.142, x1=0.037,y1=0.014}

Roupas = {
	[1]  = {["X"] = -1574.454, ["Y"] = -570.071, ["Z"] = 107.653},
	[2]  = {["X"] =72.2545394897461, ["Y"] = -1399.10229492188, ["Z"] =  28.2761386871338},
	[3]  = {["X"] =-703.77685546875, ["Y"] = -152.258544921875, ["Z"] = 36.5151458740234},
	[4]  = {["X"] =-167.863754272461, ["Y"] = -298.969482421875, ["Z"] = 38.8332878112793},
	[5]  = {["X"] =428.694885253906, ["Y"] = -800.1064453125, ["Z"] = 28.5911422729492},
	[6]  = {["X"] =-829.413269042969, ["Y"] = -1073.71032714844, ["Z"] = 10.4281078338623},
	[7]  = {["X"] =-1193.42956542969, ["Y"] = -772.262329101563, ["Z"] = 16.4244285583496},
	[8]  = {["X"] =-1447.7978515625, ["Y"] = -242.461242675781, ["Z"] = 48.9207931518555},
	[9]  = {["X"] =11.6323690414429, ["Y"] = 6514.224609375, ["Z"] = 30.9778476715088},
	[10]  = {["X"] =1696.29187011719, ["Y"] = 4829.3125, ["Z"] = 41.1631141662598},
	[11]  = {["X"] =123.64656829834, ["Y"] = -219.440338134766, ["Z"] = 53.6578384399414},
	[12]  = {["X"] =618.093444824219, ["Y"] = 2759.62939453125, ["Z"] = 41.1881042480469},
	[13]  = {["X"] =1190.55017089844, ["Y"] = 2713.44189453125, ["Z"] = 37.3226257324219},
	[14]  = {["X"] =-3172.49682617188, ["Y"] = 1048.13330078125, ["Z"] = 19.9632030487061},
	[15]  = {["X"] =-1108.44177246094, ["Y"] = 2708.92358398438, ["Z"] = 18.2078643798828},
}

local RoupasBlip = { 
	{nome = "Loja de Roupas", id = 73, cor = 3, x = -1574.454,y = -570.071,107.653},
	{nome = "Loja de Roupas", id = 73, cor = 3, x = 72.2545394897461,y = -1399.10229492188,z = 29.3761386871338},
	{nome = "Loja de Roupas", id = 73, cor = 3, x = -703.77685546875,y = -152.258544921875,z = 37.4151458740234},
	{nome = "Loja de Roupas", id = 73, cor = 3, x = -167.863754272461,y = -298.969482421875,z = 39.7332878112793},
	{nome = "Loja de Roupas", id = 73, cor = 3, x = 428.694885253906,y = -800.1064453125,z = 29.4911422729492},
	{nome = "Loja de Roupas", id = 73, cor = 3, x = -829.413269042969,y = -1073.71032714844,z = 11.3281078338623},
	{nome = "Loja de Roupas", id = 73, cor = 3, x = -1193.42956542969,y = -772.262329101563,z = 17.3244285583496},
	{nome = "Loja de Roupas", id = 73, cor = 3, x = -1447.7978515625,y = -242.461242675781,z = 49.8207931518555},
	{nome = "Loja de Roupas", id = 73, cor = 3, x = 11.6323690414429,y = 6514.224609375,z = 31.8778476715088},
	{nome = "Loja de Roupas", id = 73, cor = 3, x = 1696.29187011719,y = 4829.3125,z = 42.0631141662598},
	{nome = "Loja de Roupas", id = 73, cor = 3, x = 123.64656829834,y = -219.440338134766,z = 54.5578384399414},
	{nome = "Loja de Roupas", id = 73, cor = 3, x = 618.093444824219,y = 2759.62939453125,z = 42.0881042480469},
	{nome = "Loja de Roupas", id = 73, cor = 3, x = 1190.55017089844,y = 2713.44189453125,z = 38.2226257324219},
	{nome = "Loja de Roupas", id = 73, cor = 3, x = -3172.49682617188,y = 1048.13330078125,z = 20.8632030487061},
	{nome = "Loja de Roupas", id = 73, cor = 3, x = -1108.44177246094,y = 2708.92358398438,z = 19.1078643798828}
}

Citizen.CreateThread(function()
	for _, item in pairs(RoupasBlip) do
      	item.blip = AddBlipForCoord(item.x, item.y, item.z)
      	SetBlipSprite(item.blip, item.id)
      	SetBlipColour(item.blip, item.cor)
      	SetBlipAsShortRange(item.blip, true)
      	BeginTextCommandSetBlipName("STRING")
      	AddTextComponentString(item.nome)
      	EndTextCommandSetBlipName(item.blip)
    end
end)

function drawTxt(x,y ,width,height,scale, text, r,g,b,a)
    SetTextFont(0)
    SetTextProportional(0)
    SetTextScale(scale, scale)
    SetTextColour(r, g, b, a)
    SetTextDropShadow(0, 0, 0, 0,255)
    SetTextEdge(1, 0, 0, 0, 255)
    SetTextDropShadow()
    SetTextOutline()
    SetTextEntry("STRING")
    AddTextComponentString(text)
    DrawText(x - width/2, y - height/2 + 0.005)
end

function startClothes()
	cmenu.show = 1
	cmenu.row = 1
	cmenu.field = 1
	text_in = 0
end

function ClothShop()
	DrawRect(0.12,0.07,0.22,0.09,0,0,0,150)
	drawTxt(0.177, 0.066, 0.25, 0.03, 0.40,"LOJA DE ROUPAS",255,255,255,255)
	DrawRect(0.12,0.024,0.216,0.005,255,128,0,150)
	if cmenu.show == 1 then
		if cmenu.row == 1 then DrawRect(0.12,0.135,0.22,0.035,255,255,255,150) else DrawRect(0.12,0.135,0.22,0.035,0,0,0,150) end
		if cmenu.row == 2 then DrawRect(0.12,0.172,0.22,0.035,255,255,255,150) else DrawRect(0.12,0.172,0.22,0.035,0,0,0,150) end
		if cmenu.row == 3 then DrawRect(0.12,0.209,0.22,0.035,255,255,255,150) else DrawRect(0.12,0.209,0.22,0.035,0,0,0,150) end
		if cmenu.row == 4 then DrawRect(0.12,0.246,0.22,0.035,255,255,255,150) else DrawRect(0.12,0.246,0.22,0.035,0,0,0,150) end
		drawTxt(0.177, 0.128, 0.25, 0.03, 0.40,"Roupa",255,255,255,255)
		drawTxt(0.177, 0.165, 0.25, 0.03, 0.40,"Acessórios",255,255,255,255)
		drawTxt(0.177, 0.202, 0.25, 0.03, 0.40,"Selecionar Personagem",255,255,255,255)
		drawTxt(0.177, 0.239, 0.25, 0.03, 0.40,"Sair",255,255,255,255)
	elseif cmenu.show == 2 then
		DrawRect(0.12,0.135,0.22,0.035,255,255,255,150)
		DrawRect(0.12,0.172,0.22,0.035,0,0,0,150)
		DrawRect(0.12,0.209,0.22,0.035,0,0,0,150)
		DrawRect(0.12,0.246,0.22,0.035,0,0,0,150)
		drawTxt(0.177, 0.128, 0.25, 0.03, 0.40,"~b~Roupas",255,255,255,255)
		drawTxt(0.177, 0.165, 0.25, 0.03, 0.40,"Acessórios",255,255,255,255)
		drawTxt(0.177, 0.202, 0.25, 0.03, 0.40,"Escolher Skin",255,255,255,255)
		drawTxt(0.177, 0.239, 0.25, 0.03, 0.40,"Fechar",255,255,255,255)

		DrawRect(0.328,0.051,0.18,0.049,0,0,0,150)
		drawTxt(0.382,0.048,0.175,0.035, 0.40,"ROUPAS",255,255,255,255)
		DrawRect(0.328,0.024,0.175,0.005,255,128,0,150)
		if cmenu.row == 1 then DrawRect(0.328,0.096,0.18,0.035,255,255,255,150) else DrawRect(0.328,0.096,0.18,0.035,0,0,0,150) end
		if cmenu.row == 2 then DrawRect(0.328,0.133,0.18,0.035,255,255,255,150) else DrawRect(0.328,0.133,0.18,0.035,0,0,0,150) end
		if cmenu.row == 3 then DrawRect(0.328,0.170,0.18,0.035,255,255,255,150) else DrawRect(0.328,0.170,0.18,0.035,0,0,0,150) end
		if cmenu.row == 4 then DrawRect(0.328,0.207,0.18,0.035,255,255,255,150) else DrawRect(0.328,0.207,0.18,0.035,0,0,0,150) end

		local draw_str = string.format("Opção: %d / 11", cmenu.field-1)
		drawTxt(0.328,0.093,0.175,0.035, 0.40,draw_str,255,255,255,255)

		if GetNumberOfPedDrawableVariations(GetPlayerPed(-1), cmenu.field-1) ~= 0 and GetNumberOfPedDrawableVariations(GetPlayerPed(-1), cmenu.field-1) ~= false then
			DrawRect(0.328,0.142,0.175,0.014,255,128,0,150)
			local link = 0.138/(GetNumberOfPedDrawableVariations(GetPlayerPed(-1), cmenu.field-1)-1)
			local new_x = (bar.x-0.069)+(link*draw[1])
			if new_x < 0.259 then new_x = 0.259 end
			if new_x > 0.397 then new_x = 0.397 end
			DrawRect(new_x,bar.y,bar.x1,bar.y1,255,250,205,150)
			DrawRect(0.328,0.179,0.175,0.014,255,128,0,150)
			local link = 0.138/(GetNumberOfPedTextureVariations(GetPlayerPed(-1), cmenu.field-1, draw[1])-1)
			local new_x = (bar.x-0.069)+(link*draw[2])
			if new_x < 0.259 then new_x = 0.259 end
			if new_x > 0.397 then new_x = 0.397 end
			DrawRect(new_x,bar.y+0.037,bar.x1,bar.y1,255,250,205,150)

			DrawRect(0.328,0.216,0.175,0.014,255,128,0,150) 
			local link = 0.138/2
			local new_x = (bar.x-0.069)+(link*draw[3])
			if new_x < 0.259 then new_x = 0.259 end
			if new_x > 0.397 then new_x = 0.397 end
			DrawRect(new_x,bar.y+0.074,bar.x1,bar.y1,255,250,205,150)

			if IsControlJustPressed(1, 189) or IsDisabledControlJustPressed(1, 189) then
				PlaySound(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET", 0, 0, 1)
				if cmenu.row == 1 then
					if cmenu.field > 1 then 
						cmenu.field = cmenu.field-1
						draw[1] = 0
						draw[2] = 0
					else 
						cmenu.field = 12
						draw[1] = 0
						draw[2] = 0
					end
				elseif cmenu.row == 2 then
					if draw[1] > 0 then draw[1] = draw[1]-1 else draw[1] = 0 end
					draw[2] = 0
					SetPedComponentVariation(GetPlayerPed(-1), cmenu.field-1, draw[1], draw[2], draw[3])
				elseif cmenu.row == 3 then
					if draw[2] > 0 then draw[2] = draw[2]-1 else draw[2] = 0 end
					SetPedComponentVariation(GetPlayerPed(-1), cmenu.field-1, draw[1], draw[2], draw[3])
				elseif cmenu.row == 4 then
					if draw[3] > 0 then draw[3] = draw[3]-1 end
				end
			end
			if IsControlJustPressed(1, 190) or IsDisabledControlJustPressed(1, 190) then -- right
				PlaySound(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET", 0, 0, 1)
				if cmenu.row == 1 then
					if cmenu.field < 12 then 
						cmenu.field = cmenu.field+1 
						draw[1] = 0
						draw[2] = 0
					else 
						cmenu.field = 1
						draw[1] = 0
						draw[2] = 0
					end
				elseif cmenu.row == 2 then
					if draw[1] < GetNumberOfPedDrawableVariations(GetPlayerPed(-1), cmenu.field-1)-1 then draw[1] = draw[1]+1 else draw[1] = GetNumberOfPedDrawableVariations(GetPlayerPed(-1), cmenu.field-1)-1 end
					draw[2] = 0
					SetPedComponentVariation(GetPlayerPed(-1), cmenu.field-1, draw[1], draw[2], draw[3])
				elseif cmenu.row == 3 then
					if draw[2] < GetNumberOfPedTextureVariations(GetPlayerPed(-1), cmenu.field-1, draw[1])-1 then draw[2] = draw[2]+1 else draw[2] = GetNumberOfPedTextureVariations(GetPlayerPed(-1), cmenu.field-1, draw[1])-1 end
					SetPedComponentVariation(GetPlayerPed(-1), cmenu.field-1, draw[1], draw[2], draw[3])
				elseif cmenu.row == 4 then
					if draw[3] < 2 then draw[3] = draw[3]+1 end
				end
			end
		else
			drawTxt(0.328,0.130,0.175,0.035, 0.40,"Vazio",255,255,255,255)
			drawTxt(0.328,0.167,0.175,0.035, 0.40,"Vazio",255,255,255,255)
			drawTxt(0.328,0.204,0.175,0.035, 0.40,"Vazio",255,255,255,255)
			if IsControlJustPressed(1, 189) or IsDisabledControlJustPressed(1, 189) then -- left
				PlaySound(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET", 0, 0, 1)
				if cmenu.field > 1 then 
					cmenu.field = cmenu.field-1
					draw[1] = 0
					draw[2] = 0
				else 
					cmenu.field = 12
					draw[1] = 0
					draw[2] = 0
				end
			end
			if IsControlJustPressed(1, 190) or IsDisabledControlJustPressed(1, 190) then -- right
				PlaySound(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET", 0, 0, 1)
				if cmenu.field < 12 then 
					cmenu.field = cmenu.field+1
					draw[1] = 0
					draw[2] = 0
				else 
					cmenu.field = 1
					draw[1] = 0
					draw[2] = 0
				end
			end
		end
	elseif cmenu.show == 3 then
		DrawRect(0.12,0.135,0.22,0.035,0,0,0,150)
		DrawRect(0.12,0.172,0.22,0.035,255,255,255,150)
		DrawRect(0.12,0.209,0.22,0.035,0,0,0,150)
		DrawRect(0.12,0.246,0.22,0.035,0,0,0,150)
		drawTxt(0.177, 0.128, 0.25, 0.03, 0.40,"Roupa",255,255,255,255)
		drawTxt(0.177, 0.165, 0.25, 0.03, 0.40,"~b~Acessórios",255,255,255,255) -- row_2 (+0.037)
		drawTxt(0.177, 0.202, 0.25, 0.03, 0.40,"Selecionar Personagem",255,255,255,255) -- row_2 (+0.037)
		drawTxt(0.177, 0.239, 0.25, 0.03, 0.40,"Sair",255,255,255,255) -- row_2 (+0.037)
		---
		DrawRect(0.328,0.051,0.18,0.049,0,0,0,150) -- title
		drawTxt(0.382,0.048,0.175,0.035, 0.40,"ACESSÓRIOS",255,255,255,255)
		DrawRect(0.328,0.024,0.175,0.005,255,128,0,150)
		if cmenu.row == 1 then DrawRect(0.328,0.096,0.18,0.035,255,255,255,150) else DrawRect(0.328,0.096,0.18,0.035,0,0,0,150) end
		if cmenu.row == 2 then DrawRect(0.328,0.133,0.18,0.035,255,255,255,150) else DrawRect(0.328,0.133,0.18,0.035,0,0,0,150) end
		if cmenu.row == 3 then DrawRect(0.328,0.170,0.18,0.035,255,255,255,150) else DrawRect(0.328,0.170,0.18,0.035,0,0,0,150) end
		local draw_str = string.format("Opção: %d / 7", cmenu.field-1)
		drawTxt(0.328,0.093,0.175,0.035, 0.40,draw_str,255,255,255,255)
		--
		if GetNumberOfPedPropDrawableVariations(GetPlayerPed(-1), cmenu.field-1) ~= 0 and GetNumberOfPedPropDrawableVariations(GetPlayerPed(-1), cmenu.field-1) ~= false then
			DrawRect(0.328,0.142,0.175,0.014,255,128,0,150)
			local link = 0.138/(GetNumberOfPedPropDrawableVariations(GetPlayerPed(-1), cmenu.field-1)-1)
			local new_x = (bar.x-0.069)+(link*prop[1])
			if new_x < 0.259 then new_x = 0.259 end
			if new_x > 0.397 then new_x = 0.397 end
			DrawRect(new_x,bar.y,bar.x1,bar.y1,255,250,205,150)
			-- row_3
			DrawRect(0.328,0.179,0.175,0.014,255,128,0,150) -- bar_main
			local link = 0.138/(GetNumberOfPedPropTextureVariations(GetPlayerPed(-1), cmenu.field-1, prop[1])-1)
			local new_x = (bar.x-0.069)+(link*prop[2])
			if new_x < 0.259 then new_x = 0.259 end
			if new_x > 0.397 then new_x = 0.397 end
			DrawRect(new_x,bar.y+0.037,bar.x1,bar.y1,255,250,205,150)
			--
			if IsControlJustPressed(1, 189) or IsDisabledControlJustPressed(1, 189) then -- left
				PlaySound(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET", 0, 0, 1)
				if cmenu.row == 1 then
					if cmenu.field > 1 then 
						cmenu.field = cmenu.field-1 
						prop[1] = 0
						prop[2] = 0
					else 
						cmenu.field = 8
						prop[1] = 0
						prop[2] = 0
					end
				elseif cmenu.row == 2 then
					if prop[1] > 0 then prop[1] = prop[1]-1 else prop[1] = 0 end
					prop[2] = 0
					SetPedPropIndex(GetPlayerPed(-1), cmenu.field-1, prop[1], prop[2], prop[3])
				elseif cmenu.row == 3 then
					if prop[2] > 0 then prop[2] = prop[2]-1 else prop[2] = 0 end
					SetPedPropIndex(GetPlayerPed(-1), cmenu.field-1, prop[1], prop[2], prop[3])
				end
			end
			if IsControlJustPressed(1, 190) or IsDisabledControlJustPressed(1, 190) then -- right
				PlaySound(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET", 0, 0, 1)
				if cmenu.row == 1 then
					if cmenu.field < 8 then 
						cmenu.field = cmenu.field+1 
						prop[1] = 0
						prop[2] = 0
					else 
						cmenu.field = 1
						prop[1] = 0
						prop[2] = 0
					end
				elseif cmenu.row == 2 then
					if prop[1] < GetNumberOfPedPropDrawableVariations(GetPlayerPed(-1), cmenu.field-1)-1 then prop[1] = prop[1]+1 else prop[1] = GetNumberOfPedPropDrawableVariations(GetPlayerPed(-1), cmenu.field-1)-1 end
					prop[2] = 0
					SetPedPropIndex(GetPlayerPed(-1), cmenu.field-1, prop[1], prop[2], prop[3])
				elseif cmenu.row == 3 then
					if prop[2] < GetNumberOfPedPropTextureVariations(GetPlayerPed(-1), cmenu.field-1, prop[1])-1 then prop[2] = prop[2]+1 else prop[2] = GetNumberOfPedPropTextureVariations(GetPlayerPed(-1), cmenu.field-1, prop[1])-1 end
					SetPedPropIndex(GetPlayerPed(-1), cmenu.field-1, prop[1], prop[2], prop[3])
				end
			end
		else
			drawTxt(0.328,0.130,0.175,0.035, 0.40,"Vazio",255,255,255,255)
			drawTxt(0.328,0.167,0.175,0.035, 0.40,"Vazio",255,255,255,255)
			if IsControlJustPressed(1, 189) or IsDisabledControlJustPressed(1, 189) then -- left
				PlaySound(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET", 0, 0, 1)
				if cmenu.field > 1 then 
					cmenu.field = cmenu.field-1
					prop[1] = 0
					prop[2] = 0
				else 
					cmenu.field = 8
					prop[1] = 0
					prop[2] = 0
				end
			end
			if IsControlJustPressed(1, 190) or IsDisabledControlJustPressed(1, 190) then -- right
				PlaySound(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET", 0, 0, 1)
				if cmenu.field < 8 then 
					cmenu.field = cmenu.field+1
					prop[1] = 0
					prop[2] = 0
				else 
					cmenu.field = 1
					prop[1] = 0
					prop[2] = 0
				end
			end
		end
	elseif cmenu.show == 4 then
		DrawRect(0.12,0.135,0.22,0.035,0,0,0,150)
		DrawRect(0.12,0.172,0.22,0.035,0,0,0,150)
		DrawRect(0.12,0.209,0.22,0.035,255,255,255,150)
		DrawRect(0.12,0.246,0.22,0.035,0,0,0,150)
		drawTxt(0.177, 0.128, 0.25, 0.03, 0.40,"Roupa",255,255,255,255)
		drawTxt(0.177, 0.165, 0.25, 0.03, 0.40,"Acessórios",255,255,255,255)
		drawTxt(0.177, 0.202, 0.25, 0.03, 0.40,"~b~Selecionar Personagem",255,255,255,255)
		drawTxt(0.177, 0.239, 0.25, 0.03, 0.40,"Sair",255,255,255,255)
		DrawRect(0.328,0.051,0.18,0.049,0,0,0,150)
		drawTxt(0.382,0.048,0.235,0.035, 0.40,"SELECIONAR SKIN",255,255,255,255)
		DrawRect(0.328,0.024,0.175,0.005,255,128,0,150)
		if cmenu.row == 1 then DrawRect(0.328,0.096,0.18,0.035,255,255,255,150) else DrawRect(0.328,0.096,0.18,0.035,0,0,0,150) end
		if cmenu.row == 2 then DrawRect(0.328,0.133,0.18,0.035,255,255,255,150) else DrawRect(0.328,0.133,0.18,0.035,0,0,0,150) end
		if cmenu.row == 3 then DrawRect(0.328,0.170,0.18,0.035,255,255,255,150) else DrawRect(0.328,0.170,0.18,0.035,0,0,0,150) end
		local draw_str = string.format("Opção: < %d / 477 >", model_id)
		drawTxt(0.328,0.093,0.175,0.035, 0.40,draw_str,255,255,255,255)
		draw_str = string.format("%s", fr_skins[model_id])
		drawTxt(0.328,0.093+0.037,0.175,0.035, 0.40,draw_str,255,255,255,255)
		drawTxt(0.328,0.093+0.037*2,0.175,0.035, 0.40,"Nome do Modelo",255,255,255,255)
		if IsControlJustPressed(1, 189) or IsDisabledControlJustPressed(1, 189) then -- left
			if cmenu.row == 1 then
				if model_id > 1 then
					model_id=model_id-1
				else
					model_id = 477
				end
			end
		end
		if IsControlJustPressed(1, 190) or IsDisabledControlJustPressed(1, 190) then -- right
			if cmenu.row == 1 then
				if model_id < 477 then
					model_id=model_id+1
				else
					model_id = 1
				end
			end
		end
		if IsControlJustPressed(1, 201) or IsDisabledControlJustPressed(1, 201) then -- Enter
			if cmenu.row == 1 or cmenu.row == 2 then
				ChangeToSkin(fr_skins[model_id])
			elseif cmenu.row == 3 then
				if text_in == 0 then
					text_in = 1
					DisplayOnscreenKeyboard(false, "FMMC_KEY_TIP8", "", "", "", "", "", 64)
				end
			end
		end
	end
end

function ShowRadarMessage(message)
	SetNotificationTextEntry("STRING")
	AddTextComponentString(message)
	DrawNotification(0,1)
end

function ChangeToSkin(skin)
	local model = GetHashKey(skin)
	if IsModelInCdimage(model) and IsModelValid(model) then
		RequestModel(model)
		while not HasModelLoaded(model) do
			Citizen.Wait(0)
		end
		SetPlayerModel(PlayerId(), model)
		SetPedRandomComponentVariation(GetPlayerPed(-1), true)
		SetModelAsNoLongerNeeded(model)
	else
		exports.pNotify:SendNotification({
			text = "Modelo não encontrado",
			type = "info",timeout = 3000,progressBar = false, layout = "zezao",queue = "left",
			animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
		})
	end
end

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(1)
		for i = 1, #Roupas do
			local Coordenadas = GetEntityCoords( GetPlayerPed(-1) )
			local Distancia = GetDistanceBetweenCoords(Coordenadas.x, Coordenadas.y, Coordenadas.z, Roupas[i]["X"], Roupas[i]["Y"], Roupas[i]["Z"], true)
			if Distancia < 30.0 then
				Opacidade = math.floor(255 - (Distancia * 30))
				Texto3D(Roupas[i]["X"], Roupas[i]["Y"], Roupas[i]["Z"]+1.3, "~w~PRESSIONE ~y~[ E ]  ~w~PARA ACESSAR A LOJA DE ROUPAS", Opacidade)
				DrawMarker(27,Roupas[i]["X"], Roupas[i]["Y"], Roupas[i]["Z"], 0, 0, 0, 0, 0, 0, 1.501, 1.5001, 0.5001, 255, 255, 255, Opacidade, 0, 0, 0, 0)
			end
		end
	end
end)

function Texto3D(x,y,z, text, Opacidade)
    local onScreen,_x,_y=World3dToScreen2d(x,y,z)
    local px,py,pz=table.unpack(GetGameplayCamCoords())    
    if onScreen then
        SetTextScale(0.7, 0.7)
        SetTextFont(4)
        SetTextProportional(1)
        SetTextColour(255, 255, 255, Opacidade)
        SetTextDropshadow(0, 0, 0, 0, Opacidade)
        SetTextEdge(2, 0, 0, 0, 150)
        SetTextDropShadow()
        SetTextOutline()
        SetTextEntry("STRING")
        SetTextCentre(1)
        AddTextComponentString(text)
        DrawText(_x,_y)
    end
end

Citizen.CreateThread(function()
	while true do
		Wait(0)
		for i = 1, #Roupas do
			local playerCoords = GetEntityCoords(GetPlayerPed(-1), true)
			if GetDistanceBetweenCoords(playerCoords.x, playerCoords.y, playerCoords.z, Roupas[i]["X"], Roupas[i]["Y"], Roupas[i]["Z"], true) <= 5.0 then
				if IsControlJustPressed(0, 38) then
					PlaySound(-1, "FocusIn", "HintCamSounds", 0, 0, 1)
					TriggerServerEvent('clothes:checkmoney')
				end
			end
		end
		
		if cmenu.show == 1 then
			ClothShop()
			if IsControlJustPressed(1, 188) or IsDisabledControlJustPressed(1, 188) then -- up
				if cmenu.row > 1 then cmenu.row = cmenu.row-1 else cmenu.row = 4 end
				PlaySound(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET", 0, 0, 1)
			elseif IsControlJustPressed(1, 187) or IsDisabledControlJustPressed(1, 187) then -- down
				if cmenu.row < 4 then cmenu.row = cmenu.row+1 else cmenu.row = 1 end
				PlaySound(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET", 0, 0, 1)
			elseif IsControlJustPressed(1, 201) or IsDisabledControlJustPressed(1, 201) then -- Enter
				PlaySound(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET", 0, 0, 1)
				if cmenu.row == 1 then
					cmenu.show = 2
					cmenu.row = 1
				elseif cmenu.row == 2 then
					cmenu.show = 3
					cmenu.row = 1
				elseif cmenu.row == 3 then
					cmenu.show = 4
					cmenu.row = 1
				elseif cmenu.row == 4 then
					cmenu.show = 0
					aberto = false

					ClearPedTasks(GetPlayerPed(-1))
					cmenu.row = 0
					cmenu.field = 0
				end
			end
		elseif cmenu.show == 2 then
			ClothShop()
			if IsControlJustPressed(1, 188) or IsDisabledControlJustPressed(1, 188) then -- up
				if cmenu.row > 1 then cmenu.row = cmenu.row-1 else cmenu.row = 4 end
				PlaySound(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET", 0, 0, 1)
			elseif IsControlJustPressed(1, 187) or IsDisabledControlJustPressed(1, 187) then -- down
				if cmenu.row < 4 then cmenu.row = cmenu.row+1 else cmenu.row = 1 end
				PlaySound(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET", 0, 0, 1)
			elseif IsControlJustPressed(1, 202) or IsDisabledControlJustPressed(1, 202) then -- backspase
				PlaySound(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET", 0, 0, 1)
				cmenu.show = 1
				cmenu.row = 1
				cmenu.field = 1
			end
		elseif cmenu.show == 3 then
			ClothShop()
			if IsControlJustPressed(1, 188) or IsDisabledControlJustPressed(1, 188) then -- up
				if cmenu.row > 1 then cmenu.row = cmenu.row-1 else cmenu.row = 3 end
				PlaySound(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET", 0, 0, 1)
			elseif IsControlJustPressed(1, 187) or IsDisabledControlJustPressed(1, 187) then -- down
				if cmenu.row < 3 then cmenu.row = cmenu.row+1 else cmenu.row = 1 end
				PlaySound(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET", 0, 0, 1)
			elseif IsControlJustPressed(1, 202) or IsDisabledControlJustPressed(1, 202) then -- backspase
				PlaySound(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET", 0, 0, 1)
				cmenu.show = 1
				cmenu.row = 2
				cmenu.field = 1
			end
		elseif cmenu.show == 4 then
			if text_in == 1 then
				if UpdateOnscreenKeyboard() == 3 then text_in = 0
				elseif UpdateOnscreenKeyboard() == 1 then 
					text_in = 0
					ChangeToSkin(GetOnscreenKeyboardResult())
				elseif UpdateOnscreenKeyboard() == 2 then text_in = 0 end
			else
				ClothShop()

				if IsControlJustPressed(1, 188) or IsDisabledControlJustPressed(1, 188) then -- up
					if cmenu.row > 1 then cmenu.row = cmenu.row-1 else cmenu.row = 3 end
					PlaySound(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET", 0, 0, 1)
				elseif IsControlJustPressed(1, 187) or IsDisabledControlJustPressed(1, 187) then -- down
					if cmenu.row < 3 then cmenu.row = cmenu.row+1 else cmenu.row = 1 end
					PlaySound(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET", 0, 0, 1)
				elseif IsControlJustPressed(1, 202) or IsDisabledControlJustPressed(1, 202) then -- backspase
					PlaySound(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET", 0, 0, 1)
					cmenu.show = 1
					cmenu.row = 3
					cmenu.field = 1
				end
			end
		end
	end
end)

RegisterNetEvent('clothes:success')
AddEventHandler('clothes:success', function()
	startClothes()
	FreezeEntityPosition(GetPlayerPed(-1),false)
	RequestAnimDict("anim@random@shop_clothes@watches")
	while not HasAnimDictLoaded("anim@random@shop_clothes@watches") do
		Citizen.Wait(100)
	end
    TaskPlayAnim(GetPlayerPed(-1),"anim@random@shop_clothes@watches","intro", 8.0, -8, -1, 49, 0, -1, -1, -1)
    SetEntityCollision(GetPlayerPed(-1),true,true)    
end)