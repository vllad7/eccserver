local options = {
    x = 0.8, 
    y = 0.080, 
    width = 0.2, 
    height = 0.04, 
    scale = 0.4, 
    font = 0, 
    color_r = 0,
    color_g = 191, 
    color_b = 255, 
}

function Main()
    ClearMenu()
    Menu.addButton("AK-47", "comprarmunicao", {name = "municao-AK-47-dp"})
    Menu.addButton("M4", "comprarmunicao", {name = "municao-m4-dp"})
    Menu.addButton("Pistola", "comprarmunicao", {name = "municao-pistola-dp"})
    Menu.addButton("Pistola SNS", "comprarmunicao", {name = "municao-pistola-sns-dp"})
    Menu.addButton("12", "comprarmunicao", {name = "municao-12-dp"}) 
    Menu.addButton("SMG", "comprarmunicao", {name = "municao-smg-dp"})
end

function comprarmunicao(item)
    TriggerServerEvent("zezao:municaodp", item)
end

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- MENU PARA COMPRAR
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
municao = {
    {name = "Munição DP", colour = 1, id = 110, x = 456.07,y = -979.69,z = 29.78}, 
}

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)
        local pos = GetEntityCoords(GetPlayerPed(-1), true)
        local Distancia = GetDistanceBetweenCoords(pos.x, pos.y, pos.z, 456.07,-979.69,30.68, true)
        for k, v in ipairs(municao) do
            if(Vdist(pos.x, pos.y, pos.z, v.x, v.y, v.z) < 15.0)then
                Opacidade = math.floor(255 - (Distancia * 20))
                Texto3D(v.x, v.y, v.z+1.0, "PRESSIONE ~b~[E] ~w~PARA PEGAR MUNIÇÃO", Opacidade)
                DrawMarker(27, v.x, v.y, v.z, 0, 0, 0, 0, 0, 0, 1.5, 1.5, 0.6, 255, 255, 255, Opacidade, 0, 0, 0, 0)
                if(Vdist(pos.x, pos.y, pos.z, v.x, v.y, v.z) < 1.2)then
                        if IsControlJustReleased(1, 51) then
                            Main()
                            Menu.selection = 1
                            Menu.hidden = not Menu.hidden -- Hide/Show the menu
                        end
                    Menu.renderGUI(options) -- Draw menu on each tick if Menu.hidden = false
                elseif(Vdist(pos.x, pos.y, pos.z, v.x, v.y, v.z) > 1.0)then
                    Menu.hidden = true
                end
            end
        end
    end
end)
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- MENSAGENS
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function TextoDIVTitulo(x, y, width, height, scale, text, r, g, b, a, outline)
    SetTextFont(4)
    SetTextProportional(0)
    SetTextScale(scale, scale)
    SetTextColour(r, g, b, a)
    SetTextDropShadow(0, 0, 0, 0, 255)
    SetTextEdge(1, 0, 0, 0, 255)
    SetTextDropShadow()
    if(outline)then
        SetTextOutline()
    end
    SetTextEntry("STRING")
    AddTextComponentString(text)
    DrawText(x - width / 2, y - height / 2 + 0.005)
end

function Texto3D(x,y,z, text, Opacidade)
	local onScreen,_x,_y=World3dToScreen2d(x,y,z)
	local px,py,pz=table.unpack(GetGameplayCamCoords())    
	if onScreen then
		SetTextScale(0.7, 0.7)
		SetTextFont(4)
		SetTextProportional(1)
		SetTextColour(255, 255, 255, Opacidade)
		SetTextDropshadow(0, 0, 0, 0, Opacidade)
		SetTextEdge(2, 0, 0, 0, 150)
		SetTextDropShadow()
		SetTextOutline()
		SetTextEntry("STRING")
		SetTextCentre(1)
		AddTextComponentString(text)
		DrawText(_x,_y)
	end
end

function DrawR(xMin, xMax, yMin, yMax, color1, color2, color3, color4)
    DrawRect(xMin, yMin, xMax, yMax, color1, color2, color3, color4); 
end

Citizen.CreateThread(function()
    while true do
        if not Menu.hidden then 
            TextoDIVTitulo(1.245, 0.632, 1.0, 1.0, 0.8, "MUNIÇÃO DP", 255, 255, 255, 255, true)
        end
        Citizen.Wait(0)
    end
end)

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)
        if IsEntityDead(GetPlayerPed(-1)) then
            Menu.hidden = true
            Citizen.Wait(5000)
        end
    end
end)