-- DEFAULT --
local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP")
vRPhud = {}
Tunnel.bindInterface("Barras",vRPhud)

function vRPhud.checkHunger()
  local user_id = vRP.getUserId(source)
  return vRP.getHunger(user_id)
end

function vRPhud.checkThirst()
  local user_id = vRP.getUserId(source)
  return vRP.getThirst(user_id)
end

function vRPhud.checkSono()
  local user_id = vRP.getUserId(source)
  return vRP.getSono(user_id)
end

function vRPhud.checkNecessidades()
  local user_id = vRP.getUserId(source)
  return vRP.getMijar(user_id)
end