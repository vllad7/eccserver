
local lang = {
  garage = {
    buy = {
	  item = "{2} {1}<br /><br />{3}",
	  request = "Tem certeza de que deseja comprar este veículo?",
	},
    keys = {
	  title = "Chaves",
	  key = "Chave ({1})",
	  description = "Verifique as chaves do seu veículo",
	  sell = {
	    title = "Vender",
		prompt = "Valor da oferta:",
	    owned = "Veículo já possuido",
		request = "Você aceita a oferta de comprar um {1} por {2}?",
		description = "Oferecer para vender o veículo ao jogador mais próximo"
	  }
	},
    personal = {
	  client = {
	    locked = "Veículo trancado",
		unlocked = "Veículo destrancado"
	  },
	  out = "Você já possui um veículo fora da garagem",
	  apreendido = "Seu veiculo foi apreendido, retire-o no pátio do C.E.T",
	  bought = "Veículo enviado para sua garagem",
	  sold = "Veículo vendido",
	  stored = "Veículo guardado",
	  toofar = "Veículo muito distante"	  
	},
	showroom = {
	  title = "Showroom",
	  description = "Pressione para a direita para ver o carro e entrar para comprá-lo"
	},
    shop = {
	  title = "Shop",
	  description = "Percorrer as modificações do veículo",
	  client = {
	    nomods = "~r~Nenhuma modificação deste tipo para este veículo",
		maximum = "Você atingiu o valor ~y~máximo~w~ para esta modificação",
		minimum = "Você atingiu o valor ~y~máximo~w~ para esta modificação",
	    toggle = {
		  applied = "~g~Modificação aplicada",
		  removed = "~r~Modificação removida"
		}
	  },
	  mods = {
	    title = "Modificações",
		info = "Percorrer modificações",
	  },
	  repair = {
	    title = "Reparar",
		info = "Repare seu veículo",
	  },
	  colour = {
	    title = "Cor",
		info = "Percorrer as cores do veículo",
		primary = "Cor primária",
		secondary = "Cor secundária",
	    extra = {
		  title = "Cor Extra",
		  info = "Percorrer cores extras",
	      pearlescent = "Cor Pearlescent",
	      wheel = "Cor da roda",
	      smoke = "Cor Smoke",
		},
		custom = {
		  primary = "Cor primária personalizada",
		  secondary = "Cor secundária personalizada",
		},
	  },
	  neon = {
	    title = "Luz neon",
		info = "Mude as luzes de néon",
	    front = "Neon frente",
	    back = "Neon atrás",
	    left = "Neon esquerda",
	    right = "Neon direita",
	    colour = "Neon Cor"
	  }
	}
  }
}

return lang
