local cfg = {}

cfg.groups = {
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- ADMINISTRAÇÃO
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    ["superadmin"] = {
        _config = {
            title = "Super Admin",
            gtype = "admin"
        },
        "adicionar.grupo",
        "remover.grupo"
    },

    ["admin"] = {
        _config = {
            title = "Administrador",
            gtype = "admin"
        },
        "mostrar.customizacao",
        "criar.item",
        "pegar.dinheiro",
        "teleportar.coordenadas",
        "teleportar.ate.jogador",
        "teleportar.ate.mim",
        "pegar.coordenadas",
        "som.customizado",
        "animacao.customizada",
        "no.clip",
        "desbanir.jogador",
        "banir.jogador",
        "kickar.jogador",
        "remover.whitelist",
        "remover.jogador.grupo",
        "adicionar.jogador.grupo",
        "adicionar.whitelist",
        "lista.de.jogadores",
        "tickets.administracao",
        "god.mode",
        "teleportar.marcacao",
        "deletar.veiculo",
        "spawnar.veiculo",
        "mostrar.jogadores",
        "soltar.prisao",
        "colocar.obstaculos",
        "congelar.jogador",
        "coordenadas.txt",
        "soltar.admin",
        "congelar.admin",
        "adicionar.grupo",
        "remover.grupo",
        "spikes.admin",
        "blips.admin",
        "sprites.admin"
	},
	
	["god"] = {
        "admin.god"
	},
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- ESTE GRUPO É PARA CANCELAR OU ATIVAR AS MISSÕES (NÃO REMOVA)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    ["servico"] = {
    },    
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- GRUPOS DE VIP (MEXA APENAS NAS PERMISSÕES)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	["Platina"] = {
        _config = {
            title = "Platina",
            onspawn = function(player) end,
            gtype = "vip"
        },
        "carros.vip"
	},
	
	["Diamante"] = {
        _config = {
            title = "Diamante",
            onspawn = function(player) end,
            gtype = "vip"
        },
        "carros.vip"
	},
	
	["Ouro"] = {
        _config = {
            title = "Ouro",
            onspawn = function(player) end,
            gtype = "vip"
        },
        "carros.vip"
	},
	
	["Prata"] = {
        _config = {
            title = "Prata",
            onspawn = function(player) end,
            gtype = "vip"
        },
        "carros.vip"
    },

    ["Bronze"] = {
        _config = {
            title = "Bronze",
            onspawn = function(player) end,
            gtype = "vip"
        },
        "carros.vip"
    },
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- JOGADOR COMUM
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    ["usuario"] = {
        "chamar.administrador",
        "menu.pessoal",
        "telefone.jogador",
        "Guardar.Armas",
        "mensagem.grupo",
        "Empacotar.Dinheiro",
        "mascara.jogador",
        "minha.casa",
        "ver.casa",
        "contas.cidadao",
        "Saquear.Cadaver",
        "Guardar.Colete",
        "Equipar.Colete",
        "Revistar.Jogador",
        "ativar.missoes",
        "revistar.pessoal"
    },
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- ORGANIZAÇÕES (POLICIA)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    ["capitão"] = {
        _config = {
			title = "Capitão",
            gtype = "organizacao",
            onspawn = function(player) end,
            onjoin = function(player) vRPclient._setCop(player,true) end,
            onspawn = function(player) vRPclient._setCop(player,true) end,
            onleave = function(player) vRPclient._setCop(player,false) end
        },
        -- RESOURCES/VRP/MODULES/POLICE
        "menu.policial",
        "policial.colocar.veiculo",
        "policial.remover.veiculo",
        "policial.revistar",
        "policial.apreender.armas",
        "policial.apreender.itens",
        "policial.multar",
        "policial.pedir.rg",
        "guardar.armas",
        "policial.procurado",
        "police.pc",
        "policial.seguir",
        "policial.apreender.itens",
        "policial.apreender.armas",
        "-police.seizable", -- negative permission, police can't seize itself, even if another group add the permission

        -- RESOURCES/MENU
        "Policial.Prender",
        "Policial.Soltar",
        "Policial.Algemar",
        "Policial.Spikes",
        "Policial.Congelar",

        -- RESOURCES/ROUBOS
        "policia.roubo",

        --RESOURCES/VRP_BASIC_MISSION
        "policial.patrulhar",
        "policial.prisioneiro",

        --RESOURCES/[LOJA DE ARMAS]/MUNICAODP
        "policial.pegar.municao",

        --RESOURCES/[LOJA DE ARMAS]/PORTE
        "Verificar.Porte",

        --RESOURCES/VRP_CMDS
        "policial.multas",
        "apreender.veiculo",
        "checar.placa",





        "Equipar.Capitao",
        "passar.radar",
        "multar.jogador",
        "policia.garagem",
        "remover.cnh",
        "pedir.cnh",
        "helicoptero.garagem",
        "policia.garagem",
        "policia.servico.chamar",
        "salario.capitao",
        "mission.police.transfer",
        "mission.police.patrol",
        "capitao.farda",
    },

    ["sargento"] = {
        _config = {
            title = "Sargento",
            onspawn = function(player) end,
			gtype = "organizacao",
            onjoin = function(player) vRPclient._setCop(player,true) end,
            onspawn = function(player) vRPclient._setCop(player,true) end,
            onleave = function(player) vRPclient._setCop(player,false) end
        },
        "Equipar.Sargento"
    },

    ["cadete"] = {
        _config = {
            title = "Cadete",
            onspawn = function(player) end,
			gtype = "organizacao",
            onjoin = function(player) vRPclient._setCop(player,true) end,
            onspawn = function(player) vRPclient._setCop(player,true) end,
            onleave = function(player) vRPclient._setCop(player,false) end
        },
        "Equipar.Cadete"
    },
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- ORGANIZAÇÕES (S.A.M.U)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    ["samu"] = {
        _config = {
            title = "SAMU",
            onspawn = function(player) end,
            gtype = "organizacao"
        },
        "menu.samu",
        "reanimar.jogador",
        "Equipar.Paramedico",
        "hospital.garagem",
        "samu.servico.chamar",
        "salario.samu",
        "paciente.missao"
    },
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- ORGANIZAÇÕES (ILEGAIS)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	["Comando Vermelho"] = {
		_config = {
            title = "Comando Vermelho",
            onspawn = function(player) end,
			gtype = "organizacao"
		},
		"invadir.favela"
	},

	["PCC"] = {
        _config = {
            title = "PCC",
            onspawn = function(player) end,
            gtype = "organizacao"
        },
        "invadir.favela"
    },
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- EMPREGOS LEGAIS (SECUNDÁRIOS)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    ["Caminhoneiro"] = {
        _config = {
            title = "Caminhoneiro",
            gtype = "secundario"
        },
        "caminhoneiro.trabalhar"
    },

    ["Mecânico"] = {
        _config = {
            title = "Mecânicos",
            gtype = "secundario"
        },
        "repair.service",
        "substituir.veiculo",
        "reparar.veiculo",
        "mecanico.garagem",
        "Equipar.Mecanico",
        "chamar.mecanico",
        "menu.mecanico",
        "reparar.satelites",
        "reparar.turbinas"
    },

    ["Uber"] = {
        _config = {
            title = "Uber",
            gtype = "secundario"
        },
        "uber.service",
        "Equipar.Uber",
        "chamar.uber",
        "uber.garagem",
        "uber.missao"
    },

    ["Pescador"] = {
        _config = {
            title = "Pescador",
            gtype = "secundario"
        },
        "pescador.trabalhar"
    },

    ["Uber Eats"] = {
        _config = {
            title = "Uber Eats",
            gtype = "secundario"
        },
        "entregar.comida",
        "uber.eats.garagem",
        "eats.missao"
	},
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- ESTE GRUPO É PARA O SCRIPT DE CAÇADOR QUE É VENDIDO FORA DA BASE
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	["Caçador"] = {
        _config = {
            title = "Caçador",
            gtype = "secundario"
        },
        "trabalho.cacador"
    },
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- EMPREGOS ILEGAIS
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    ["Traficante de Maconha"] = {
        _config = {
            title = "Traficante de Maconha",
            gtype = "secundario"
        },
        "colher.maconha",
        "processar.maconha",
        "vender.maconha",
        "entregar.maconha",
        "Bater.Carteira"
    },

    ["Traficante de Cocaina"] = {
        _config = {
            title = "Traficante de Cocaina",
            gtype = "secundario"
        },
        "processar.cocaina",
        "colher.cocaina",
        "vender.cocaina",
        "Bater.Carteira",
        "entregar.cocaina"
    },

    ["Hacker"] = {
        _config = {
            title = "Hacker",
            gtype = "secundario"
        },
        "mission.hacker.information",
        "hacker.hack",
        "hackear.cartao.credito",
        "Hackear.Jogador",
        "Bater.Carteira"
    },

    ["Traficante de Animais"] = {
        _config = {
            title = "Traficante de Animais",
            gtype = "secundario"
        },
        "apanhar.tartaruga",
        "vender.tartaruga"
    },

    ["Vendedor de Armas"] = {
        _config = {
            title = "Vendedor de Armas",
            gtype = "secundario"
        },
        "processar.arma",
        "aprimorar.arma",
        "pegar.cobre",
        "fundir.aço",
        "produzir.balas",
        "Bater.Carteira",
        "Entregar.Armas"
    },

    ["Ladrão de Carros"] = {
        _config = {
            title = "Ladrão de Carros",
            gtype = "secundario"
        },
        "missao.roubar.carros",
        "desmanchar.carro",
        "Arrombar.Carro",
        "Bater.Carteira",
        "Entregar.Carro"
    },

    ["Sedex"] = {
        _config = {
            title = "Sedex",
            gtype = "secundario"
        },
        "sedex.permissao"
    },

    ["Desempregado"] = {
        _config = {
            title = "Desempregado",
            gtype = "secundario"
        }
    }
}
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- SETA O 1° JOGADOR A ENTRAR APÓS WIPE DO BANCO A ESTES GRUPOS
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
cfg.users = {
    [1] = {
        "superadmin",
        "admin"
    }
}

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- EMPREGOS
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
cfg.selectors = {
    ["Agencia de Empregos"] = {
        _config = {x = -1082.1042480469, y = -247.56590270996, z = 37.663282775879, blipid = 351, blipcolor = 47},
        "Uber Eats",
        "Pescador",
        "Uber",
        "Mecânico",
        "Sedex",
        "Caminhoneiro",
        -- "Cacador", EMPREGO PARA QUEM TIVER O SCRIPT FORA PARTE DA BASE, CASO NÃO TENHA PODE REMOVER
        "Desempregado"
    },
    ["Empregos Ilegais"] = {
        _config = {x = 706.37066650391, y = -964.56231689453, z = 30.408256530762, blipid = 351, blipcolor = 1},
        "Vendedor de Armas",
        "Traficante de Maconha",
        "Traficante de Cocaina",
        "Hacker",
        "Traficante de Animais"
    }
}

return cfg
