ch_mobilepay = {function(player,choice)
	local user_id = vRP.getUserId(player)
	local menu = {}
	menu.name = "Meu Telefone"
	menu.css = {header_color = "rgba(255,128,0,0.99)"}
    menu.onclose = function(player) vRP.openMainMenu(player) end
	menu["Transferir Dinheiro"] = {
	  	function(player,choice) 
		local phone = vRP.prompt(player,"Numero do Telefone","")
		local identity = vRP.getUserIdentity(user_id)
		if phone ~= nil and phone ~= "" then 
			if phone == identity.phone then
				TriggerClientEvent("pNotify:SendNotification", player, {
					text = "Você não pode transferir para você mesmo",
					type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
					animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
				})
			else
				vRPbm.payPhoneNumber(user_id,phone)
			end
		else
			TriggerClientEvent("pNotify:SendNotification", player, {
				text = "Valor Inválido",
				type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
				animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
			})
		end
	end}
	local directory = vRP.getPhoneDirectory(user_id)
	for k,v in pairs(directory) do
	 	 menu[k] = {
	    	function(player,choice) 
		  		vRPbm.payPhoneNumber(user_id,v)
	    	end
	  	,v}
	end
	vRP.openMenu(player, menu)
end}

ch_mobilecharge = {function(player,choice) 
	local user_id = vRP.getUserId(player)
	local menu = {}
	menu.name = "Meu Telefone"
	menu.css = {header_color = "rgba(255,128,0,0.99)"}
    menu.onclose = function(player) vRP.openMainMenu(player) end
	menu["Gerar Cobrança"] = {
		function(player,choice) 
		local phone = vRP.prompt(player,"Numero do Telefone","")
		if phone ~= nil and phone ~= "" then 
			vRPbm.chargePhoneNumber(user_id,phone)
		else
			TriggerClientEvent("pNotify:SendNotification", player, {
				text = "Valor Inválido",
				type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
				animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
			})
		end
	end}
	local directory = vRP.getPhoneDirectory(user_id)
	for k,v in pairs(directory) do
	  	menu[k] = {
	    	function(player,choice) 
		  		vRPbm.chargePhoneNumber(user_id,v)
	    	end
	  	,v}
	end
	vRP.openMenu(player, menu)
end}