local cfg = {}

-- LISTA DE VEÍCULOS: https://wiki.fivem.net/wiki/Vehicles

cfg.lang = "en"
cfg.rent_factor = 0.1
cfg.sell_factor = 0.1

cfg.price = {
    repair = 500,
    colour = 100,
    extra = 100,
    neon = 100
}


cfg.items = {
}

-- configure garage types
cfg.adv_garages = {
    ["Concessionaria [Carros]"] = {
        _config = {gpay="wallet",gtype={"store"},vtype="car",blipid=326,blipcolor=3},
		--["NOME DE SPAWN"] = {"NOME QUE IRÁ APARECER PARA O PLAYER",VALOR DO VEÍCULO, "DESCRIÇÃO CASO QUEIRA"},
        ["laferrari15"] = {"LA Ferrari",0, ""},
        ["sultanrs"] = {"Sultan RS",0, ""}
    },

    ["Concessionaria [Motos]"] = {
        _config = {gpay="wallet",gtype={"store"},vtype="bike",blipid=326,blipcolor=3},
        ["bati2"] = {"Bati 801RR",0, ""},
        ["thrust"] = {"Thrust",0, ""}
    },

    ["Garagem [Carros]"]  = {
        _config = {gpay="wallet",gtype={"personal"},vtype="car",blipid=357,blipcolor=3},
    },

    ["Garagem [Motos]"]  = {
        _config = {gpay="wallet",gtype={"personal"},vtype="bike",blipid=357,blipcolor=3},
    },
    

    ["Samu"] = {
        _config = {gpay="bank",gtype={"rental"},vtype="car",blipid=50,blipcolor=3,permissions={"hospital.garagem"}},
        ["ambulance"] = {"Ambulancia",0, ""}
    },

    ["Águia"] = {
        _config = {gpay="bank",gtype={"rental"},vtype="car",radius=5.1,permissions={"policia.garagem"}},
        ["polmav"] = {"Helicoptero da Policia",0, ""}
    },

    ["Policia"] = {
        _config = {gpay="bank",gtype={"rental"},vtype="car",blipcolor=4,permissions={"policia.garagem"}},
		["police"] = {"Viatura 1",0, ""},
		["police2"] = {"Viatura 2",0, ""},
		["police3"] = {"Viatura 3",0, ""},
		["police4"] = {"Viatura 4",0, ""}
    },

    ["Mecanico"] = {
        _config = {gpay="bank",gtype={"rental"},vtype="car",blipid=85,blipcolor=31,permissions={"mecanico.garagem"}},
        ["towtruck"] = {"Guinho 1",0, ""},
        ["towtruck2"] = {"Guinho 2",0, ""}
    },

    ["Uber"] = {
        _config = {gpay="bank",gtype={"rental"},vtype="car",blipid=56,blipcolor=4,permissions={"uber.garagem"}},
        ["Asterope"] = {"Asterope",0, ""},
        ["Tailgater"] = {"Tailgater",0, ""}
    },

    ["Ranch Main"]  = {
        _config = {gpay="wallet",gtype={"personal"},vtype="car",blipid=357,blipcolor=3,ghome="Ranch Main"},
    },
    ["Rich Housing"]  = {
        _config = {gpay="wallet",gtype={"personal"},vtype="car",blipid=357,blipcolor=3,ghome="Rich Housing"},
    },
    ["Rich Housing 2"]  = {
        _config = {gpay="wallet",gtype={"personal"},vtype="car",blipid=357,blipcolor=3,ghome="Rich Housing 2"},
    },
    ["Basic Housing 1"]  = {
        _config = {gpay="wallet",gtype={"personal"},vtype="car",blipid=357,blipcolor=3,ghome="Basic Housing 1"},
    },
    ["Basic Housing 2"]  = {
        _config = {gpay="wallet",gtype={"personal"},vtype="car",blipid=357,blipcolor=3,ghome="Basic Housing 2"},
    },
    ["Regular House 1"]  = {
        _config = {gpay="wallet",gtype={"personal"},vtype="car",blipid=357,blipcolor=3,ghome="Regular House 1"},
    },
    ["Regular House 2"]  = {
        _config = {gpay="wallet",gtype={"personal"},vtype="car",blipid=357,blipcolor=3,ghome="Regular House 2"},
    },
    ["Regular House 3"]  = {
        _config = {gpay="wallet",gtype={"personal"},vtype="car",blipid=357,blipcolor=3,ghome="Regular House 3"},
    },
    ["Regular House 4"]  = {
        _config = {gpay="wallet",gtype={"personal"},vtype="car",blipid=357,blipcolor=3,ghome="Regular House 4"},
    },
    ["Regular House 5"]  = {
        _config = {gpay="wallet",gtype={"personal"},vtype="car",blipid=357,blipcolor=3,ghome="Regular House 5"},
    },
    ["Regular House 6"]  = {
        _config = {gpay="wallet",gtype={"personal"},vtype="car",blipid=357,blipcolor=3,ghome="Regular House 6"},
    },

    ["LS Customs"]  = {
        _config = {gpay="wallet",gtype={"shop"},vtype="car",blipid=72,blipcolor=7},
        _shop = {
            [0] = {"Spoilers",500,""},
            [1] = {"Front Bumper",500,""},
            [2] = {"Rear Bumper",500,""},
            [3] = {"Side Skirt",500,""},
            [4] = {"Exhaust",500,""},
            [5] = {"Frame",500,""},
            [6] = {"Grille",500,""},
            [7] = {"Hood",500,""},
            [8] = {"Fender",500,""},
            [9] = {"Right Fender",500,""},
            [10] = {"Roof",500,""},
            [11] = {"Engine",500,""},
            [12] = {"Brakes",500,""},
            [13] = {"Transmission",500,""},
            [14] = {"Horns",500,""},
            [15] = {"Suspension",500,""},
            [16] = {"Armor",500,""},
            [18] = {"Turbo",500,""},
            [20] = {"Tire Smoke",500,""},
            [22] = {"Xenon Headlights",500,""},
            [23] = {"Wheels",500,"Press enter to change wheel type"},
            [24] = {"Back Wheels (Bike)",500,""},
            [25] = {"Plateholders",500,""},
            [27] = {"Trims",500,""},
            [28] = {"Ornaments",500,""},
            [29] = {"Dashboards",500,""},
            [30] = {"Dials",500,""},
            [31] = {"Door Speakers",500,""},
            [32] = {"Seats",500,""},
            [33] = {"Steering Wheel",500,""},
            [34] = {"H Shift",500,""},
            [35] = {"Plates",500,""},
            [36] = {"Speakers",500,""},
            [37] = {"Trunks",500,""},
            [38] = {"Hydraulics",500,""},
            [39] = {"Engine Block",500,""},
            [40] = {"Air Filter",500,""},
            [41] = {"Struts",500,""},
            [42] = {"Arch Covers",500,""},
            [43] = {"Arials",500,""},
            [44] = {"Extra Trims",500,""},
            [45] = {"Tanks",500,""},
            [46] = {"Windows",500,""},
            [48] = {"Livery",500,""},
        }
    },
}

-- position garages on the map {garage_type,x,y,z}
cfg.garages = {
		{"Concessionaria [Carros]",-37.442855834961,-1100.5230712891,26.422353744507},
		{"Concessionaria [Motos]",-45.023914337158,-1097.7325439453,26.422367095947},		
        {"Uber",907.38049316406,-175.86546325684,74.130157470703},
        {"Mecanico",494.61499023438,-1328.7794189453,29.340810775757},
		{"Samu",-471.15466308594,-326.14950561523,34.363273620605},
		{"Policia",435.20669555664,-1014.9512329102,28.747278213501},
		{"Águia",450.04306030273,-981.55078125,43.691665649414},

        {"Garagem [Motos]",230.01,-796.76,30.59},
        {"Garagem [Carros]",215.32,-791.41,30.83},

        
        {"LS Customs",-337.3863,-136.9247,39.0737},
        {"LS Customs",-1155.536,-2007.183,13.244},
        {"LS Customs",731.8163,-1088.822,22.233},
        {"LS Customs",1175.04,2640.216,37.82177},
        {"LS Customs",110.8406,6626.568,32.287},

        {"Ranch Main", 1408.32495117188,1117.44665527344,114.737692260742},
        {"Rich Housing", -751.5107421875,365.883117675781,87.9666687011719},
        {"Rich Housing 2",  -81.860595703125,-809.427734375,36.4030570983887},
        {"Basic Housing 1", -635.4501953125,57.4368324279785,44.8587303161621},
        {"Basic Housing 2", -1448.18701171875,-514.856567382813,31.6881823348999},
        {"Regular House 1", 843.398803710938,-191.063568115234,72.6714935302734},
        {"Regular House 2",  174.276748657227,483.056274414063,142.339096069336},
        {"Regular House 3", -820.590148925781,184.175857543945,72.0921401977539},
        {"Regular House 4", -1858.14965820313,328.570861816406,88.6500091552734},
        {"Regular House 5", -25.002462387085,-1436.29431152344,30.6531391143799},
        {"Regular House 6", -2587,1930.97326660156,167.304656982422},
}

return cfg
