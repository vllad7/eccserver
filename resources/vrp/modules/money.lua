local lang = vRP.lang

vRP.prepare("vRP/money_tables", [[
CREATE TABLE IF NOT EXISTS vrp_user_moneys(
  user_id INTEGER,
  wallet INTEGER,
  bank INTEGER,
  CONSTRAINT pk_user_moneys PRIMARY KEY(user_id),
  CONSTRAINT fk_user_moneys_users FOREIGN KEY(user_id) REFERENCES vrp_users(id) ON DELETE CASCADE
);
]])

vRP.prepare("vRP/money_init_user","INSERT IGNORE INTO vrp_user_moneys(user_id,wallet,bank) VALUES(@user_id,@wallet,@bank)")
vRP.prepare("vRP/get_money","SELECT wallet,bank FROM vrp_user_moneys WHERE user_id = @user_id")
vRP.prepare("vRP/set_money","UPDATE vrp_user_moneys SET wallet = @wallet, bank = @bank WHERE user_id = @user_id")

async(function()
  	vRP.execute("vRP/money_tables")
end)

local cfg = module("cfg/money")

function vRP.getMoney(user_id)
	local tmp = vRP.getUserTmpTable(user_id)
	if tmp then
		return tmp.wallet or 0
	else
		return 0
	end
end

function vRP.setMoney(user_id,value)
	local tmp = vRP.getUserTmpTable(user_id)
	if tmp then
	  tmp.wallet = value
	end

	local source = vRP.getUserSource(user_id)
	if source then
	  vRPclient._setDivContent(source,"carteira",lang.money.display({value}))
	end
  end

function vRP.tryPayment(user_id,amount)
	local money = vRP.getMoney(user_id)
	if amount >= 0 and money >= amount then
		vRP.setMoney(user_id,money-amount)
		return true
	else
		return false
	end
end

function vRP.giveMoney(user_id,amount)
	if amount > 0 then
		local money = vRP.getMoney(user_id)
		vRP.setMoney(user_id,money+amount)
	end
end

function vRP.getBankMoney(user_id)
	local tmp = vRP.getUserTmpTable(user_id)
	if tmp then
		return tmp.bank or 0
	else
		return 0
	end
end

function vRP.setBankMoney(user_id,value)
	local tmp = vRP.getUserTmpTable(user_id)
	if tmp then
		tmp.bank = value
	end

	local source = vRP.getUserSource(user_id)
	if source then
	  vRPclient._setDivContent(source,"banco",lang.money.bdisplay({value}))
	end
end

function vRP.giveBankMoney(user_id,amount)
	if amount > 0 then
		local money = vRP.getBankMoney(user_id)
		vRP.setBankMoney(user_id,money+amount)
	end
end

function vRP.tryWithdraw(user_id,amount)
	local money = vRP.getBankMoney(user_id)
	if amount >= 0 and money >= amount then
		vRP.setBankMoney(user_id,money-amount)
		vRP.giveMoney(user_id,amount)
		return true
	else
		return false
	end
end

function vRP.tryDeposit(user_id,amount)
	if amount >= 0 and vRP.tryPayment(user_id,amount) then
		vRP.giveBankMoney(user_id,amount)
		return true
	else
		return false
	end
end

function vRP.tryFullPayment(user_id,amount)
	local money = vRP.getMoney(user_id)
	if money >= amount then
		return vRP.tryPayment(user_id, amount)
	else
		if vRP.tryWithdraw(user_id, amount-money) then
			return vRP.tryPayment(user_id, amount)
		end
	end
	return false
end

AddEventHandler("vRP:playerJoin",function(user_id,source,name,last_login)
	vRP.execute("vRP/money_init_user", {user_id = user_id, wallet = cfg.open_wallet, bank = cfg.open_bank})
	local tmp = vRP.getUserTmpTable(user_id)
	if tmp then
		local rows = vRP.query("vRP/get_money", {user_id = user_id})
		if #rows > 0 then
			tmp.bank = rows[1].bank
			tmp.wallet = rows[1].wallet
		end
	end
end)

AddEventHandler("vRP:playerLeave",function(user_id,source)
	local tmp = vRP.getUserTmpTable(user_id)
	if tmp and tmp.wallet and tmp.bank then
		vRP.execute("vRP/set_money", {user_id = user_id, wallet = tmp.wallet, bank = tmp.bank})
	end
end)

AddEventHandler("vRP:save", function()
	for k,v in pairs(vRP.user_tmp_tables) do
		if v.wallet and v.bank then
			vRP.execute("vRP/set_money", {user_id = k, wallet = v.wallet, bank = v.bank})
		end
	end
end)

AddEventHandler("vRP:playerSpawn",function(user_id, source, first_spawn)
	if first_spawn then
		--vRPclient._setDiv(source,"carteira",cfg.display_css,lang.money.display({vRP.getMoney(user_id)}))
		--vRPclient._setDiv(source,"banco",cfg.display_css,lang.money.bdisplay({vRP.getBankMoney(user_id)}))
	end
end)

local function ch_give(player,choice)
	local user_id = vRP.getUserId(player)
	if user_id then
		local nplayer = vRPclient.getNearestPlayer(player,10)
		if nplayer then
			local nuser_id = vRP.getUserId(nplayer)
			if nuser_id then
				local amount = vRP.prompt(player,"Quanto deseja enviar?","")
				local amount = parseInt(amount)
				if amount > 0 and vRP.tryPayment(user_id,amount) then
					vRP.giveMoney(nuser_id,amount)
					TriggerClientEvent("pNotify:SendNotification", player, {
						text = "Você enviou " ..amount .. " reais.",
						type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
						animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
					})

					TriggerClientEvent("pNotify:SendNotification", nplayer, {
						text = "Você recebeu " ..amount .. " reais.",
						type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
						animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
					})
				else
					TriggerClientEvent("pNotify:SendNotification", player, {
						text = "Dinheiro insuficiente",
						type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
						animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
					})
				end
			else
				TriggerClientEvent("pNotify:SendNotification", player, {
					text = "Não há nenhum jogador por perto",
					type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
					animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
				})
			end
		else
			TriggerClientEvent("pNotify:SendNotification", player, {
				text = "Não há nenhum jogador por perto",
				type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
				animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
			})
		end
	end
end

local function ch_guardard(player,choice)
	local user_id = vRP.getUserId(player)
	if user_id ~= nil then
		local amount = vRP.getMoney(user_id)
		if vRP.tryPayment(user_id, amount) then
			vRP.giveInventoryItem(user_id, "money", amount, true)
		end
	end
end

vRP.registerMenuBuilder("pessoal", function(add, data)
	local player = data.player
	local user_id = vRP.getUserId(player)
	if user_id then
		local choices = {}
		choices["Enviar Dinheiro"] = {ch_give}
		choices["Guardar Dinheiro"] = {ch_guardard}
		add(choices)
	end
end)