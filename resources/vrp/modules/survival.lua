local Tunnel = module("vrp", "lib/Tunnel")
HUDclient = Tunnel.getInterface("Barras") 

function vRP.getHunger(user_id)
	local data = vRP.getUserDataTable(user_id)
	if data then
		return data.hunger
	end
	return 0
end

function vRP.getThirst(user_id)
	local data = vRP.getUserDataTable(user_id)
	if data then
		return data.thirst
	end
	return 0
end

function vRP.getSono(user_id)
	local data = vRP.getUserDataTable(user_id)
	if data then
		return data.sono
	end
	return 0
end

function vRP.getNecessidades(user_id)
	local data = vRP.getUserDataTable(user_id)
	if data then
		return data.necessidades
	end
	return 0
end

function vRP.setHunger(user_id,value)
	local data = vRP.getUserDataTable(user_id)
	if data then
		data.hunger = value
		if data.hunger < 0 then data.hunger = 0
		elseif data.hunger > 100 then data.hunger = 100 
		end
		local source = vRP.getUserSource(user_id)
		HUDclient.setHunger(source,data.hunger)
	end
end

function vRP.setThirst(user_id,value)
	local data = vRP.getUserDataTable(user_id)
	if data then
		data.thirst = value
		if data.thirst < 0 then data.thirst = 0
		elseif data.thirst > 100 then data.thirst = 100 
		end
		local source = vRP.getUserSource(user_id)
		HUDclient.setThirst(source,data.thirst)
	end
end

function vRP.setSono(user_id,value)
	local data = vRP.getUserDataTable(user_id)
	if data then
		data.sono = value
		if data.sono < 0 then data.sono = 0
		elseif data.sono > 100 then data.sono = 100 
		end
		local source = vRP.getUserSource(user_id)
		HUDclient.setSono(source,data.sono)
	end
end

function vRP.setNecessidades(user_id,value)
	local data = vRP.getUserDataTable(user_id)
	if data then
		data.necessidades = value
		if data.necessidades < 0 then data.necessidades = 0
		elseif data.necessidades > 100 then data.necessidades = 100 
		end
		local source = vRP.getUserSource(user_id)
		HUDclient.setNecessidades(source,data.necessidades)
	end
end

function vRP.varyHunger(user_id, variation)
	local data = vRP.getUserDataTable(user_id)
	if data then
		local was_starving = data.hunger >= 100
		data.hunger = data.hunger + variation
		local is_starving = data.hunger >= 100
		local overflow = data.hunger-100
		if overflow > 0 then
			vRPclient._varyHealth(vRP.getUserSource(user_id),-overflow*2)
		end
		if data.hunger < 0 then data.hunger = 0
		elseif data.hunger > 100 then data.hunger = 100 
		end
		local source = vRP.getUserSource(user_id)
		HUDclient.setHunger(source,data.hunger)
	end
end

function vRP.varyThirst(user_id, variation)
	local data = vRP.getUserDataTable(user_id)
	if data then
		local was_thirsty = data.thirst >= 100
		data.thirst = data.thirst + variation
		local is_thirsty = data.thirst >= 100
		local overflow = data.thirst-100
		if overflow > 0 then
			vRPclient._varyHealth(vRP.getUserSource(user_id),-overflow*2)
		end
		if data.thirst < 0 then data.thirst = 0
		elseif data.thirst > 100 then data.thirst = 100 
		end
		local source = vRP.getUserSource(user_id)
		HUDclient.setThirst(source,data.thirst)
	end
end

function vRP.varySono(user_id, variation)
	local data = vRP.getUserDataTable(user_id)
	if data then
		local was_sono = data.sono >= 100
		data.sono = data.sono + variation
		local is_sono = data.sono >= 100
		local overflow = data.sono-100
		if overflow > 0 then
		vRPclient._playAnim(source,false,{task="WORLD_HUMAN_BUM_SLUMPED"},false)
		TriggerClientEvent("pNotify:SendNotification", source, {
			text = "Você desmaiou de sono",
			type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
			animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
		})
		if not deitado then
			deitado = true
			SetTimeout(30000, function()
			deitado = false
			local source = vRP. getUserSource(user_id)
			vRPclient._stopAnim(source,false)
			TriggerClientEvent("pNotify:SendNotification", source, {
				text = "Você acordou",
				type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
				animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
			})
			vRP.varySono(user_id,-30)
			end)
		end
		end

		if data.sono < 0 then 
		data.sono = 0
		elseif data.sono > 100 then 
		data.sono = 100 
		end

		local source = vRP.getUserSource(user_id)
		HUDclient.setSono(source,data.sono)
	end
end

function vRP.varyNecessidades(user_id, variation)
	local data = vRP.getUserDataTable(user_id)
	if data then
		local was_necessidades = data.necessidades >= 100
		data.necessidades = data.necessidades + variation
		local is_necessidades = data.necessidades >= 100
		local overflow = data.necessidades-100
		if overflow > 0 then
			vRPclient._playAnim(source, true,{{"missbigscore1switch_trevor_piss", "piss_loop", 1}},true)
			TriggerClientEvent("pNotify:SendNotification", source, {
				text = "Você está muito apertado, aguarde até sentir-se mais aliviado",
				type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
				animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
			})
			if not liberando then
				liberando = true
				SetTimeout(60000, function()
				liberando = false
				local source = vRP. getUserSource(user_id)
				vRPclient._stopAnim(source,false)
				vRP.varyNecessidades(user_id,-50)
				TriggerClientEvent("pNotify:SendNotification", source, {
					text = "Você se aliviou",
					type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
					animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
				})
				end)
			end
		end    
		if data.necessidades < 0 then data.necessidades = 0
		elseif data.necessidades > 100 then data.necessidades = 100 
		end
		local source = vRP.getUserSource(user_id)
		HUDclient.setNecessidades(source,data.necessidades)
	end
end

function tvRP.varyHunger(variation)
	local user_id = vRP.getUserId(source)
	if user_id then
		vRP.varyHunger(user_id,variation)
	end
end

function tvRP.varyThirst(variation)
	local user_id = vRP.getUserId(source)
	if user_id then
		vRP.varyThirst(user_id,variation)
	end
end

function tvRP.varySono(variation)
	local user_id = vRP.getUserId(source)
	if user_id then
		vRP.varySono(user_id,variation)
	end
end

function tvRP.varyNecessidades(variation)
  	local user_id = vRP.getUserId(source)
  	if user_id then
    	vRP.varyNecessidades(user_id,variation)
  	end
end

-- tasks

function task_update()
	for k,v in pairs(vRP.users) do
		vRP.varyHunger(v,1.0)
		vRP.varyThirst(v,1.0)
		vRP.varySono(v,0.3125)
		vRP.varyNecessidades(v,0.625)
	end
	SetTimeout(60000,task_update)
end

async(function()
  	task_update()
end)

AddEventHandler("vRP:playerJoin",function(user_id,source,name,last_login)
	local data = vRP.getUserDataTable(user_id)
	if data.hunger == nil then
		data.hunger = 0
		data.thirst = 0
		data.sono = 0
		data.necessidades = 0
	end
end)

AddEventHandler("vRP:playerSpawn",function(user_id, source, first_spawn)
  	local data = vRP.getUserDataTable(user_id)
  	vRPclient._setPolice(source,false)
  	vRPclient._setFriendlyFire(source,true)
  	vRP.setHunger(user_id, data.hunger)
  	vRP.setThirst(user_id, data.thirst)
  	vRP.setSono(user_id, data.sono)
  	vRP.setNecessidades(user_id, data.necessidades)
end)

local revive_seq = {
  	{"amb@medic@standing@kneel@enter","enter",1},
  	{"amb@medic@standing@kneel@idle_a","idle_a",1},
  	{"amb@medic@standing@kneel@exit","exit",1}
}

local choice_revive = {function(player,choice)
	local user_id = vRP.getUserId(player)
	if user_id then
		local nplayer = vRPclient.getNearestPlayer(player,10)
		local nuser_id = vRP.getUserId(nplayer)
		if nuser_id then
			if vRPclient.isInComa(nplayer) then
				if vRP.tryGetInventoryItem(user_id,"kit medico",1,true) then
					vRPclient._playAnim(player,false,revive_seq,false) -- anim
					SetTimeout(15000, function()
						vRPclient._varyHealth(nplayer,50) -- heal 50
					end)
				end
			else
				TriggerClientEvent("pNotify:SendNotification", user_id, {
				text = "Este jogador não está em coma",
				type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
				animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
				})
			end
		else
			TriggerClientEvent("pNotify:SendNotification", user_id, {
			text = "Não há nenhum jogador próximo de você",
			type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
			animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
			})
		end
	end
end}

vRP.registerMenuBuilder("main", function(add, data)
	local user_id = vRP.getUserId(data.player)
	if user_id then
		local choices = {}
		if vRP.hasPermission(user_id,"menu.samu") then
			choices["SAMU"] = {function(player,choice)
				local menu  = vRP.buildMenu("samu", {player = player})
				menu.name = "SAMU"
				menu.css={header_color="rgba(255,0,0,0.99)"}
				menu.onclose = function(player) vRP.openMainMenu(player) end

				vRP.openMenu(player,menu)
			end}
		end
		add(choices)
	end
end)

vRP.registerMenuBuilder("samu", function(add, data)
	local user_id = vRP.getUserId(data.player)
	if user_id then
		local choices = {}
		if vRP.hasPermission(user_id,"reanimar.jogador") then
		choices["Reanimar Jogador"] = choice_revive
		end

		add(choices)
	end
end)

local guardar_colete = {function(player,choice)
	local user_id = vRP.getUserId(player)
	local colete = vRPclient.checarColete(player)
	if user_id then
		if colete == 100 then
			TriggerClientEvent("pNotify:SendNotification", user_id, {
				text = "Você guardou seu colete",
				type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
				animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
			})
			vRP.giveInventoryItem(user_id, "colete", 1, false)
			vRPclient.tirarColete(player)
		else
			TriggerClientEvent("pNotify:SendNotification", user_id, {
				text = "Você não pode guardar um colete danificado",
				type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
				animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
			})
		end
	end
end}

local equipar_colete = {function(player,choice)
	local user_id = vRP.getUserId(player)
	if user_id then
		if vRP.tryGetInventoryItem(user_id,"colete", 1,false) then
			TriggerClientEvent("pNotify:SendNotification", user_id, {
				text = "Você equipou seu colete",
				type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
				animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
			})
			vRPclient.colocarColete(player)
		else
			TriggerClientEvent("pNotify:SendNotification", user_id, {
				text = "Você não possui colete para equipar",
				type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
				animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
			})
		end
	end
end}

vRP.registerMenuBuilder("pessoal", function(add, data)
    local user_id = vRP.getUserId(data.player)
    if user_id then
        local choices = {}
        if vRP.hasPermission(user_id,"Guardar.Colete") then
            choices["Guardar Colete"] = guardar_colete
		end
		
		if vRP.hasPermission(user_id,"Equipar.Colete") then
            choices["Equipar Colete"] = equipar_colete
        end

        add(choices)
    end
end)
