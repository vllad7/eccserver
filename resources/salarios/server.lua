local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

local cfg = module("salarios", "cfg/paycheck")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP")

function paycheck_giver()
    for k,v in pairs(cfg.salario) do
        local users = vRP.getUsersByPermission(k)
        for l,w in pairs(users) do
            local user_id = w
            local paycheck = v
            local player = vRP.getUserSource(user_id)
            vRP.giveBankMoney(user_id,paycheck)
            TriggerClientEvent("pNotify:SendNotification", player, {
                text = "Você recebeu " .. paycheck .. " reais de salario",
                type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
            })
        end
    end
    SetTimeout(1000*60*60, function()
        paycheck_giver()
    end)
end

Citizen.CreateThread(function()
    paycheck_giver()
end)

function paycheck_taker()
    for k,v in pairs(cfg.imposto) do
        local users = vRP.getUsersByPermission(k)
        for l,w in pairs(users) do
            local user_id = w
            local bill = v
            local player = vRP.getUserSource(user_id)
            local money = vRP.getBankMoney(user_id)
            vRP.setBankMoney(user_id,money-bill)
            TriggerClientEvent("pNotify:SendNotification", player, {
                text = "Você pagou " .. bill .. " reais de impostos",
                type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
            })
        end
    end
    SetTimeout(1000*60*30, function()
        paycheck_taker()
    end)
end

Citizen.CreateThread(function()
    paycheck_taker()
end)