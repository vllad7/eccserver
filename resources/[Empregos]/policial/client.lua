local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP")
Zezaoserver = Tunnel.getInterface("zezao")

contador = 0
Zezao = {}
Tunnel.bindInterface("policial",Zezao)

local Delegacia = { 
	{nome = "Delegacia", id = 60, cor = 3, x = 451.45, y = -979.35, z = 29.78},
}

Citizen.CreateThread(function()
	for _, item in pairs(Delegacia) do
      	item.blip = AddBlipForCoord(item.x, item.y, item.z)
      	SetBlipSprite(item.blip, item.id)
      	SetBlipColour(item.blip, item.cor)
      	SetBlipAsShortRange(item.blip, true)
      	BeginTextCommandSetBlipName("STRING")
      	AddTextComponentString(item.nome)
      	EndTextCommandSetBlipName(item.blip)
    end
end)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- CADETE
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
RegisterNetEvent("Policial:Cadete")
AddEventHandler("Policial:Cadete", function()
	TriggerServerEvent("Mensagem:Dentro")
    SetEntityHealth(GetPlayerPed(-1), 200)
    SetPedArmour(GetPlayerPed(-1), 100)
end)

RegisterNetEvent("Policial:SairCadete")
AddEventHandler("Policial:SairCadete", function()
	TriggerServerEvent("Mensagem:Fora")
end)

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- CAPITÃO
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
RegisterNetEvent("Policial:Capitao")
AddEventHandler("Policial:Capitao", function()
	TriggerServerEvent("Mensagem:Dentro")
    SetEntityHealth(GetPlayerPed(-1), 200)
    SetPedArmour(GetPlayerPed(-1), 100)
end)

RegisterNetEvent("Policial:SairCapitao")
AddEventHandler("Policial:SairCapitao", function()
	TriggerServerEvent("Mensagem:Fora")
end)

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- SARGENTO
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
RegisterNetEvent("Policial:Sargento")
AddEventHandler("Policial:Sargento", function()
	TriggerServerEvent("Mensagem:Dentro")
    SetEntityHealth(GetPlayerPed(-1), 200)
    SetPedArmour(GetPlayerPed(-1), 100)
end)

RegisterNetEvent("Policial:SairSargento")
AddEventHandler("Policial:SairSargento", function()
	TriggerServerEvent("Mensagem:Fora")
end)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- THREAD PRINCIPAL
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
local ServicoDP = {
	[0]  = { ["X"] = 451.45, ["Y"] = -979.35, ["Z"] = 31.53},
	[1]  = { ["X"] = 451.45, ["Y"] = -979.35, ["Z"] = 31.53}
}

ServicoDP = {
	{nome = "Serviço DP", cor = 3, id = 60, x = 451.45, y = -979.35, z = 29.83}, -- LOS SANTOS
	{nome = "Serviço DP", cor = 3, id = 60, x = 1849.49,y = 3689.12,z = 33.36}, -- PALETO BAY
}

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(1)
		for k, v in ipairs(ServicoDP) do
			local Coordenadas = GetEntityCoords(GetPlayerPed(-1))
			local Distancia = GetDistanceBetweenCoords(Coordenadas.x, Coordenadas.y, Coordenadas.z, v.x, v.y, v.z, true)
			if Distancia < 5.0 then
				Opacidade = math.floor(255 - (Distancia * 40))
				Texto3D(v.x, v.y, v.z+1.2, "~g~[ F ] ~w~PARA INICIAR O EXPEDIENTE / ~r~[ Y ] ~w~PARA SAIR DO EXPEDIENTE", Opacidade)
				DrawMarker(27, v.x, v.y, v.z, 0, 0, 0, 0, 0, 0, 1.501, 1.5001, 0.5001, 255, 255, 255, 155, 0, 0, 0, 0)
				if contador == 0 then
					Texto3D(v.x, v.y, v.z+0.8, "VOCÊ ESTÁ PRONTO PARA ENTRAR EM SERVIÇO", Opacidade)
					if (IsControlJustPressed(1,49)) then
						TriggerServerEvent('Policial:Emprego')				
						contador = 300
					end

					if (IsControlJustPressed(1,246)) then
						TriggerServerEvent('Policial:ForaEmprego')				
						contador = 300
						RemoveAllPedWeapons(GetPlayerPed(-1))
						SetPedArmour(GetPlayerPed(-1), 0)
					end
				else
					Texto3D(v.x, v.y, v.z+0.8, "AGUARDE ~r~".. contador .. " ~w~SEGUNDOS PARA TENTAR NOVAMENTE", Opacidade)
				end
			end
		end
	end
end)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- CONTADOR
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Citizen.CreateThread(function()
	while true do
		Wait(1000)
		if contador > 0 then
			contador = contador - 1
		end
	end
end)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- TEXTO 3D
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function Texto3D(x,y,z, text, Opacidade)
    local onScreen,_x,_y=World3dToScreen2d(x,y,z)
    local px,py,pz=table.unpack(GetGameplayCamCoords())    
    if onScreen then
        SetTextScale(0.54, 0.54)
        SetTextFont(4)
        SetTextProportional(1)
        SetTextColour(255, 255, 255, Opacidade)
        SetTextDropshadow(0, 0, 0, 0, Opacidade)
        SetTextEdge(2, 0, 0, 0, 150)
        SetTextDropShadow()
        SetTextOutline()
        SetTextEntry("STRING")
        SetTextCentre(1)
        AddTextComponentString(text)
        DrawText(_x,_y)
    end
end

function Texto3D(x,y,z, text, Opacidade)
    local onScreen,_x,_y=World3dToScreen2d(x,y,z)
    local px,py,pz=table.unpack(GetGameplayCamCoords())    
    if onScreen then
        SetTextScale(0.54, 0.54)
        SetTextFont(4)
        SetTextProportional(1)
        SetTextColour(255, 255, 255, Opacidade)
        SetTextDropshadow(0, 0, 0, 0, Opacidade)
        SetTextEdge(2, 0, 0, 0, 150)
        SetTextDropShadow()
        SetTextOutline()
        SetTextEntry("STRING")
        SetTextCentre(1)
        AddTextComponentString(text)
        DrawText(_x,_y)
    end
end