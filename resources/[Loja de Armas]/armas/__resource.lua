server_scripts {
	'@vrp/lib/utils.lua',
	'zezao.lua',
	'server.lua'
}

client_script {
	'client.lua',
	'zezao.lua',
	'@vrp/lib/utils.lua',
	'GUI.lua'
}