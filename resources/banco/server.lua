local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP","banco")
isTransfer = false

AddEventHandler("vRPcli:playerSpawned",function(user_id,source) 
	local bankbalance = vRP.getBankMoney(user_id)
	TriggerClientEvent('banco:updateBalance', source, bankbalance)
end)

RegisterServerEvent('playerSpawned')
AddEventHandler('playerSpawned', function()
	local user_id = vRP.getUserId(source)
	local bankbalance = vRP.getBankMoney(user_id)
	TriggerClientEvent('banco:updateBalance', source, bankbalance)
end)

	function bankBalance(player)
	return vRP.getBankMoney(vRP.getUserId(player))
end

function deposit(player, amount)
	local bankbalance = bankBalance(player)
	local new_balance = bankbalance + math.abs(amount)

	local user_id = vRP.getUserId(player)
	TriggerClientEvent("banco:updateBalance", source, new_balance)
	vRP.tryDeposit(user_id,math.floor(math.abs(amount)))
end

function withdraw(player, amount)
	local bankbalance = bankBalance(player)
	local new_balance = bankbalance - math.abs(amount)

	local user_id = vRP.getUserId(player)
	TriggerClientEvent("banco:updateBalance", source, new_balance)
	vRP.tryWithdraw(user_id,math.floor(math.abs(amount)))
end

function transfer (fPlayer, tPlayer, amount)
	local bankbalance = bankBalance(fPlayer)
	local bankbalance2 = bankBalance(tPlayer)
	local new_balance = bankbalance - math.abs(amount)
	local new_balance2 = bankbalance2 + math.abs(amount)

	local user_id = vRP.getUserId(fPlayer)
	local user_id2 = vRP.getUserId(tPlayer)

	vRP.setBankMoney(user_id, new_balance)
	vRP.setBankMoney(user_id2, new_balance2)

	TriggerClientEvent("banco:updateBalance", fPlayer, new_balance)
	TriggerClientEvent("banco:updateBalance", tPlayer, new_balance2)
end

function round(num, numDecimalPlaces)
	local mult = 5^(numDecimalPlaces or 0)
	return math.floor(num * mult + 0.5) / mult
end

RegisterServerEvent('bank:update')
AddEventHandler('bank:update', function()
	local user_id = vRP.getUserId(source)
	local source = vRP.getUserSource(user_id)
	local bankbalance = vRP.getBankMoney(user_id)
	TriggerClientEvent("banco:updateBalance", source, bankbalance)
end)

RegisterServerEvent('bank:deposit')
AddEventHandler('bank:deposit', function(amount)
	if not amount then return end
		local user_id = vRP.getUserId(source)
		local source = vRP.getUserSource(user_id)
		local wallet = vRP.getMoney(user_id)
		local rounded = math.ceil(tonumber(amount))

		if(string.len(rounded) >= 9) then
		TriggerClientEvent("pNotify:SendNotification", user_id, {
			text = "Valor muito alto, tente um valor mais baixo",
			type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
			animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
		})
	else
		local bankbalance = vRP.getBankMoney(user_id)
		if(rounded <= wallet) then
			TriggerClientEvent("banco:updateBalance", source, (bankbalance + rounded))
			TriggerClientEvent("banco:addBalance", source, rounded)

			deposit(source, rounded)
		else
			TriggerClientEvent("pNotify:SendNotification", user_id, {
				text = "Dinheiro Insuficiente",
				type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
				animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
			})
		end
	end
end)


RegisterServerEvent('bank:withdraw')
AddEventHandler('bank:withdraw', function(amount)
	if not amount then return end
		local user_id = vRP.getUserId(source)
		local source = vRP.getUserSource(user_id)
		local rounded = round(tonumber(amount), 0)
		if(string.len(rounded) >= 9) then
			TriggerClientEvent("pNotify:SendNotification", user_id, {
				text = "Valor muito alto, tente um valor mais baixo",
				type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
				animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
			})
	else
		local bankbalance = vRP.getBankMoney(user_id)
		if(tonumber(rounded) <= tonumber(bankbalance)) then
			TriggerClientEvent("banco:updateBalance", source, (vRP.getBankMoney(user_id) - rounded))
			TriggerClientEvent("banco:removeBalance", source, rounded)

			withdraw(source, rounded)
		else
			TriggerClientEvent("pNotify:SendNotification", user_id, {
				text = "Você não possui este dinheiro todo em conta",
				type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
				animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
			})
		end
	end
end)

RegisterServerEvent('bank:transfer')
AddEventHandler('bank:transfer', function(fromPlayer, toPlayer, amount)
	local user_id = vRP.getUserId(source)
	local fPlayer = vRP.getUserSource(user_id)

	if tonumber(fPlayer) == tonumber(toPlayer) then
		TriggerClientEvent("pNotify:SendNotification", fPlayer, {
			text = "Você não pode transferir para você mesmo",
			type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
			animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
		})
		CancelEvent()
	else
		local rounded = round(tonumber(amount), 0)
		if(string.len(rounded) >= 9) then
			TriggerClientEvent("pNotify:SendNotification", fPlayer, {
				text = "Valor muito alto, tente um valor mais baixo",
				type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
				animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
			})
			CancelEvent()
		else
			local bankbalance = vRP.getBankMoney(user_id)
			if(tonumber(rounded) <= tonumber(bankbalance)) then
				TriggerClientEvent("banco:updateBalance", fPlayer, (bankbalance - rounded))
				TriggerClientEvent("banco:removeBalance", fPlayer, rounded)
				local user_id2 = vRP.getUserId(toPlayer)
				local bankbalance2 = vRP.getBankMoney(user_id2)
				
				transfer(fPlayer, toPlayer, rounded)
				TriggerClientEvent("pNotify:SendNotification", fPlayer, {
					text = "Transferiu " .. math.floor(rounded) .. " reais",
					type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
					animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
				})
			
				TriggerClientEvent("banco:updateBalance", toPlayer, (bankbalance2 + rounded))
				TriggerClientEvent("banco:addBalance", toPlayer, rounded)
				
				TriggerClientEvent("pNotify:SendNotification", toPlayer, {
					text = "Você recebeu " .. math.floor(rounded) .. " reais",
					type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
					animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
				})
				CancelEvent()
			else
				TriggerClientEvent("pNotify:SendNotification", fPlayer, {
					text = "Você não possui este dinheiro todo em conta",
					type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
					animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
				})
			end
		end    
	end
end)