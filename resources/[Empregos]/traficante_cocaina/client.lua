-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- CONEXÕES DO VRP
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP")
ECserver = Tunnel.getInterface("traficante_cocaina")
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- LOCALIZAÇÕES -> AQUI É OS PONTOS DE ENTREGA, SEGUIR O PADRÃO
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
local entregas = {
	[1] = {name = "Vinewood Hills",x = -1220.50, y = 666.95 , z = 143.10},
	[2] = {name = "Vinewood Hills",x = -1338.97, y = 606.31 , z = 133.37},
	[3] = {name = "Rockford Hills",x = -1051.85, y = 431.66 , z = 76.06 },
	[4] = {name = "Rockford Hills",x = -904.04 , y = 191.49 , z = 68.44 },
	[5] = {name = "Rockford Hills",x = -21.58  , y = -23.70 , z = 72.24 },
	[6] = {name = "Hawick"        ,x = -904.04 , y = 191.49 , z = 68.44 },
	[7] = {name = "Alta"          ,x = 253.10, y = -344.79, z = 44.92 },
	[8] = {name = "Pillbox Hills" ,x = 5.62    , y = -707.72, z = 44.97 },
	[9] = {name = "Mission Row"   ,x = 284.50  , y = -938.50 , z = 28.35},
	[10] ={name = "Rancho"        ,x = 411.59  , y = -1487.54, z = 29.14},
	[11] ={name = "Davis"         ,x = 85.19   , y = -1958.18, z = 20.12},
	[12] ={name ="Chamberlain Hills",x = -213.00, y =-1617.35 , z =37.35},
	[13] ={name ="La puerta"      ,x = -1015.65, y =-1515.05 ,z = 5.51}
}
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- VARIAVEIS LOCAIS
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
local PagamentoM = 0
local PagamentoG = 0
local EmServico = false
local destino = 0
local IndoEntrega = false
local IndoCocaina = false
local px = 0
local py = 0
local pz = 0
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- BLIP | https://wiki.gtanet.work/index.php?title=Blips
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
local blips = {
	{title="Traficante de Cocaína", colour=4, id=501, x = -581.38, y = -985.08, z = 25.98},
}

Citizen.CreateThread(function()
	for _, info in pairs(blips) do
		info.blip = AddBlipForCoord(info.x, info.y, info.z)
		SetBlipSprite(info.blip, info.id)
		SetBlipDisplay(info.blip, 4)
		SetBlipScale(info.blip, 0.9)
		SetBlipColour(info.blip, info.colour)
		SetBlipAsShortRange(info.blip, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString(info.title)
		EndTextCommandSetBlipName(info.blip)
	end
end)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- TEXTO 3D
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Citizen.CreateThread(function ()
	while true do
		Citizen.Wait(1)
		local source = source
		local Coordenadas = GetEntityCoords(GetPlayerPed(-1))			
		local Distancia = GetDistanceBetweenCoords(Coordenadas.x, Coordenadas.y, Coordenadas.z, zezao.cocainaX, zezao.cocainaY, zezao.cocainaZ, true)
		if Distancia < 10.0 and EmServico == false then
			Opacidade = math.floor(255 - (Distancia * 20))
			Texto3D(zezao.cocainaX, zezao.cocainaY, zezao.cocainaZ+0.8, "PRESSIONE ~g~[E] ~w~PARA TRABALHAR", Opacidade)
			DrawMarker(27,zezao.cocainaX, zezao.cocainaY, zezao.cocainaZ-0.9, 0, 0, 0, 0, 0, 0, 1.5001, 1.5001, 0.6001,255,255,51, 200, 0, 0, 0, 0)
		end
	end
end)

Citizen.CreateThread(function ()
	while true do
		Citizen.Wait(1)
		local source = source
		local Coordenadas = GetEntityCoords(GetPlayerPed(-1))			
		local Distancia = GetDistanceBetweenCoords(Coordenadas.x, Coordenadas.y, Coordenadas.z, zezao.cocainaX, zezao.cocainaY, zezao.cocainaZ, true)
		if Distancia < 10.0 and IndoCocaina == true then
			Opacidade = math.floor(255 - (Distancia * 20))
			Texto3D(zezao.cocainaX, zezao.cocainaY, zezao.cocainaZ+0.8, "PRESSIONE ~g~[E] ~w~PARA RECEBER", Opacidade)
			DrawMarker(27,zezao.cocainaX, zezao.cocainaY, zezao.cocainaZ-0.9, 0, 0, 0, 0, 0, 0, 1.5001, 1.5001, 0.6001,255,255,51, 200, 0, 0, 0, 0)
		end
	end
end)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- GERANDO LUGAR PARA ENTREGA
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Citizen.CreateThread(function ()
	while true do
		Citizen.Wait(1)
		local Coordenadas = GetEntityCoords(GetPlayerPed(-1))	
		local Distancia = GetDistanceBetweenCoords(Coordenadas.x, Coordenadas.y, Coordenadas.z, zezao.cocainaX, zezao.cocainaY, zezao.cocainaZ, true)
		if EmServico == false then
			local Coordenadas = GetEntityCoords(GetPlayerPed(-1))	
			local Distancia = GetDistanceBetweenCoords(Coordenadas.x, Coordenadas.y, Coordenadas.z, zezao.cocainaX, zezao.cocainaY, zezao.cocainaZ, true)
			if Distancia < 1.5 then
				if IsControlJustPressed(1,zezao.tecla) then
					if ECserver.PermissaoCoca() then
						EmServico = true
						IndoEntrega = true
						destino = math.random(1, 13)
						cocainas = math.random(1,20)
						ChanceG = math.random(1,10)
						px = entregas[destino].x
						py = entregas[destino].y
						pz = entregas[destino].z
						IrEntregaCocaina(entregas,destino)
					else
						exports.pNotify:SendNotification({
							text = "Você não é um Traficante de Cocaína",
							type = "error",timeout = 3000,progressBar = false, layout = "zezao",queue = "left",
							animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
						})
					end
				end
			end
		end
	end
end)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- INDO ENTREGAR
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Citizen.CreateThread(function ()
	while true do
		Citizen.Wait(1)
		if IndoEntrega == true then				
			destinol = entregas[destino].name
			local Coordenadas = GetEntityCoords(GetPlayerPed(-1))			
			local Distancia = GetDistanceBetweenCoords(Coordenadas.x, Coordenadas.y, Coordenadas.z, entregas[destino].x,entregas[destino].y,entregas[destino].z, true)
			
			drawTxt("Vá até ~g~"..destinol .." ~w~para entregar o pedido ("..cocainas.." unidades de cocaina)",4, 1, 0.45, 0.90, 0.70,255,255,255,255)
			drawTxt("~r~[F5] ~w~para cancelar a missão",4, 1, 0.45, 0.95, 0.70,255,255,255,255)
			if GetDistanceBetweenCoords(px,py,pz, GetEntityCoords(GetPlayerPed(-1),true)) < 10 then
				Opacidade = math.floor(255 - (Distancia * 20))
				Texto3D(entregas[destino].x,entregas[destino].y,entregas[destino].z+1.5, "PRESSIONE ~g~[E] ~w~PARA ENTREGAR", Opacidade)
				DrawMarker(27,entregas[destino].x,entregas[destino].y,entregas[destino].z, 0, 0, 0, 0, 0, 0, 1.5001, 1.5001, 0.6001,255,255,51, 200, 0, 0, 0, 0)
				if IsControlJustPressed(1,zezao.tecla) then	
					if ECserver.ChecarCocaina() >= cocainas then
						GorjetaCocaina()
						TriggerServerEvent("zezao_cocaina:remover",cocainas)
						IndoEntrega = false
						IndoCocaina = true
						RemoveBlip(entrega_cocaina)
						SetNewWaypoint(zezao.cocainaX, zezao.cocainaY)
					else
						exports.pNotify:SendNotification({
							text = "Você não possui cocaínas suficientes para entregar, faltam " .. cocainas,
							type = "success",timeout = 3000,progressBar = false, layout = "zezao",queue = "left",
							animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
						})
					end
				end
			end
		end
	end
end)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- CANCELAR MISSÃO
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Citizen.CreateThread(function ()
	while true do
		Citizen.Wait(1)
		if IsControlJustPressed(1,zezao.teclaCancelar) and IndoEntrega == true then
			exports.pNotify:SendNotification({
				text = "Você cancelou a missão",
				type = "info",timeout = 3000,progressBar = false, layout = "zezao",queue = "left",
				animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
			})
			EmServico = false
			destino = 0
			IndoEntrega = false
			IndoCocaina = false
			gerado = false
			pagamento = 0
			RemoveBlip(entrega_cocaina)
			px = 0
			py = 0
			pz = 0
		end

		if IsEntityDead(GetPlayerPed(-1)) and IndoEntrega == true then
			EmServico = false
			destino = 0
			IndoEntrega = false
			IndoCocaina = false
			gerado = false
			pagamento = 0
			RemoveBlip(entrega_cocaina)
			px = 0
			py = 0
			pz = 0
		end
	end
end)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- VOLTANDO PARA RECEBER
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Citizen.CreateThread(function ()
	while true do
	Citizen.Wait(1)
		if IndoCocaina == true then
			drawTxt("Volte até a ~g~Central ~w~para ser pago",4, 1, 0.45, 0.92, 0.70,255,255,255,255)
			local Coordenadas = GetEntityCoords(GetPlayerPed(-1))			
			local Distancia = GetDistanceBetweenCoords(Coordenadas.x, Coordenadas.y, Coordenadas.z, zezao.cocainaX, zezao.cocainaY, zezao.cocainaZ, true)
			if IsControlJustPressed(1,zezao.tecla) and Distancia < 1.5 then
				PagamentoCocaina()
				IndoEntrega = false
				IndoCocaina = false
				EmServico = false
				pagamento = 0
				gerado = false
				RemoveBlip(entrega_cocaina)
				px = 0
				py = 0
				pz = 0
			end
		end
	end
end)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- FUNÇÕES
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function Texto3D(x,y,z, text, Opacidade)
	local onScreen,_x,_y=World3dToScreen2d(x,y,z)
	local px,py,pz=table.unpack(GetGameplayCamCoords())    
	if onScreen then
		SetTextScale(0.54, 0.54)
		SetTextFont(4)
		SetTextProportional(1)
		SetTextColour(255, 255, 255, Opacidade)
		SetTextDropshadow(0, 0, 0, 0, Opacidade)
		SetTextEdge(2, 0, 0, 0, 150)
		SetTextDropShadow()
		SetTextOutline()
		SetTextEntry("STRING")
		SetTextCentre(1)
		AddTextComponentString(text)
		DrawText(_x,_y)
	end
end

function drawTxt(text,font,centre,x,y,scale,r,g,b,a)
	SetTextFont(font)
	SetTextProportional(0)
	SetTextScale(scale, scale)
	SetTextColour(r, g, b, a)
	SetTextDropShadow(0, 0, 0, 0,255)
	SetTextEdge(1, 0, 0, 0, 255)
	SetTextDropShadow()
	SetTextOutline()
	SetTextCentre(centre)
	SetTextEntry("STRING")
	AddTextComponentString(text)
	DrawText(x , y)
end

function IrEntregaCocaina(entregas,destino)
	entrega_cocaina = AddBlipForCoord(entregas[destino].x,entregas[destino].y, entregas[destino].z)
	SetBlipSprite(entrega_cocaina, 94)
	SetBlipColour(entrega_cocaina, 4)
	SetBlipAsShortRange(entrega_cocaina, false)
    BeginTextCommandSetBlipName("STRING")
    AddTextComponentString("Ponto de Entrega") --> Nome do BLIP no mapa
    EndTextCommandSetBlipName(entrega_cocaina)
	SetNewWaypoint(entregas[destino].x,entregas[destino].y)
end

function PagamentoCocaina()
	distancia = round(GetDistanceBetweenCoords(zezao.cocainaX, zezao.cocainaY, zezao.cocainaZ, px,py,pz))
	PagamentoM = distancia * zezao.multiplicadorP * cocainas
	TriggerServerEvent("zezao_cocaina:pagamento", PagamentoM)
end

function GorjetaCocaina()
	distancia = round(GetDistanceBetweenCoords(zezao.cocainaX, zezao.cocainaY, zezao.cocainaZ, px,py,pz))
	GorjetaM = distancia * zezao.multiplicadorG
	local ChanceG = math.random(1,10)
	if ChanceG == 1 or ChanceG == 3 or ChanceG == 5 or ChanceG == 7 or ChanceG == 9 then
		TriggerServerEvent("zezao_cocaina:gorjeta", GorjetaM)
	end
end

function round(num, numDecimalPlaces)
	local mult = 5^(numDecimalPlaces or 0)
	return math.floor(num * mult + 0.5) / mult
end