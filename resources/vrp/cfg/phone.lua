
local cfg = {}

-- size of the sms history
cfg.sms_history = 15

-- maximum size of an sms
cfg.sms_size = 500

-- duration of a sms position marker (in seconds)
cfg.smspos_duration = 300

-- phone sounds (playAudioSource)
cfg.dialing_sound = "sounds/phone_dialing.ogg" -- loop
cfg.ringing_sound = "sounds/phone_ringing.ogg" -- loop
cfg.sms_sound = "sounds/phone_sms.ogg"

-- define phone services
-- blipid, blipcolor (customize alert blip)
-- alert_time (alert blip display duration in seconds)
-- alert_permission (permission required to receive the alert)
-- alert_notify (notification received when an alert is sent)
-- notify (notification when sending an alert)
cfg.services = {
  ["police"] = {
    blipid = 304,
    blipcolor = 38,
    alert_time = 300, -- 5 minutes
    alert_permission = "policia.servico.chamar",
    alert_notify = "~r~COPOM:~n~~s~",
    notify = "~b~Você chamou a polícia.",
    answer_notify = "~b~A policia esta a caminho."
  },
  ["emergency"] = {
    blipid = 153,
    blipcolor = 1,
    alert_time = 300, -- 5 minutes
    alert_permission = "samu.servico.chamar",
    alert_notify = "~r~SAMU:~n~~s~",
    notify = "~b~Você chamou o SAMU.",
    answer_notify = "~b~O socorro está a caminho."
  },
  ["taxi"] = {
    blipid = 198,
    blipcolor = 5,
    alert_time = 300,
    alert_permission = "chamar.uber",
    alert_notify = "~y~Alerta UBER:~n~~s~",
    notify = "~y~Você chamou um UBER.",
    answer_notify = "~y~O Uber está a caminho."
  },
  ["repair"] = {
    blipid = 446,
    blipcolor = 5,
    alert_time = 300,
    alert_permission = "chamar.mecanico",
    alert_notify = "~y~Alerta Mecânico:~n~~s~",
    notify = "~y~Você chamou um mecânico.",
    answer_notify = "~y~O mecânico está a caminho."
  }
}

-- define phone announces
-- image: background image for the announce (800x150 px)
-- price: amount to pay to post the announce
-- description (optional)
-- permission (optional): permission required to post the announce
cfg.announces = {
}

return cfg

