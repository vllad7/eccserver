local lang = vRP.lang
local cfg = module("cfg/police")

function vRP.insertPoliceRecord(user_id, line)
	if user_id then
		local data = vRP.getUData(user_id, "vRP:police_records")
		local records = data..line.."<br />"
		vRP.setUData(user_id, "vRP:police_records", records)
	end
end

local menu_pc = {name="Computador da Polícia",css={header_color="rgba(0,125,255,0.99)"}}

local function ch_searchreg(player,choice)
	local reg = vRP.prompt(player,"Digite o número do RG","")
	local user_id = vRP.getUserByRegistration(reg)
	if user_id then
		local identity = vRP.getUserIdentity(user_id)
		if identity then
			local name = identity.name
			local firstname = identity.firstname
			local age = identity.age
			local phone = identity.phone
			local registration = identity.registration
			local bname = ""
			local bcapital = 0
			local home = ""
			local number = ""

			local business = vRP.getUserBusiness(user_id)
			if business then
				bname = business.name
				bcapital = business.capital
			end

			local address = vRP.getUserAddress(user_id)
			if address then
				home = address.home
				number = address.number
			end

			local content = lang.police.identity.info({name,firstname,age,registration,phone,bname,bcapital,home,number})
			vRPclient._setDiv(player,"police_pc",".div_police_pc{ background-color: rgba(0,0,0,0.75); color: white; font-weight: bold; width: 500px; padding: 10px; margin: auto; margin-top: 150px; }",content)
		else
			TriggerClientEvent("pNotify:SendNotification", player, {
				text = "R.G não localizado",
				type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
				animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
			})
		end
	else
		TriggerClientEvent("pNotify:SendNotification", player, {
			text = "Não Encontrado",
			type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
			animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
		})
	end
end

local function ch_show_police_records(player,choice)
	local reg = vRP.prompt(player,"Digite o número do RG","")
	local user_id = vRP.getUserByRegistration(reg)
	if user_id then
		local content = vRP.getUData(user_id, "vRP:police_records")
		vRPclient._setDiv(player,"police_pc",".div_police_pc{ background-color: rgba(0,0,0,0.75); color: white; font-weight: bold; width: 500px; padding: 10px; margin: auto; margin-top: 150px; }",content)
	else
		TriggerClientEvent("pNotify:SendNotification", player, {
			text = "Não Encontrado",
			type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
			animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
		})
	end
end

local function ch_delete_police_records(player,choice)
	local reg = vRP.prompt(player,"Digite o número do RG","")
	local user_id = vRP.getUserByRegistration(reg)
	if user_id then
		vRP.setUData(user_id, "vRP:police_records", "")
		TriggerClientEvent("pNotify:SendNotification", player, {
			text = "Ficha Criminal removida",
			type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
			animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
		})
	else
		TriggerClientEvent("pNotify:SendNotification", player, {
			text = "Não Encontrado",
			type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
			animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
		})
	end
end

local function ch_closebusiness(player,choice)
	local nplayer = vRPclient.getNearestPlayer(player,5)
	local nuser_id = vRP.getUserId(nplayer)
	if nuser_id then
		local identity = vRP.getUserIdentity(nuser_id)
		local business = vRP.getUserBusiness(nuser_id)
		if identity and business then
			if vRP.request(player,"Você tem certeza de fechar a empresa de " .. identity.name .. " " .. identity.firstname .. " ?",30) then
				vRP.closeBusiness(nuser_id)
				TriggerClientEvent("pNotify:SendNotification", player, {
					text = "Empresa Fechada",
					type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
					animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
				})
			end
		else
			TriggerClientEvent("pNotify:SendNotification", player, {
				text = "Nenhum jogador próximo",
				type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
				animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
			})
		end
	else
		TriggerClientEvent("pNotify:SendNotification", player, {
			text = "Nenhum jogador próximo",
			type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
			animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
		})
	end
end

local function ch_trackveh(player,choice)
	local reg = vRP.prompt(player,"Digite o número do RG","")
	local user_id = vRP.getUserByRegistration(reg)
	if user_id then
		local note = vRP.prompt(player,"Digite uma nota / observação","")
		TriggerClientEvent("pNotify:SendNotification", player, {
			text = "Rastreando",
			type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
			animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
		})
		local seconds = math.random(60,120)
		SetTimeout(seconds*1000,function()
			local tplayer = vRP.getUserSource(user_id)
			if tplayer then
				local ok,x,y,z = vRPclient.getAnyOwnedVehiclePosition(tplayer)
				if ok then
					vRP.sendServiceAlert(nil, cfg.trackveh.service,x,y,z,lang.police.pc.trackveh.tracked({reg,note}))
				else
					TriggerClientEvent("pNotify:SendNotification", player, {
						text = "Rastreamento falhou",
						type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
						animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
					})
				end
			else
				TriggerClientEvent("pNotify:SendNotification", player, {
					text = "Rastreamento falhou",
					type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
					animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
				})
			end
		end)
	else
		TriggerClientEvent("pNotify:SendNotification", player, {
			text = "Não Encontrado",
			type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
			animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
		})
	end
end

menu_pc["Verificar Identidade"] = {ch_searchreg}
menu_pc["Localizar Veículo"] = {ch_trackveh}
menu_pc["Mostrar Ficha Criminal"] = {ch_show_police_records}
menu_pc["Deletar Ficha Criminal"] = {ch_delete_police_records}
menu_pc["Fechar Empresa"] = {ch_closebusiness}

menu_pc.onclose = function(player)
  	vRPclient._removeDiv(player,"police_pc")
end

local function pc_enter(source,area)
	local user_id = vRP.getUserId(source)
	if user_id and vRP.hasPermission(user_id,"police.pc") then
		vRP.openMenu(source,menu_pc)
	end
end

local function pc_leave(source,area)
  	vRP.closeMenu(source)
end

local choice_handcuff = {function(player,choice)
	local nplayer = vRPclient.getNearestPlayer(player,10)
	if nplayer then
		local nuser_id = vRP.getUserId(nplayer)
		if nuser_id then
			vRPclient._toggleHandcuff(nplayer)
		else
			TriggerClientEvent("pNotify:SendNotification", player, {
				text = "Nenhum jogador próximo",
				type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
				animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
			})
		end
	end
end}

local choice_drag = {function(player,choice)
	local nplayer = vRPclient.getNearestPlayer(player,10)
	if nplayer then
		local nuser_id = vRP.getUserId(nplayer)
		if nuser_id then
			local followed = vRPclient.getFollowedPlayer(nplayer)
			if followed ~= player then
				vRPclient._followPlayer(nplayer, player)
			else
				vRPclient._followPlayer(nplayer)
			end
		else
			TriggerClientEvent("pNotify:SendNotification", player, {
				text = "Nenhum jogador próximo",
				type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
				animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
			})
		end
	end
end}

local choice_putinveh = {function(player,choice)
	local nplayer = vRPclient.getNearestPlayer(player,10)
	local nuser_id = vRP.getUserId(nplayer)
	if nuser_id then
		if vRPclient.isHandcuffed(nplayer) then
			vRPclient._putInNearestVehicleAsPassenger(nplayer, 5)
		else
			TriggerClientEvent("pNotify:SendNotification", player, {
				text = "Jogador não está algemado",
				type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
				animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
			})
		end
	else
		TriggerClientEvent("pNotify:SendNotification", player, {
			text = "Nenhum jogador próximo",
			type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
			animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
		})
	end
end}

local choice_getoutveh = {function(player,choice)
	local nplayer = vRPclient.getNearestPlayer(player,10)
	local nuser_id = vRP.getUserId(nplayer)
	if nuser_id then
		if vRPclient.isHandcuffed(nplayer) then
			vRPclient._ejectVehicle(nplayer)
		else
			TriggerClientEvent("pNotify:SendNotification", player, {
				text = "Jogador não está algemado",
				type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
				animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
			})
		end
	else
		TriggerClientEvent("pNotify:SendNotification", player, {
			text = "Nenhum jogador próximo",
			type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
			animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
		})
	end
end}

local choice_askid = {function(player,choice)
	local nplayer = vRPclient.getNearestPlayer(player,10)
	local nuser_id = vRP.getUserId(nplayer)
	if nuser_id then
		TriggerClientEvent("pNotify:SendNotification", player, {
			text = "Aguardando entrega do RG",
			type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
			animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
		})
		if vRP.request(nplayer,"Deseja mostrar seu R.G ?",15) then
			local identity = vRP.getUserIdentity(nuser_id)
			if identity then
				local name = identity.name
				local firstname = identity.firstname
				local age = identity.age
				local phone = identity.phone
				local registration = identity.registration
				local bname = ""
				local bcapital = 0
				local home = ""
				local number = ""
				local business = vRP.getUserBusiness(nuser_id)
				if business then
				bname = business.name
				bcapital = business.capital
				end
				local address = vRP.getUserAddress(nuser_id)
				if address then
				home = address.home
				number = address.number
				end
				local content = lang.police.identity.info({name,firstname,age,registration,phone,bname,bcapital,home,number})
				vRPclient._setDiv(player,"police_identity",".div_police_identity{ background-color: rgba(0,0,0,0.75); color: white; font-weight: bold; width: 500px; padding: 10px; margin: auto; margin-top: 150px; }",content)
				vRP.request(player, "Devolver R.G", 1000)
				vRPclient._removeDiv(player,"police_identity")
			end
		else
			TriggerClientEvent("pNotify:SendNotification", player, {
				text = "O jogador se recusou a mostrar seu R.G",
				type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
				animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
			})
		end
	else
		TriggerClientEvent("pNotify:SendNotification", player, {
			text = "Nenhum jogador próximo",
			type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
			animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
		})
	end
end}

local choice_check = {function(player,choice)
	local nplayer = vRPclient.getNearestPlayer(player,5)
	local nuser_id = vRP.getUserId(nplayer)
	if nuser_id then
		TriggerClientEvent("pNotify:SendNotification", nplayer, {
			text = "Você está sendo revistado",
			type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
			animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
		})
		local weapons = vRPclient.getWeapons(nplayer)
		local money = vRP.getMoney(nuser_id)
		local items = ""
		local data = vRP.getUserDataTable(nuser_id)
		if data and data.inventory then
			for k,v in pairs(data.inventory) do
				local item_name, item_desc, item_weight = vRP.getItemDefinition(k)
				if item_name then
				items = items.."<br />"..item_name.." ("..v.amount..")"
				end
			end
		end
		local weapons_info = ""
		for k,v in pairs(weapons) do
			weapons_info = weapons_info.."<br />"..k.." ("..v.ammo..")"
		end
		vRPclient._setDiv(player,"police_check",".div_police_check{ background-color: rgba(0,0,0,0.75); color: white; font-weight: bold; width: 500px; padding: 10px; margin: auto; margin-top: 150px; }",lang.police.menu.check.info({money,items,weapons_info}))
		vRP.request(player, "Deseja parar de revistar ?", 1000)
		vRPclient._removeDiv(player,"police_check")
	else
		TriggerClientEvent("pNotify:SendNotification", player, {
			text = "Nenhum jogador próximo",
			type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
			animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
		})
	end
end}

local choice_seize_weapons = {function(player, choice)
	local user_id = vRP.getUserId(player)
	if user_id then
	  local nplayer = vRPclient.getNearestPlayer(player, 5)
	  local nuser_id = vRP.getUserId(nplayer)
	  if nuser_id and vRP.hasPermission(nuser_id, "police.seizable") then
		if vRPclient.isHandcuffed(nplayer) then  -- check handcuffed
		  local weapons = vRPclient.getWeapons(nplayer)
		  for k,v in pairs(weapons) do -- display seized weapons
			-- vRPclient._notify(player,lang.police.menu.seize.seized({k,v.ammo}))
			-- convert weapons to parametric weapon items
			vRP.giveInventoryItem(user_id, "wbody|"..k, 1, true)
			if v.ammo > 0 then
			  vRP.giveInventoryItem(user_id, "wammo|"..k, v.ammo, true)
			end
		  end
  
		  -- clear all weapons
		  vRPclient._giveWeapons(nplayer,{},true)
		  vRPclient._notify(nplayer,lang.police.menu.seize.weapons.seized())
		else
		  vRPclient._notify(player,lang.police.not_handcuffed())
		end
	  else
		vRPclient._notify(player,lang.common.no_player_near())
	  end
	end
  end, lang.police.menu.seize.weapons.description()}
  
  local choice_seize_items = {function(player, choice)
	local user_id = vRP.getUserId(player)
	if user_id then
	  local nplayer = vRPclient.getNearestPlayer(player, 5)
	  local nuser_id = vRP.getUserId(nplayer)
	  if nuser_id and vRP.hasPermission(nuser_id, "police.seizable") then
		if vRPclient.isHandcuffed(nplayer) then  -- check handcuffed
		  local inv = vRP.getInventory(user_id)
  
		  for k,v in pairs(cfg.seizable_items) do -- transfer seizable items
			local sub_items = {v} -- single item
  
			if string.sub(v,1,1) == "*" then -- seize all parametric items of this idname
			  local idname = string.sub(v,2)
			  sub_items = {}
			  for fidname,_ in pairs(inv) do
				if splitString(fidname, "|")[1] == idname then -- same parametric item
				  table.insert(sub_items, fidname) -- add full idname
				end
			  end
			end
  
			for _,idname in pairs(sub_items) do
			  local amount = vRP.getInventoryItemAmount(nuser_id,idname)
			  if amount > 0 then
				local item_name, item_desc, item_weight = vRP.getItemDefinition(idname)
				if item_name then -- do transfer
				  if vRP.tryGetInventoryItem(nuser_id,idname,amount,true) then
					vRP.giveInventoryItem(user_id,idname,amount,false)
					vRPclient._notify(player,lang.police.menu.seize.seized({item_name,amount}))
				  end
				end
			  end
			end
		  end
  
		  vRPclient._notify(nplayer,lang.police.menu.seize.items.seized())
		else
		  vRPclient._notify(player,lang.police.not_handcuffed())
		end
	  else
		vRPclient._notify(player,lang.common.no_player_near())
	  end
	end
  end, lang.police.menu.seize.items.description()}

local choice_jail = {function(player, choice)
	local user_id = vRP.getUserId(player)
	if user_id then
		local nplayer = vRPclient.getNearestPlayer(player, 5)
		local nuser_id = vRP.getUserId(nplayer)
		if nuser_id then
			if vRPclient.isJailed(nplayer) then
				vRPclient._unjail(nplayer)
				TriggerClientEvent("pNotify:SendNotification", nplayer, {
					text = "Você foi solto",
					type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
					animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
				})
				TriggerClientEvent("pNotify:SendNotification", player, {
					text = "Detento libertado",
					type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
					animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
				})
			else
				local x,y,z = vRPclient.getPosition(nplayer)
				local d_min = 1000
				local v_min = nil
				for k,v in pairs(cfg.jails) do
					local dx,dy,dz = x-v[1],y-v[2],z-v[3]
					local dist = math.sqrt(dx*dx+dy*dy+dz*dz)
					if dist <= d_min and dist <= 15 then
						d_min = dist
						v_min = v
					end
					if v_min then
						vRPclient._jail(nplayer,v_min[1],v_min[2],v_min[3],v_min[4])
						TriggerClientEvent("pNotify:SendNotification", nplayer, {
							text = "Você foi preso",
							type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
							animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
						})
						TriggerClientEvent("pNotify:SendNotification", player, {
							text = "Jogador foi preso",
							type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
							animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
						})
					else
						TriggerClientEvent("pNotify:SendNotification", player, {
							text = "Você não está proximo das celas",
							type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
							animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
						})
					end
				end
			end
		else
			TriggerClientEvent("pNotify:SendNotification", player, {
				text = "Nenhum jogador próximo",
				type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
				animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
			})
		end
	end
end}

local choice_fine = {function(player, choice)
	local user_id = vRP.getUserId(player)
	if user_id then
		local nplayer = vRPclient.getNearestPlayer(player, 5)
		local nuser_id = vRP.getUserId(nplayer)
		if nuser_id then
			local money = vRP.getMoney(nuser_id)+vRP.getBankMoney(nuser_id)
			local menu = {name="Multar",css={header_color="rgba(0,125,255,0.99)"}}
			local choose = function(player,choice)
			local amount = cfg.fines[choice]
			if amount ~= nil then
				if vRP.tryFullPayment(nuser_id, amount) then
					vRP.insertPoliceRecord(nuser_id, lang.police.menu.fine.record({choice,amount}))
					TriggerClientEvent("pNotify:SendNotification", player, {
						text = "Você foi multado em " .. amount .. " reais por " .. choice,
						type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
						animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
					})
					TriggerClientEvent("pNotify:SendNotification", nplayer, {
						text = "Multa aplicada em " .. amount .. " reais por " .. choice,
						type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
						animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
					})
					vRP.closeMenu(player)
				else
					TriggerClientEvent("pNotify:SendNotification", player, {
						text = "O jogador não possui dinheiro suficiente",
						type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
						animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
					})
				end
			end
		end
		for k,v in pairs(cfg.fines) do
			if v <= money then
				menu[k] = {choose,v}
			end
		end
		vRP.openMenu(player, menu)
		else
			TriggerClientEvent("pNotify:SendNotification", player, {
				text = "Nenhum jogador próximo",
				type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
				animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
			})
		end
	end
end}

local choice_store_weapons = {function(player, choice)
	local user_id = vRP.getUserId(player)
	if user_id then
		local weapons = vRPclient.getWeapons(player)
		for k,v in pairs(weapons) do
			vRP.giveInventoryItem(user_id, "wbody|"..k, 1, true)
			if v.ammo > 0 then
				vRP.giveInventoryItem(user_id, "wammo|"..k, v.ammo, true)
			end
		end
		vRPclient._giveWeapons(player,{},true)
	end
end}

vRP.registerMenuBuilder("pessoal", function(add, data)
	local user_id = vRP.getUserId(data.player)
	if user_id then
		local choices = {}
		if vRP.hasPermission(user_id,"revistar.pessoal") then
			choices["Revistar"] = choice_check
		end

		add(choices)
	end
end)

vRP.registerMenuBuilder("main", function(add, data)
	local player = data.player
	local user_id = vRP.getUserId(player)
	if user_id then
		local choices = {}
		if vRP.hasPermission(user_id,"menu.policial") then
			choices["Polícia"] = {function(player,choice)
				local menu = vRP.buildMenu("police", {player = player})
				menu.name = "Policial"
				menu.css = {header_color="rgba(0,125,255,0.99)"}

				if vRP.hasPermission(user_id,"policial.colocar.veiculo") then
					menu["Colocar no Veículo"] = choice_putinveh
				end

				if vRP.hasPermission(user_id,"policial.remover.veiculo") then
					menu["Remover do Veículo"] = choice_getoutveh
				end

				if vRP.hasPermission(user_id,"policial.revistar") then
					menu["Revistar Jogador"] = choice_check
				end

				if vRP.hasPermission(user_id,"policial.apreender.armas") then
					menu["Apreender Armas"] = choice_seize_weapons
				  end
		  
				  if vRP.hasPermission(user_id,"policial.apreender.itens") then
					menu["Apreender Itens"] = choice_seize_items
				  end

				if vRP.hasPermission(user_id,"policial.multar") then
					menu["Multar Jogador"] = choice_fine
				end

				if vRP.hasPermission(user_id,"policial.pedir.rg") then
					menu["Solicitar Documentos"] = choice_askid
				end

				if vRP.hasPermission(user_id, "guardar.armas") then
					menu["Guardar Armas"] = choice_store_weapons
				end

				if vRP.hasPermission(user_id,"policial.seguir") then
					menu["Arrastar Jogador"] = choice_drag
				end

				--[[
				if vRP.hasPermission(user_id,"policial.prender") then
					menu["Prender Jogador"] = choice_jail
				end

				if vRP.hasPermission(user_id,"policial.algemar") then
					menu["Algemar Jogador"] = choice_handcuff
				end				
				]]

				vRP.openMenu(player,menu)
			end}
		end

		add(choices)
	end
end)

local function build_client_points(source)
	for k,v in pairs(cfg.pcs) do
		local x,y,z = table.unpack(v)
		vRPclient._addMarker(source,x,y,z-1,0.7,0.7,0.5,0,125,255,125,150)
		vRP.setArea(source,"vRP:police:pc"..k,x,y,z,1,1.5,pc_enter,pc_leave)
	end
end

AddEventHandler("vRP:playerSpawn",function(user_id, source, first_spawn)
  	if first_spawn then
    	build_client_points(source)
  	end
end)

local wantedlvl_players = {}
function vRP.getUserWantedLevel(user_id)
  	return wantedlvl_players[user_id] or 0
end

function tvRP.updateWantedLevel(level)
	local player = source
	local user_id = vRP.getUserId(player)
	if user_id then
		local was_wanted = (vRP.getUserWantedLevel(user_id) > 0)
		wantedlvl_players[user_id] = level
		local is_wanted = (level > 0)
		if not was_wanted and is_wanted then
			local x,y,z = vRPclient.getPosition(player)
			vRP.sendServiceAlert(nil, cfg.wanted.service,x,y,z,lang.police.wanted({level}))
		end
		if was_wanted and not is_wanted then
			vRPclient._removeNamedBlip(-1, "vRP:wanted:"..user_id)
		end
	end
end

AddEventHandler("vRP:playerLeave", function(user_id, player)
  	wantedlvl_players[user_id] = nil
  	vRPclient._removeNamedBlip(-1, "vRP:wanted:"..user_id)
end)

local function task_wanted_positions()
	local listeners = vRP.getUsersByPermission("policial.procurado")
	for k,v in pairs(wantedlvl_players) do
		local player = vRP.getUserSource(tonumber(k))
		if player and v and v > 0 then
			local x,y,z = vRPclient.getPosition(player)
			for l,w in pairs(listeners) do
				local lplayer = vRP.getUserSource(w)
				if lplayer then
					vRPclient._setNamedBlip(lplayer, "vRP:wanted:"..k,x,y,z,cfg.wanted.blipid,cfg.wanted.blipcolor,lang.police.wanted({v}))
				end
			end
		end
	end
	SetTimeout(5000, task_wanted_positions)
end

async(function()
  	task_wanted_positions()
end)

vRP.registerMenuBuilder("pessoal", function(add, data)
	local user_id = vRP.getUserId(data.player)
	if user_id then
		local choices = {}
		choices["Guardar Armas"] = choice_store_weapons
		add(choices)
	end
end)