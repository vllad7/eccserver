K9Config = {}
K9Config = setmetatable(K9Config, {})

K9Config.AbrirMenu = true

K9Config.SkinsBloqueadas = {
	"s_m_y_cop_01",
	"s_m_y_sheriff_01"
}

-- Restricts the dog to getting into certain vehicles
K9Config.VehicleRestriction = true
K9Config.VehiclesList = {
"police",
"police4"
}