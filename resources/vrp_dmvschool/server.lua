--[[
    FiveM Scripts
    Copyright C 2018  Sighmir

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    at your option any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
]]

Tunnel = module("vrp", "lib/Tunnel")
Proxy = module("vrp", "lib/Proxy")
htmlEntities = module("vrp", "lib/htmlEntities")

vRPdmv = {}
vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP")
DMVclient = Tunnel.getInterface("vrp_dmvschool")
Tunnel.bindInterface("vrp_dmvschool",vRPdmv)
Proxy.addInterface("vrp_dmvschool",vRPdmv)
cfg = module("vrp_dmvschool", "cfg/dmv")
-- load global and local languages
Luang = module("vrp", "lib/Luang")
Lang = Luang()
Lang:loadLocale(cfg.lang, module("vrp", "cfg/lang/"..cfg.lang) or {})
Lang:loadLocale(cfg.lang, module("vrp_dmvschool", "cfg/lang/"..cfg.lang) or {})
lang = Lang.lang[cfg.lang]

function vRPdmv.setLicense()
	local user_id = vRP.getUserId(source)
	vRP.setUData(user_id,"vRP:dmv:license",json.encode(os.date("%x")))
end

function vRPdmv.payTheory()
	local user_id = vRP.getUserId(source)
	local player = vRP.getUserSource(user_id)
	if vRP.tryPayment(user_id,250) then
        DMVclient.startTheory(player)
	else
		TriggerClientEvent("pNotify:SendNotification", player, {
      text = "Dinheiro Insuficiente",
      type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
      animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
    })
	end
end

function vRPdmv.payPractical()
	local user_id = vRP.getUserId(source)
	local player = vRP.getUserSource(user_id)
	if vRP.tryPayment(user_id,500) then
        DMVclient.startPractical(player)
	else
		TriggerClientEvent("pNotify:SendNotification", player, {
      text = "Dinheiro Insuficiente",
      type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
      animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
    })
	end
end

AddEventHandler("vRP:playerSpawn", function(user_id, source, first_spawn)
	local user_id = vRP.getUserId(source)
	local player = vRP.getUserSource(user_id)
	local data = vRP.getUData(user_id,"vRP:dmv:license")
	if data then
	  local license = json.decode(data)
	  if license and license ~= 0 then
        DMVclient.setLicense(player, true)
	  end
	end
end)

local choice_asklc = {function(player,choice)
  local nplayer = vRPclient.getNearestPlayer(player,10)
  local nuser_id = vRP.getUserId(nplayer)
  if nuser_id then
    TriggerClientEvent("pNotify:SendNotification", player, {
      text = "Pedido enviado",
      type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
      animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
    })
    if vRP.request(nplayer,"Deseja mostrar sua CNH ?",15) then
	  local data = vRP.getUData(nuser_id,"vRP:dmv:license")
      if data then
	    local license = json.decode(data)
		if license and license ~= 0 then
          local identity = vRP.getUserIdentity(nuser_id)
          if identity then
            -- display identity and business
            local name = identity.name
            local firstname = identity.firstname
            local age = identity.age
            local phone = identity.phone
            local registration = identity.registration
          
            local content = lang.dmv.police.license({name,firstname,age,registration,phone,license})
            vRPclient.setDiv(player,"police_identity",".div_police_identity{ background-color: rgba(0,0,0,0.75); color: white; font-weight: bold; width: 500px; padding: 10px; margin: auto; margin-top: 150px; }",content)
            -- request to hide div
            vRP.request(player, "Devolver CNH", 1000)
            vRPclient.removeDiv(player,"police_identity")
          end
	    else
	      TriggerClientEvent("pNotify:SendNotification", player, {
          text = "Este jogador não possui CNH",
          type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
          animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
        })
        end
	  else
	    TriggerClientEvent("pNotify:SendNotification", player, {
          text = "Este jogador não possui CNH",
          type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
          animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
        })
      end
    else
      TriggerClientEvent("pNotify:SendNotification", player, {
        text = "Este jogador se recusou a mostrar sua CNH",
        type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
        animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
      })
    end
  else
    TriggerClientEvent("pNotify:SendNotification", player, {
      text = "Nenhum jogador por perto",
      type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
      animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
    })
  end
end}

local choice_takelc = {function(player,choice)
  local nplayer = vRPclient.getNearestPlayer(player,10)
  local nuser_id = vRP.getUserId(nplayer)
  if nuser_id then
    if vRP.request(player,"Tem certeza que deseja remover esta licença?",15) then
	  local data = vRP.getUData(nuser_id,"vRP:dmv:license")
      if data then
	    local license = json.decode(data)
		if license and license ~= 0 then
          DMVclient.setLicense(nplayer, false)
	      vRP.setUData(nuser_id,"vRP:dmv:license",json.encode())
        TriggerClientEvent("pNotify:SendNotification", nplayer, {
          text = "Sua CNH foi retirada",
          type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
          animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
        })
        TriggerClientEvent("pNotify:SendNotification", player, {
          text = "CNH removida com sucesso",
          type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
          animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
        })
	    else
	      TriggerClientEvent("pNotify:SendNotification", player, {
          text = "Este jogador não possui CNH",
          type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
          animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
        })
        end
	  else
	    TriggerClientEvent("pNotify:SendNotification", player, {
        text = "Este jogador não possui CNH",
        type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
        animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
      })
      end
    else
      TriggerClientEvent("pNotify:SendNotification", player, {
        text = "Este jogador se recusou a mostrar sua CNH",
        type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
        animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
      })
    end
  else
    TriggerClientEvent("pNotify:SendNotification", player, {
      text = "Nenhum jogador por perto",
      type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
      animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
    })
  end
end}

Citizen.CreateThread(function()
  vRP.registerMenuBuilder("police", function(add, data)
    local player = data.player
  
    local user_id = vRP.getUserId(player)
    if user_id ~= nil then
      local choices = {}
	  
      if vRP.hasPermission(user_id,"pedir.cnh") then
         choices["Pedir CNH"] = choice_asklc
      end
	  
      if vRP.hasPermission(user_id,"remover.cnh") then
         choices["Retirar CNH"] = choice_takelc
      end
  	
      add(choices)
    end
  end)
end)