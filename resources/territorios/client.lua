local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP")

local domming = false
local favela = ""
local secondsRemaining = 0


function favela_DisplayHelpText(str)
	SetTextComponentFormat("STRING")
	AddTextComponentString(str)
	DisplayHelpTextFromStringLabel(0, 0, 1, -1)
end

function favela_drawTxt(x,y ,width,height,scale, text, r,g,b,a, outline)
    SetTextFont(0)
    SetTextProportional(0)
    SetTextScale(scale, scale)
    SetTextColour(r, g, b, a)
    SetTextDropShadow(0, 0, 0, 0,255)
    SetTextEdge(1, 0, 0, 0, 255)
    SetTextDropShadow()
    if(outline)then
	    SetTextOutline()
	end
    SetTextEntry("STRING")
    AddTextComponentString(text)
    DrawText(x - width/2, y - height/2 + 0.005)
end

local dominacao = {
    ["Favela do Alemão"] = {
        coordenada = {['x'] = 771.35, ['y'] = -233.64, ['z'] = 66.21}, 
}}


local favelas = cfg.favelas


RegisterNetEvent('es_favela:currentlydomming')
AddEventHandler('es_favela:currentlydomming', function(domm)
	domming = true
	favela = domm
	secondsRemaining = cfg.seconds
end)

RegisterNetEvent('es_favela:toofarlocal')
AddEventHandler('es_favela:toofarlocal', function(domm)
	domming = false
	RemoveBlip(blip)
	TriggerEvent('chatMessage', 'SISTEMA', { 0, 0x99, 255 }, "A invasão foi cancelada, vaza daqui.")
	dommingName = ""
	secondsRemaining = 0
	incircle = false
end)

RegisterNetEvent('es_favela:playerdiedlocal')
AddEventHandler('es_favela:playerdiedlocal', function(domm)
	domming = false
	RemoveBlip(blip)
	TriggerEvent('chatMessage', 'SISTEMA', { 0, 0x99, 255 }, "A invasão foi cancelada, você perdeu!.")
	dommingName = ""
	secondsRemaining = 0
	incircle = false
end)


RegisterNetEvent('es_favela:dommerycomplete')
AddEventHandler('es_favela:dommerycomplete', function(reward)
	domming = false
	RemoveBlip(blip)
	favela = ""
	secondsRemaining = 0
	incircle = false
end)

Citizen.CreateThread(function()
	while true do
		if domming then
			Citizen.Wait(1000)
			if(secondsRemaining > 0)then
				secondsRemaining = secondsRemaining - 1
			end
		end
		Citizen.Wait(0)
	end
end)

incircle = false

Citizen.CreateThread(function()
	while true do
		local pos = GetEntityCoords(GetPlayerPed(-1), true)

		for k,v in pairs(favelas)do
			local pos2 = v.position

			if(Vdist(pos.x, pos.y, pos.z, pos2.x, pos2.y, pos2.z) < 15.0)then
				if not domming then
					DrawMarker(27, v.position.x, v.position.y, v.position.z - 1, 0, 0, 0, 0, 0, 0, 10.0001, 10.0001, 0.5001, 1555, 0, 0,255, 0, 0, 0,0)
					
					if(Vdist(pos.x, pos.y, pos.z, pos2.x, pos2.y, pos2.z) < 3.4)then
						if (incircle == false) then
							favela_DisplayHelpText("Pressione ~INPUT_CONTEXT~ para invadir a ~b~" .. v.nameoffavela .. "~w~ cuidado, geral vai ficar ligado!")
						end
						incircle = true
						if (IsControlJustReleased(1, 51)) then
							TriggerServerEvent('es_favela:dom', k)
							for k, v in pairs(dominacao)do
								local ve = v.coordenada
			            		blip = AddBlipForCoord(ve.x, ve.y, ve.z)
			            		SetBlipSprite(blip, 84)
						      	SetBlipColour(blip, 46)
						      	SetBlipAsShortRange(blip, true)
						      	BeginTextCommandSetBlipName("STRING")
						      	AddTextComponentString("Dominacao")
						      	EndTextCommandSetBlipName(blip)
			            	end   
						end
					elseif(Vdist(pos.x, pos.y, pos.z, pos2.x, pos2.y, pos2.z) > 2)then
						incircle = false
					end
				end
			end
		end

		if domming then
			favela_drawTxt(0.66, 1.44, 1.0,1.0,0.4, "Dominando: ~r~" .. secondsRemaining .. "~w~ segundos restantes", 255, 255, 255, 255)
			
			local pos2 = favelas[favela].position
			local ped = GetPlayerPed(-1)
			
            if IsEntityDead(ped) then
			TriggerServerEvent('es_favela:playerdied', favela)
			elseif (Vdist(pos.x, pos.y, pos.z, pos2.x, pos2.y, pos2.z) > 15)then
				TriggerServerEvent('es_favela:toofar', favela)
			end
		end

		Citizen.Wait(0)
	end
end)