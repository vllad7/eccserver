
local cfg = {}

-- minimum capital to open a business
cfg.minimum_capital = 100000

-- capital transfer reset interval in minutes
-- default: reset every 24h
cfg.transfer_reset_interval = 24*60

-- commerce chamber {blipid,blipcolor}
cfg.blip = {491,70} 

-- positions of commerce chambers
cfg.commerce_chambers = {
  {-100.65676116943,-556.36260986328,40.481346130371}

}

return cfg
