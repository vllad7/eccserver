function vRPbm.chargePhoneNumber(user_id,phone)
    local player = vRP.getUserSource(user_id)
    local directory_name = vRP.getPhoneDirectoryName(user_id, phone)
    if directory_name == "unknown" then
        directory_name = phone
    end
    local charged = vRP.prompt(player,"Quanto deseja cobrar?","")
    if charge ~= nil and charge ~= "" and tonumber(charge)>0 then
        local target_id = vRP.getUserByPhone(phone)
        if target_id~=nil then
            if charge ~= nil and charge ~= "" then
                local target = vRP.getUserSource(target_id)
                if target ~= nil then
                    local identity = vRP.getUserIdentity(user_id)
                    local my_directory_name = vRP.getPhoneDirectoryName(target_id, identity.phone)
                    if my_directory_name == "unknown" then
                        my_directory_name = identity.phone
                    end
                    local ok = vRP.request(target,"Deseja aceitar o pagamento de ".. my_directory_name .. " em um valor de " .. charge .." reais ?",30)
                    if ok then
                        local target_bank = vRP.getBankMoney(target_id) - tonumber(charge)
                        local my_bank = vRP.getBankMoney(user_id) + tonumber(charge)
                        if target_bank>0 then
                            vRP.setBankMoney(user_id,my_bank)
                            vRP.setBankMoney(target_id,target_bank)
                            TriggerClientEvent("pNotify:SendNotification", player, {
                                text = "Voce cobrou " .. directory_name .. " em " .. charge .. " reais",
                                type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                                animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                            })

                            TriggerClientEvent("pNotify:SendNotification", target, {
                                text = my_directory_name .. " lhe cobrou " .. charge .. " reais",
                                type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                                animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                            })
                            vRP.closeMenu(player)
                        else
                            TriggerClientEvent("pNotify:SendNotification", target, {
                                text = "Dinheiro Insuficiente",
                                type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                                animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                            })
                        end
                    else
                        TriggerClientEvent("pNotify:SendNotification", player, {
                            text = "Oferta recusada",
                            type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                            animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                        })
                    end
                else
                    TriggerClientEvent("pNotify:SendNotification", player, {
                        text = "Valor Inválido",
                        type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                        animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                    })
                end
            else
                TriggerClientEvent("pNotify:SendNotification", player, {
                    text = "Valor Inválido",
                    type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end
        else
            TriggerClientEvent("pNotify:SendNotification", player, {
                text = "Valor Inválido",
                type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
            })
        end
    else
        TriggerClientEvent("pNotify:SendNotification", player, {
            text = "Valor Inválido",
            type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
            animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
        })
    end
end

function vRPbm.payPhoneNumber(user_id,phone)
    local player = vRP.getUserSource(user_id)
    local directory_name = vRP.getPhoneDirectoryName(user_id, phone)
    if directory_name == "unknown" then
        directory_name = phone
    end
    local transfer = vRP.prompt(player,"Valor a pagar","")
    if transfer ~= nil and transfer ~= "" and tonumber(transfer)>0 then
        local target_id = vRP.getUserByPhone(phone)
        local my_bank = vRP.getBankMoney(user_id) - tonumber(transfer)
        if target_id~=nil then
            if my_bank >= 0 then
                local target = vRP.getUserSource(target_id)
                if target ~= nil then
					vRP.setBankMoney(user_id,my_bank)
					TriggerClientEvent("pNotify:SendNotification", player, {
                        text = "Você transferiu " .. transfer .. " reais para " .. directory_name,
                        type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                        animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                    })
                    local target_bank = vRP.getBankMoney(target_id) + tonumber(transfer)
                    vRP.setBankMoney(target_id,target_bank)
                    local identity = vRP.getUserIdentity(user_id)
                    local my_directory_name = vRP.getPhoneDirectoryName(target_id, identity.phone)
                    if my_directory_name == "unknown" then
                        my_directory_name = identity.phone
                    end
					TriggerClientEvent("pNotify:SendNotification", target, {
                        text = "Você recebeu " .. transfer .. " reais de " .. my_directory_name,
                        type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                        animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                    })
                    vRP.closeMenu(player)
                else
                    TriggerClientEvent("pNotify:SendNotification", player, {
                        text = "Valor Inválido",
                        type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                        animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                    })
                end
            else
                TriggerClientEvent("pNotify:SendNotification", player, {
                    text = "Dinheiro Insuficiente",
                    type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end
        else
            TriggerClientEvent("pNotify:SendNotification", player, {
                text = "Valor Inválido",
                type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
            })
        end
    else
        TriggerClientEvent("pNotify:SendNotification", player, {
            text = "Valor Inválido",
            type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
            animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
        })
    end
end