local lang = vRP.lang
local cfg = module("cfg/inventory")

vRP.items = {}
function vRP.defInventoryItem(idname,name,description,choices,weight)
	if weight == nil then
		weight = 0
	end

	local item = {name=name,description=description,choices=choices,weight=weight}
	vRP.items[idname] = item
end


function ch_give(idname, player, choice)
    local user_id = vRP.getUserId(player)
    if user_id then
        local nplayer = vRPclient.getNearestPlayer(player,10)
        if nplayer then
            local nuser_id = vRP.getUserId(nplayer)
            if nuser_id then
                local amount = vRP.prompt(player,lang.inventory.give.prompt({vRP.getInventoryItemAmount(user_id,idname)}),"")
                local amount = parseInt(amount)
                local new_weight = vRP.getInventoryWeight(nuser_id)+vRP.getItemWeight(idname)*amount
                if new_weight <= vRP.getInventoryMaxWeight(nuser_id) then
                    if vRP.tryGetInventoryItem(user_id,idname,amount,true) then
                        vRP.giveInventoryItem(nuser_id,idname,amount,true)
                        vRPclient._playAnim(player,true,{{"mp_common","givetake1_a",1}},false)
                        vRPclient._playAnim(nplayer,true,{{"mp_common","givetake2_a",1}},false)
                    else
                        TriggerClientEvent("pNotify:SendNotification", player, {
                            text = "Valor Inválido",
                            type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                            animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                        })
                    end
                else
                    TriggerClientEvent("pNotify:SendNotification", player, {
                        text = "Inventário cheio",
                        type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                        animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                    })
                end
            else
                TriggerClientEvent("pNotify:SendNotification", player, {
                    text = "Nenhum jogador por perto",
                    type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end
        else
            TriggerClientEvent("pNotify:SendNotification", player, {
                text = "Nenhum jogador por perto",
                type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
            })
        end
    end
end

function ch_trash(idname, player, choice)
    local user_id = vRP.getUserId(player)
    if user_id then
        local amount = vRP.prompt(player,lang.inventory.trash.prompt({vRP.getInventoryItemAmount(user_id,idname)}),"")
        local amount = parseInt(amount)
        if vRP.tryGetInventoryItem(user_id,idname,amount,false) then
            TriggerClientEvent("pNotify:SendNotification", player, {
                text = "Você jogou fora " .. amount .. " " .. vRP.getItemName(idname) .. ".",
                type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
            })
            vRPclient._playAnim(player,true,{{"pickup_object","pickup_low",1}},false)
        else
            TriggerClientEvent("pNotify:SendNotification", player, {
                text = "Valor inválido",
                type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
            })
        end
    end
end

function vRP.computeItemName(item,args)
	if type(item.name) == "string" then return item.name
	else return item.name(args) end
end

function vRP.computeItemDescription(item,args)
	if type(item.description) == "string" then 
		return item.description
	else 
		return item.description(args) 
	end
end

function vRP.computeItemChoices(item,args)
	if item.choices ~= nil then
		return item.choices(args)
	else
		return {}
	end
end

function vRP.computeItemWeight(item,args)
	if type(item.weight) == "number" then 
		return item.weight
	else 
		return item.weight(args) 
	end
end


function vRP.parseItem(idname)
  	return splitString(idname,"|")
end

function vRP.getItemDefinition(idname)
	local args = vRP.parseItem(idname)
	local item = vRP.items[args[1]]
	if item then
		return vRP.computeItemName(item,args), vRP.computeItemDescription(item,args), vRP.computeItemWeight(item,args)
	end
	return nil,nil,nil
end

function vRP.getItemName(idname)
	local args = vRP.parseItem(idname)
	local item = vRP.items[args[1]]
	if item then 
		return vRP.computeItemName(item,args) 
	end
	return args[1]
end

function vRP.getItemDescription(idname)
	local args = vRP.parseItem(idname)
	local item = vRP.items[args[1]]
	if item then 
		return vRP.computeItemDescription(item,args) 
	end
	return ""
end

function vRP.getItemChoices(idname)
	local args = vRP.parseItem(idname)
	local item = vRP.items[args[1]]
	local choices = {}
	if item then
		local cchoices = vRP.computeItemChoices(item,args)
		if cchoices then
			for k,v in pairs(cchoices) do
				choices[k] = v
			end
		end
		choices["Enviar Item"] = {function(player,choice) ch_give(idname, player, choice) end}
		choices["Jogar Item Fora"] = {function(player, choice) ch_trash(idname, player, choice) end}
	end
	return choices
end

function vRP.getItemWeight(idname)
  	local args = vRP.parseItem(idname)
  	local item = vRP.items[args[1]]
	  if item then 
		return vRP.computeItemWeight(item,args) 
	end
  	return 0
end

function vRP.computeItemsWeight(items)
	local weight = 0
	for k,v in pairs(items) do
		local iweight = vRP.getItemWeight(k)
		weight = weight+iweight*v.amount
	end
	return weight
end

function vRP.giveInventoryItem(user_id,idname,amount,notify)
    if notify == nil
    then notify = true
    end
    local data = vRP.getUserDataTable(user_id)
    if data and amount > 0 then
        local entry = data.inventory[idname]
        if entry then
            entry.amount = entry.amount+amount
        else
            data.inventory[idname] = {amount=amount}
        end
        if notify then
            local player = vRP.getUserSource(user_id)
            if player then
                TriggerClientEvent("pNotify:SendNotification", player, {
                    text = "Recebeu " .. amount .. " " .. vRP.getItemName(idname) .. ".",
                    type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end
        end
    end
end

function vRP.tryGetInventoryItem(user_id,idname,amount,notify)
    if notify == nil then notify = true end
    local data = vRP.getUserDataTable(user_id)
    if data and amount > 0 then
        local entry = data.inventory[idname]
        if entry and entry.amount >= amount then
            entry.amount = entry.amount-amount
            if entry.amount <= 0 then
                data.inventory[idname] = nil
            end
            if notify then
                local player = vRP.getUserSource(user_id)
                if player then
                    TriggerClientEvent("pNotify:SendNotification", player, {
                        text = "Enviou " .. amount .. " " .. vRP.getItemName(idname) .. ".",
                        type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                        animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                    })
                end
            end
            return true
        else
            if notify then
                local player = vRP.getUserSource(user_id)
                if player then
                    local entry_amount = 0
                    if entry then entry_amount = entry.amount end
                    TriggerClientEvent("pNotify:SendNotification", player, {
                        text = "Faltando " .. amount-entry_amount .. " " .. vRP.getItemName(idname) .. ".",
                        type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                        animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                    })
                end
            end
        end
    end
    return false
end

function vRP.getInventoryItemAmount(user_id,idname)
    local data = vRP.getUserDataTable(user_id)
    if data and data.inventory then
        local entry = data.inventory[idname]
        if entry then
            return entry.amount
        end
    end

    return 0
end

function vRP.getInventory(user_id)
    local data = vRP.getUserDataTable(user_id)
    if data then
        return data.inventory
    end
end

function vRP.getInventoryWeight(user_id)
    local data = vRP.getUserDataTable(user_id)
    if data and data.inventory then
        return vRP.computeItemsWeight(data.inventory)
    end

    return 0
end

function vRP.getInventoryMaxWeight(user_id)
    return math.floor(vRP.expToLevel(vRP.getExp(user_id, "fisico", "forca")))*cfg.inventory_weight_per_strength
end

function vRP.clearInventory(user_id)
    local data = vRP.getUserDataTable(user_id)
    if data then
        data.inventory = {}
    end
end

function vRP.openInventory(source)
    local user_id = vRP.getUserId(source)
    if user_id then
        local data = vRP.getUserDataTable(user_id)
        if data then
            local menudata = {name="Meu Inventário",css={header_color="rgba(0,125,255,0.99)"}}
            local weight = vRP.getInventoryWeight(user_id)
            local max_weight = vRP.getInventoryMaxWeight(user_id)
            local hue = math.floor(math.max(125*(1-weight/max_weight), 0))
            menudata["<div class=\"dprogressbar\" data-value=\""..string.format("%.2f",weight/max_weight).."\" data-color=\"hsl("..hue..",100%,50%)\" data-bgcolor=\"hsl("..hue..",100%,25%)\" style=\"height: 12px; border: 3px solid black;\"></div>"] = {function()end, lang.inventory.info_weight({string.format("%.2f",weight),max_weight})}
            local kitems = {}
            local choose = function(player,choice)
                if string.sub(choice,1,1) ~= "@" then
                    local choices = vRP.getItemChoices(kitems[choice])
                    local submenudata = {name=choice,css={header_color="rgba(0,125,255,0.99)"}}
                    for k,v in pairs(choices) do
                        submenudata[k] = v
                    end
                    submenudata.onclose = function()
                        vRP.openInventory(source)
                    end
                    vRP.openMenu(source,submenudata)
                end
            end
            for k,v in pairs(data.inventory) do
                local name,description,weight = vRP.getItemDefinition(k)
                if name ~= nil then
                    kitems[name] = k
                    menudata[name] = {choose,lang.inventory.iteminfo({v.amount,description,string.format("%.2f",weight)})}
                end
            end
            vRP.openMenu(source,menudata)
        end
    end
end

AddEventHandler("vRP:playerJoin", function(user_id,source,name,last_login)
    local data = vRP.getUserDataTable(user_id)
    if not data.inventory then
        data.inventory = {}
    end
end)

local choices = {}
choices["Meu Inventário"] = {function(player, choice) vRP.openInventory(player) end}

vRP.registerMenuBuilder("main", function(add, data)
    add(choices)
end)

local chests = {}
local function build_itemlist_menu(name, items, cb)
    local menu = {name=name, css={header_color="rgba(255,125,0,0.99)"}}
    local kitems = {}
    local choose = function(player,choice)
        local idname = kitems[choice]
        if idname then
            cb(idname)
        end
    end
    for k,v in pairs(items) do
        local name,description,weight = vRP.getItemDefinition(k)
        if name then
            kitems[name] = k
            menu[name] = {choose,lang.inventory.iteminfo({v.amount,description,string.format("%.2f", weight)})}
        end
    end
    return menu
end

function vRP.openChest(source, name, max_weight, cb_close, cb_in, cb_out)
    local user_id = vRP.getUserId(source)
    if user_id then
        local data = vRP.getUserDataTable(user_id)
        if data.inventory then
            if not chests[name] then
                local close_count = 0
                local chest = {max_weight = max_weight}
                chests[name] = chest
                local cdata = vRP.getSData("chest:"..name)
                chest.items = json.decode(cdata) or {}
                local menu = {name="Inventário", css={header_color="rgba(255,125,0,0.99)"}}
                local cb_take = function(idname)
                    local citem = chest.items[idname]
                    local amount = vRP.prompt(source, lang.inventory.chest.take.prompt({citem.amount}), "")
                    amount = parseInt(amount)
                    if amount >= 0 and amount <= citem.amount then
                        local new_weight = vRP.getInventoryWeight(user_id)+vRP.getItemWeight(idname)*amount
                        if new_weight <= vRP.getInventoryMaxWeight(user_id) then
                            vRP.giveInventoryItem(user_id, idname, amount, true)
                            citem.amount = citem.amount-amount
                            if citem.amount <= 0 then
                                chest.items[idname] = nil
                            end
                            if cb_out then cb_out(idname,amount) end
                            vRP.closeMenu(source)
                        else
                            TriggerClientEvent("pNotify:SendNotification", source, {
                                text = "Inventário cheio",
                                type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                                animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                            })
                        end
                    else
                        TriggerClientEvent("pNotify:SendNotification", source, {
                            text = "Valor Inválido",
                            type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                            animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                        })
                    end
                end
                local ch_take = function(player, choice)
                    local submenu = build_itemlist_menu("Retirar", chest.items, cb_take)
                    local weight = vRP.computeItemsWeight(chest.items)
                    local hue = math.floor(math.max(125*(1-weight/max_weight), 0))
                    submenu["<div class=\"dprogressbar\" data-value=\""..string.format("%.2f",weight/max_weight).."\" data-color=\"hsl("..hue..",100%,50%)\" data-bgcolor=\"hsl("..hue..",100%,25%)\" style=\"height: 12px; border: 3px solid black;\"></div>"] = {function()end, lang.inventory.info_weight({string.format("%.2f",weight),max_weight})}
                    submenu.onclose = function()
                        close_count = close_count-1
                        vRP.openMenu(player, menu)
                    end
                    close_count = close_count+1
                    vRP.openMenu(player, submenu)
                end
                local cb_put = function(idname)
                    local amount = vRP.prompt(source, lang.inventory.chest.put.prompt({vRP.getInventoryItemAmount(user_id, idname)}), "")
                    amount = parseInt(amount)
                    local new_weight = vRP.computeItemsWeight(chest.items)+vRP.getItemWeight(idname)*amount
                    if new_weight <= max_weight then
                        if amount >= 0 and vRP.tryGetInventoryItem(user_id, idname, amount, true) then
                            local citem = chest.items[idname]
                            if citem ~= nil then
                                citem.amount = citem.amount+amount
                            else
                                chest.items[idname] = {amount=amount}
                            end
                            if cb_in then cb_in(idname,amount) end
                            vRP.closeMenu(source)
                        end
                    else
                        TriggerClientEvent("pNotify:SendNotification", source, {
                            text = "Inventário cheio",
                            type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                            animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                        })
                    end
                end
                local ch_put = function(player, choice)
                    local submenu = build_itemlist_menu(lang.inventory.chest.put.title(), data.inventory, cb_put)
                    local weight = vRP.computeItemsWeight(data.inventory)
                    local max_weight = vRP.getInventoryMaxWeight(user_id)
                    local hue = math.floor(math.max(125*(1-weight/max_weight), 0))
                    submenu["<div class=\"dprogressbar\" data-value=\""..string.format("%.2f",weight/max_weight).."\" data-color=\"hsl("..hue..",100%,50%)\" data-bgcolor=\"hsl("..hue..",100%,25%)\" style=\"height: 12px; border: 3px solid black;\"></div>"] = {function()end, lang.inventory.info_weight({string.format("%.2f",weight),max_weight})}
                    submenu.onclose = function()
                        close_count = close_count-1
                        vRP.openMenu(player, menu)
                    end
                    close_count = close_count+1
                    vRP.openMenu(player, submenu)
                end
                menu["Retirar"] = {ch_take}
                menu["Colocar"] = {ch_put}
                menu.onclose = function()
                    if close_count == 0 then
                        vRP.setSData("chest:"..name, json.encode(chest.items))
                        chests[name] = nil
                        if cb_close then cb_close() end
                    end
                end
                vRP.openMenu(source, menu)
            else
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Este bau já foi aberto",
                    type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end
        end
    end
end

local function build_client_static_chests(source)
    local user_id = vRP.getUserId(source)
    if user_id then
        for k,v in pairs(cfg.static_chests) do
            local mtype,x,y,z = table.unpack(v)
            local schest = cfg.static_chest_types[mtype]
            if schest then
                local function schest_enter(source)
                    local user_id = vRP.getUserId(source)
                    if user_id ~= nil and vRP.hasPermissions(user_id,schest.permissions or {}) then
                        vRP.openChest(source, "static:"..k, schest.weight or 0)
                    end
                end
                local function schest_leave(source)
                    vRP.closeMenu(source)
                end
                vRPclient._addBlip(source,x,y,z,schest.blipid,schest.blipcolor,schest.title)
                vRPclient._addMarker(source,x,y,z-1,0.7,0.7,0.5,255,226,0,125,150)

                vRP.setArea(source,"vRP:static_chest:"..k,x,y,z,1,1.5,schest_enter,schest_leave)
            end
        end
    end
end

AddEventHandler("vRP:playerSpawn",function(user_id, source, first_spawn)
  	if first_spawn then
    	build_client_static_chests(source)
  	end
end)


