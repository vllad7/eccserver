local cfg = {}

-- start wallet/bank values
cfg.open_wallet = 300
cfg.open_bank = 800

cfg.display_css = [[
.div_carteira{
  position: absolute;
  top: 90px;
  right: 10px;
  font-size: 1.4em;
  font-weight: bold;
  color: white;
  text-transform: uppercase;
  text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.80);
}

.div_banco{
  position: absolute;
  top: 115px;
  right: 10px;
  font-size: 1.4em;
  font-weight: bold;
  color: white;
  text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.80);
}
  .div_carteira .symbol{

  content: url('https://i.imgur.com/BNeOko6.png');

}

.div_banco .symbol{
  content: url('https://i.imgur.com/ZP4cv71.png');

}
]]

return cfg