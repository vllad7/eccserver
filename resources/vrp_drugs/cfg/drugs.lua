--[[
    FiveM Scripts
    Copyright C 2018  Sighmir

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    at your option any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
]]


local cfg = {}
cfg.drugs= {
    ["maconha"] = {
        name = "maconha",
        desc = "",
        choices = function(args)
            local menu = {}
            menu["Fumar"] = {function(player,choice)
                local user_id = vRP.getUserId(player)
                if user_id ~= nil then
                    if vRP.tryGetInventoryItem(user_id,"maconha",1,false) then
                        vRPclient.varyHealth(player,25)
                        TriggerClientEvent("pNotify:SendNotification", player, {
                            text = "Fumando Maconha",
                            type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                            animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                        })
                        local seq = {
                            {"mp_player_int_uppersmoke","mp_player_int_smoke_enter",1},
                            {"mp_player_int_uppersmoke","mp_player_int_smoke",1},
                            {"mp_player_int_uppersmoke","mp_player_int_smoke",1},
                            {"mp_player_int_uppersmoke","mp_player_int_smoke",1},
                            {"mp_player_int_uppersmoke","mp_player_int_smoke_exit",1}
                        }
                        vRPclient.playAnim(player,true,seq,false)
                        SetTimeout(10000,function()
                            Dclient.playMovement(player,"MOVE_M@DRUNK@SLIGHTLYDRUNK",true,true,false,false)
                            Dclient.playScreenEffect(player, "DMT_flight", 120)
                        end)
                        SetTimeout(120000,function()
                            Dclient.resetMovement(player,false)
                        end)
                        vRP.closeMenu(player)
                    end
                end
            end}
            return menu
        end,
        weight = 0.1
    },
    ["cocaina"] = {
        name = "cocaina",
        desc = "",
        choices = function(args)
            local menu = {}
            menu["Cheirar"] = {function(player,choice)
                local user_id = vRP.getUserId(player)
                if user_id ~= nil then
                    if vRP.tryGetInventoryItem(user_id,"cocaina",1,false) then
                        vRPclient.varyHealth(player,25)
                        TriggerClientEvent("pNotify:SendNotification", player, {
                            text = "Cheirando Cocaína",
                            type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                            animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                        })
                        local seq = {
                            {"mp_player_int_uppersmoke","mp_player_int_smoke_enter",1},
                            {"mp_player_int_uppersmoke","mp_player_int_smoke",1},
                            {"mp_player_int_uppersmoke","mp_player_int_smoke",1},
                            {"mp_player_int_uppersmoke","mp_player_int_smoke",1},
                            {"mp_player_int_uppersmoke","mp_player_int_smoke_exit",1}
                        }
                        vRPclient.playAnim(player,true,seq,false)
                        SetTimeout(10000,function()
                            Dclient.playMovement(player,"MOVE_M@DRUNK@SLIGHTLYDRUNK",true,true,false,false)
                            Dclient.playScreenEffect(player, "DMT_flight", 120)
                        end)
                        SetTimeout(120000,function()
                            Dclient.resetMovement(player,false)
                        end)
                        vRP.closeMenu(player)
                    end
                end
            end}
            return menu
        end,
        weight = 0.1
    },
    ["LSD"] = {
        name = "LSD",
        desc = "",
        choices = function(args)
            local menu = {}
            menu["Tomar"] = {function(player,choice)
                local user_id = vRP.getUserId(player)
                if user_id ~= nil then
                    if vRP.tryGetInventoryItem(user_id,"LSD",1,false) then
                        vRPclient.varyHealth(player,25)
                        TriggerClientEvent("pNotify:SendNotification", player, {
                            text = "Tomando LSD",
                            type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                            animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                        })
                        local seq = {
                            {"mp_player_int_uppersmoke","mp_player_int_smoke_enter",1},
                            {"mp_player_int_uppersmoke","mp_player_int_smoke",1},
                            {"mp_player_int_uppersmoke","mp_player_int_smoke",1},
                            {"mp_player_int_uppersmoke","mp_player_int_smoke",1},
                            {"mp_player_int_uppersmoke","mp_player_int_smoke_exit",1}
                        }
                        vRPclient.playAnim(player,true,seq,false)
                        SetTimeout(10000,function()
                            Dclient.playMovement(player,"MOVE_M@DRUNK@SLIGHTLYDRUNK",true,true,false,false)
                            Dclient.playScreenEffect(player, "DMT_flight", 120)
                        end)
                        SetTimeout(120000,function()
                            Dclient.resetMovement(player,false)
                        end)
                        vRP.closeMenu(player)
                    end
                end
            end}
            return menu
        end,
        weight = 0.1
    },
    ["vodka"] = {
        name = "vodka",
        desc = "",
        choices = function(args)
            local menu = {}
            menu["drink"] = {function(player,choice)
                local user_id = vRP.getUserId(player)
                if user_id ~= nil then
                    if vRP.tryGetInventoryItem(user_id,"vodka",1,false) then
                        vRP.varyHunger(user_id,30)
                        vRP.varyThirst(user_id,-70)
                        vRP.varyNecessidades(user_id,10)
                        vRPclient.varyHealth(player,10)
                        TriggerClientEvent("pNotify:SendNotification", player, {
                            text = "Bebendo Vodka",
                            type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                            animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                        })
                        local seq = {
                            {"mp_player_intdrink","intro_bottle",1},
                            {"mp_player_intdrink","loop_bottle",1},
                            {"mp_player_intdrink","outro_bottle",1}
                        }
                        vRPclient.playAnim(player,true,seq,false)
                        SetTimeout(5000,function()
                            Dclient.playMovement(player,"MOVE_M@DRUNK@VERYDRUNK",true,true,false,false)
                            Dclient.playScreenEffect(player, "Rampage", 120)
                            SetTimeout(120000,function()
                                Dclient.resetMovement(player,false)
                            end)
                        end)
                        vRP.closeMenu(player)
                    end
                end
            end}
            return menu
        end,
        weight = 0.5
    },
}

return cfg