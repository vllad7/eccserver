local cfg = module("cfg/business")
local htmlEntities = module("lib/htmlEntities")
local lang = vRP.lang
local sanitizes = module("cfg/sanitizes")

vRP.prepare("vRP/business_tables",[[
CREATE TABLE IF NOT EXISTS vrp_user_business(
  user_id INTEGER,
  name VARCHAR(30),
  description TEXT,
  capital INTEGER,
  laundered INTEGER,
  reset_timestamp INTEGER,
  CONSTRAINT pk_user_business PRIMARY KEY(user_id),
  CONSTRAINT fk_user_business_users FOREIGN KEY(user_id) REFERENCES vrp_users(id) ON DELETE CASCADE
);
]])

vRP.prepare("vRP/create_business","INSERT IGNORE INTO vrp_user_business(user_id,name,description,capital,laundered,reset_timestamp) VALUES(@user_id,@name,'',@capital,0,@time)")
vRP.prepare("vRP/delete_business","DELETE FROM vrp_user_business WHERE user_id = @user_id")
vRP.prepare("vRP/get_business","SELECT name,description,capital,laundered,reset_timestamp FROM vrp_user_business WHERE user_id = @user_id")
vRP.prepare("vRP/add_capital","UPDATE vrp_user_business SET capital = capital + @capital WHERE user_id = @user_id")
vRP.prepare("vRP/add_laundered","UPDATE vrp_user_business SET laundered = laundered + @laundered WHERE user_id = @user_id")
vRP.prepare("vRP/get_business_page","SELECT user_id,name,description,capital FROM vrp_user_business ORDER BY capital DESC LIMIT @b,@n")
vRP.prepare("vRP/reset_transfer","UPDATE vrp_user_business SET laundered = 0, reset_timestamp = @time WHERE user_id = @user_id")

async(function()
    vRP.execute("vRP/business_tables")
end)

function vRP.getUserBusiness(user_id, cbr)
    if user_id then
        local rows = vRP.query("vRP/get_business", {user_id = user_id})
        local business = rows[1]
        if business and os.time() >= business.reset_timestamp+cfg.transfer_reset_interval*60 then
            vRP.execute("vRP/reset_transfer", {user_id = user_id, time = os.time()})
            business.laundered = 0
        end
        return business
    end
end

function vRP.closeBusiness(user_id)
    vRP.execute("vRP/delete_business", {user_id = user_id})
end

local function open_business_directory(player,page)
    if page < 0 then page = 0 end
    local menu = {name="Ver Empresas".." (Página "..page..")",css={header_color="rgba(255,128,0,0.99)"}}
    local rows = vRP.query("vRP/get_business_page", {b = page*10, n = 10})
    local count = 0
    for k,v in pairs(rows) do
        count = count+1
        local row = v
        if row.user_id ~= nil then
            local identity = vRP.getUserIdentity(row.user_id)
            if identity then
                menu[htmlEntities.encode(row.name)] = {function()end, lang.business.directory.info({row.capital,htmlEntities.encode(identity.name),htmlEntities.encode(identity.firstname),identity.registration,identity.phone})}
            end

            count = count-1
            if count == 0 then
                menu["> Próxima"] = {function() open_business_directory(player,page+1) end}
                menu["> Anterior"] = {function() open_business_directory(player,page-1) end}

                vRP.openMenu(player,menu)
            end
        end
    end
end

local function business_enter(source)
    local source = source
    local user_id = vRP.getUserId(source)
    if user_id then
        local menu = {name="Empresa",css={header_color="rgba(240,203,88,0.75)"}}
        local business = vRP.getUserBusiness(user_id)
        if business then
            menu["Informações"] = {function(player,choice)
            end, lang.business.info.info({htmlEntities.encode(business.name), business.capital, business.laundered})}
			
			menu["Adicionar Capital"] = {function(player,choice)
				local amount = vRP.prompt(player,"Quanto de capital desse adicionar?","")
				amount = parseInt(amount)
				if amount > 0 then
					if vRP.tryPayment(user_id,amount) then
						vRP.execute("vRP/add_capital", {user_id = user_id, capital = amount})
						TriggerClientEvent("pNotify:SendNotification", player, {
							text = "Você adicionou " .. amount .. " reais ao capital da sua empresa",
							type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
							animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
						})
					else
						TriggerClientEvent("pNotify:SendNotification", player, {
							text = "Dinheiro Insuficiente",
							type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
							animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
						})
					end
				else
					TriggerClientEvent("pNotify:SendNotification", player, {
						text = "Valor Inválido",
						type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
						animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
					})
				end
			end}

			menu["Lavar Dinheiro"] = {function(player,choice)
				local business = vRP.getUserBusiness(user_id)
				local launder_left = math.min(business.capital-business.laundered,vRP.getInventoryItemAmount(user_id,"Dinheiro Sujo")) -- compute launder capacity
				local amount = vRP.prompt(player,"Qual a quantia que deseja lavar ? (Você ainda pode lavar " .. launder_left .. " reais)","")
				amount = parseInt(amount)
				if amount > 0 and amount <= launder_left then
					if vRP.tryGetInventoryItem(user_id,"Dinheiro Sujo",amount,false) then
						vRP.execute("vRP/add_laundered", {user_id = user_id, laundered = amount})
						vRP.giveMoney(user_id,amount)
						TriggerClientEvent("pNotify:SendNotification", player, {
							text = "Você lavou " .. amount .. " reais de dinheiro sujo",
							type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
							animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
						})
					else
						TriggerClientEvent("pNotify:SendNotification", player, {
							text = "Dinheiro Insuficiente",
							type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
							animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
						})
					end
				else
					TriggerClientEvent("pNotify:SendNotification", player, {
						text = "Valor Inválido",
						type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
						animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
					})
				end
			end}
        else
            menu["Abrir Empresa"] = {function(player,choice)
                local name = vRP.prompt(player,lang.business.open.prompt_name({30}),"")
                if string.len(name) >= 2 and string.len(name) <= 30 then
                    name = sanitizeString(name, sanitizes.business_name[1], sanitizes.business_name[2])
                    local capital = vRP.prompt(player,"Qual o capital inicial ? (Minimo "..cfg.minimum_capital " reais)","")
                    capital = parseInt(capital)
                    if capital >= cfg.minimum_capital then
                        if vRP.tryPayment(user_id,capital) then
                            vRP.execute("vRP/create_business", {
                                user_id = user_id,
                                name = name,
                                capital = capital,
                                time = os.time()
                            })
                            TriggerClientEvent("pNotify:SendNotification", player, {
                                text = "Empresa Aberta com sucesso",
                                type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                                animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                            })
                            vRP.closeMenu(player)
                        else
                            TriggerClientEvent("pNotify:SendNotification", player, {
                                text = "Dinheiro Insuficiente",
                                type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                                animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                            })
                        end
                    else
                        TriggerClientEvent("pNotify:SendNotification", player, {
                            text = "Valor Inválido",
                            type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                            animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                        })
                    end
                else
                    TriggerClientEvent("pNotify:SendNotification", player, {
                        text = "Nome Inválido",
                        type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                        animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                    })
                end
            end,lang.business.open.description({cfg.minimum_capital})}
        end
        menu["Empresas"] = {function(player,choice)
            open_business_directory(player,0)
        end}
        vRP.openMenu(source,menu)
    end
end

local function business_leave(source)
    vRP.closeMenu(source)
end

local function build_client_business(source)
    local user_id = vRP.getUserId(source)
    if user_id then
        for k,v in pairs(cfg.commerce_chambers) do
            local x,y,z = table.unpack(v)
            vRPclient._addBlip(source,x,y,z,cfg.blip[1],cfg.blip[2],"Empresas")
            vRPclient._addMarker(source,x,y,z-1,0.7,0.7,0.5,0,255,125,125,150)
            vRP.setArea(source,"vRP:business"..k,x,y,z,1,1.5,business_enter,business_leave)
        end
    end
end

AddEventHandler("vRP:playerSpawn",function(user_id, source, first_spawn)
    if first_spawn then
        build_client_business(source)
    end
end)