local htmlEntities = module("lib/htmlEntities")
local Tools = module("lib/Tools")
local player_lists = {}

local function ch_list(player,choice)
    local user_id = vRP.getUserId(player)
    if user_id and vRP.hasPermission(user_id,"lista.de.jogadores") then
        if player_lists[player] then
            player_lists[player] = nil
            vRPclient._removeDiv(player,{"user_list"})
        else
            local content = ""
            for k,v in pairs(vRP.rusers) do
                local source = vRP.getUserSource(k)
                local identity = vRP.getUserIdentity(k)
                if source then
                    content = content.."<br />"..k.." => <span class=\"pseudo\">"..vRP.getPlayerName(source).."</span> <span class=\"endpoint\">"..vRP.getPlayerEndpoint(source).."</span>"
                    if identity then
                        content = content.." <span class=\"name\">"..htmlEntities.encode(identity.firstname).." "..htmlEntities.encode(identity.name).."</span> <span class=\"reg\">"..identity.registration.."</span> <span class=\"phone\">"..identity.phone.."</span>"
                    end
                end
            end

            player_lists[player] = true
            local css = [[
				.div_user_list{ 
				margin: auto; 
				padding: 8px; 
				width: 650px; 
				margin-top: 80px; 
				background: black; 
				color: white; 
				font-weight: bold; 
				font-size: 1.1em;
				} 

				.div_user_list .pseudo{ 
				color: rgb(0,255,125);
				}

				.div_user_list .endpoint{ 
				color: rgb(255,0,0);
				}

				.div_user_list .name{ 
				color: #309eff;
				}

				.div_user_list .reg{ 
				color: rgb(0,125,255);
				}
							
				.div_user_list .phone{ 
				color: rgb(211, 0, 255);
				}
			]]
            vRPclient._setDiv(player, "user_list", css, content)
        end
    end
end

local function ch_whitelist(player,choice)
    local user_id = vRP.getUserId(player)
    if user_id and vRP.hasPermission(user_id,"adicionar.whitelist") then
        local id = vRP.prompt(player,"ID do usuário a adicionar a whitelist","")
        id = parseInt(id)
        vRP.setWhitelisted(id,true)
        TriggerClientEvent("pNotify:SendNotification", player, {
            text = "ID " .. id .. " adicionado a whitelist",
            type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
            animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
        })
    end
end

local function ch_unwhitelist(player,choice)
    local user_id = vRP.getUserId(player)
    if user_id and vRP.hasPermission(user_id,"remover.whitelist") then
        local id = vRP.prompt(player,"ID do usuário a remover da whitelist","")
        id = parseInt(id)
        vRP.setWhitelisted(id,false)
        TriggerClientEvent("pNotify:SendNotification", player, {
            text = "ID " .. id .. " removido da whitelist",
            type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
            animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
        })
    end
end

local function ch_addgroup(player,choice)
    local user_id = vRP.getUserId(player)
    if user_id ~= nil and vRP.hasPermission(user_id,"adicionar.grupo") then
        local id = vRP.prompt(player,"ID do usuário a adicionar ao grupo","")
        id = parseInt(id)
        local group = vRP.prompt(player,"Qual grupo irá adiciona-lo?","")
        if group then
            vRP.addUserGroup(id,group)
            TriggerClientEvent("pNotify:SendNotification", player, {
                text = "ID " .. id .. " adicionado ao grupo " .. group .. ".",
                type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
            })
        end
    end
end

local function ch_removegroup(player,choice)
    local user_id = vRP.getUserId(player)
    if user_id and vRP.hasPermission(user_id,"remover.grupo") then
        local id = vRP.prompt(player,"ID do usuário a adicionar ao grupo","")
        id = parseInt(id)
        local group = vRP.prompt(player,"Qual grupo irá remove-lo?","")
        if group then
            vRP.removeUserGroup(id,group)
            TriggerClientEvent("pNotify:SendNotification", player, {
                text = "ID " .. id .. " removido do grupo" .. group .. ".",
                type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
            })
        end
    end
end

local function ch_kick(player,choice)
    local user_id = vRP.getUserId(player)
    if user_id and vRP.hasPermission(user_id,"kickar.jogador") then
        local id = vRP.prompt(player,"ID do usuário a ser kickado","")
        id = parseInt(id)
        local reason = vRP.prompt(player,"Qual o motivo do kick?","")
        local source = vRP.getUserSource(id)
        if source then
            vRP.kick(source,reason)
            TriggerClientEvent("pNotify:SendNotification", player, {
                text = "ID " .. id .. " foi kickado por " .. reason .. ".",
                type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
            })
        end
    end
end

local function ch_ban(player,choice)
    local user_id = vRP.getUserId(player)
    if user_id and vRP.hasPermission(user_id,"banir.jogador") then
        local id = vRP.prompt(player,"ID do usuário a ser banido","")
        id = parseInt(id)
        local reason = vRP.prompt(player,"Qual o motivo do banimento?","")
        local source = vRP.getUserSource(id)
        if source then
            vRP.ban(source,reason)
            TriggerClientEvent("pNotify:SendNotification", player, {
                text = "ID " .. id .. " foi banido por " .. reason .. ".",
                type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
            })
        end
    end
end

local function ch_unban(player,choice)
    local user_id = vRP.getUserId(player)
    if user_id and vRP.hasPermission(user_id,"desbanir.jogador") then
        local id = vRP.prompt(player,"ID do usuário a ser desbanido","")
        id = parseInt(id)
        vRP.setBanned(id,false)
        TriggerClientEvent("pNotify:SendNotification", player, {
            text = "ID " .. id .. " foi desbanido.",
            type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
            animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
        })
    end
end

local function ch_emote(player,choice)
    local user_id = vRP.getUserId(player)
    if user_id and vRP.hasPermission(user_id,"animacao.customizada") then
        local content = vRP.prompt(player,"Qual a sequência da animação? ('dict anim optional_loops')","")
        local seq = {}
        for line in string.gmatch(content,"[^\n]+") do
            local args = {}
            for arg in string.gmatch(line,"[^%s]+") do
                table.insert(args,arg)
            end

            table.insert(seq,{args[1] or "", args[2] or "", args[3] or 1})
        end

        vRPclient._playAnim(player, true,seq,false)
    end
end

local function ch_sound(player,choice)
    local user_id = vRP.getUserId(player)
    if user_id and vRP.hasPermission(user_id,"som.customizado") then
        local content = vRP.prompt(player,"Qual som deseja tocar? ('dict name')","")
        local args = {}
        for arg in string.gmatch(content,"[^%s]+") do
            table.insert(args,arg)
        end
        vRPclient._playSound(player, args[1] or "", args[2] or "")
    end
end

local function ch_coords(player,choice)
    local x,y,z = vRPclient.getPosition(player)
    vRP.prompt(player,"Copie as coordenadas com CTRL+A e depois CTRL+C","x = " .. x .. ",y = " .. y .. ",z = " .. z)
end

local function ch_tptome(player,choice)
    local x,y,z = vRPclient.getPosition(player)
    local user_id = vRP.prompt(player,"ID do usuário a ser teleportado até você","")
    local tplayer = vRP.getUserSource(tonumber(user_id))
    if tplayer then
        vRPclient._teleport(tplayer,x,y,z)
    end
end

local function ch_tpto(player,choice)
    local user_id = vRP.prompt(player,"Qual ID do jogador que deseja teleportar-se?","")
    local tplayer = vRP.getUserSource(tonumber(user_id))
    if tplayer then
        vRPclient._teleport(player, vRPclient.getPosition(tplayer))
    end
end

local function ch_tptocoords(player,choice)
    local fcoords = vRP.prompt(player,"Digite ou cole as coordenadas para teleportar-se","")
    local coords = {}
    for coord in string.gmatch(fcoords or "0,0,0","[^,]+") do
        table.insert(coords,tonumber(coord))
    end

    vRPclient._teleport(player, coords[1] or 0, coords[2] or 0, coords[3] or 0)
end

local function ch_givemoney(player,choice)
    local user_id = vRP.getUserId(player)
    if user_id then
        local amount = vRP.prompt(player,"Quanto deseja receber?","")
        amount = parseInt(amount)
        vRP.giveMoney(user_id, amount)
    end
end

local function ch_giveitem(player,choice)
    local user_id = vRP.getUserId(player)
    if user_id then
        local idname = vRP.prompt(player,"Qual o nome do item que deseja receber?","")
        idname = idname or ""
        local amount = vRP.prompt(player,"Qual a quantidade que deseja receber?","")
        amount = parseInt(amount)
        vRP.giveInventoryItem(user_id, idname, amount,true)
    end
end

local function ch_calladmin(player,choice)
    local user_id = vRP.getUserId(player)
    if user_id then
        local desc = vRP.prompt(player,"Qual o seu problema?","") or ""
        local answered = false
        local players = {}
        for k,v in pairs(vRP.rusers) do
            local player = vRP.getUserSource(tonumber(k))
            -- check user
            if vRP.hasPermission(k,"tickets.administracao") and player then
                table.insert(players,player)
            end
        end

        -- send notify and alert to all listening players
        for k,v in pairs(players) do
            async(function()
                local ok = vRP.request(v,"ID "..user_id.." requisitou um administrador, deseja ir até ele? "..htmlEntities.encode(desc), 60)
                if ok then -- take the call
                    if not answered then
                        -- answer the call
                        TriggerClientEvent("pNotify:SendNotification", player, {
                            text = "Um administrador está a caminho",
                            type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                            animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                        })
                        vRPclient._teleport(v, vRPclient.getPosition(player))
                        answered = true
                else
                    TriggerClientEvent("pNotify:SendNotification", v, {
                        text = "Este ticket já foi aceito",
                        type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                        animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                    })
                end
                end
            end)
        end
    end
end

local player_customs = {}

local function ch_display_custom(player, choice)
    local custom = vRPclient.getCustomization(player)
    if player_customs[player] then -- hide
        player_customs[player] = nil
        vRPclient._removeDiv(player,"customization")
    else -- show
        local content = ""
        for k,v in pairs(custom) do
            content = content..k.." => "..json.encode(v).."<br />"
        end

        player_customs[player] = true
        vRPclient._setDiv(player,"customization",".div_customization{ margin: auto; padding: 8px; width: 500px; margin-top: 80px; background: black; color: white; font-weight: bold; ", content)
    end
end

local function ch_noclip(player, choice)
    vRPclient._toggleNoclip(player)
end

local function ch_audiosource(player, choice)
    local infos = splitString(vRP.prompt(player, "Audio source: name=url, omit url to delete the named source.", ""), "=")
    local name = infos[1]
    local url = infos[2]

    if name and string.len(name) > 0 then
        if url and string.len(url) > 0 then
            local x,y,z = vRPclient.getPosition(player)
            vRPclient._setAudioSource(-1,"vRP:admin:"..name,url,0.5,x,y,z,125)
        else
            vRPclient._removeAudioSource(-1,"vRP:admin:"..name)
        end
    end
end

local gods = {}
function task_god()
    SetTimeout(1000, task_god)

    for k,v in pairs(gods) do
        vRP.setHunger(v, 0)
        vRP.setThirst(v, 0)
        vRP.setSono(v, 0)
        vRP.setNecessidades(v, 0)

        local player = vRP.getUserSource(v)
        if player ~= nil then
            vRPclient.setHealth(player, 200)
        end
    end
end
task_god()

local ch_godmode = {function(player,choice)
    local user_id = vRP.getUserId(player)
    if user_id ~= nil then
        if gods[player] then
            gods[player] = nil
            TriggerClientEvent("pNotify:SendNotification", player, {
                text = "GOD MODE Desativado",
                type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
            })
        else
            gods[player] = user_id
            TriggerClientEvent("pNotify:SendNotification", player, {
                text = "GOD MODE Ativado",
                type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
            })
        end
    end
end}

vRP.registerMenuBuilder("main", function(add, data)
    local user_id = vRP.getUserId(data.player)
    if user_id then
        local choices = {}

        -- build admin menu
        choices["Administração"] = {function(player,choice)
            local menu  = vRP.buildMenu("admin", {player = player})
            menu.name = "Administração"
            menu.css={top="75px",header_color="rgba(255,150,0,0.99)"}
            menu.onclose = function(player) vRP.openMainMenu(player) end -- nest menu

            if vRP.hasPermission(user_id,"lista.de.jogadores") then
                menu["Lista de Jogadores"] = {ch_list}
            end

            if vRP.hasPermission(user_id,"adicionar.whitelist") then
                menu["Adicionar Jogador a Whitelist"] = {ch_whitelist}
            end

            if vRP.hasPermission(user_id,"adicionar.jogador.grupo") then
                menu["Adicionar Jogador a Grupo"] = {ch_addgroup}
            end

            if vRP.hasPermission(user_id,"remover.jogador.grupo") then
                menu["Retirar Jogador de Grupo"] = {ch_removegroup}
            end

            if vRP.hasPermission(user_id,"remover.whitelist") then
                menu["Retirar Jogador da Whitelist"] = {ch_unwhitelist}
            end

            if vRP.hasPermission(user_id,"kickar.jogador") then
                menu["Kickar Jogador"] = {ch_kick}
            end

            if vRP.hasPermission(user_id,"banir.jogador") then
                menu["Banir Jogador"] = {ch_ban}
            end

            if vRP.hasPermission(user_id,"god.mode") then
                menu["GOD MODE"] = ch_godmode
            end

            if vRP.hasPermission(user_id,"desbanir.jogador") then
                menu["Desbanir Jogador"] = {ch_unban}
            end

            if vRP.hasPermission(user_id,"no.clip") then
                menu["NO CLIP"] = {ch_noclip}
            end

            --[[
      if vRP.hasPermission(user_id,"animacao.customizada") then
        menu["Animação Customizada"] = {ch_emote}
      end

      if vRP.hasPermission(user_id,"som.customizado") then
        menu["Som Customizado"] = {ch_sound}
      end

      if vRP.hasPermission(user_id,"som.customizado") then
        menu["Audiosource Customizado"] = {ch_audiosource}
      end]]

            if vRP.hasPermission(user_id,"pegar.coordenadas") then
                menu["Pegar Coordenadas"] = {ch_coords}
            end

            if vRP.hasPermission(user_id,"teleportar.ate.mim") then
                menu["Teleportar Jogador até mim"] = {ch_tptome}
            end

            if vRP.hasPermission(user_id,"teleportar.ate.jogador") then
                menu["Teleportar até Jogador"] = {ch_tpto}
            end

            if vRP.hasPermission(user_id,"teleportar.coordenadas") then
                menu["Teleportar até Coordenadas"] = {ch_tptocoords}
            end

            if vRP.hasPermission(user_id,"pegar.dinheiro") then
                menu["Pegar Dinheiro"] = {ch_givemoney}
            end

            if vRP.hasPermission(user_id,"criar.item") then
                menu["Criar Item"] = {ch_giveitem}
            end

            if vRP.hasPermission(user_id,"mostrar.customizacao") then
                menu["Mostrar Customização"] = {ch_display_custom}
            end

            if vRP.hasPermission(user_id,"chamar.administrador") then
                menu["Chamar Administrador"] = {ch_calladmin}
            end

            vRP.openMenu(player,menu)
        end}

        add(choices)
    end
end)
