
local cfg = {}

-- exp notes:
-- levels are defined by the amount of xp
-- with a step of 5: 5|15|30|50|75 (by default)
-- total exp for a specific level, exp = step*lvl*(lvl+1)/2
-- level for a specific exp amount, lvl = (sqrt(1+8*exp/step)-1)/2

-- define groups of aptitudes
--- _title: title of the group
--- map of aptitude => {title,init_exp,max_exp}
---- max_exp: -1 for infinite exp
cfg.gaptitudes = {
  ["fisico"] = {
    _title = "Fisico",
    ["forca"] = {"Força", 30, 105} -- required, level 3 to 6 (by default, can carry 10kg per level)
  },
  ["Ciência"] = {
    _title = "Science",
    ["Química"] = {"Química", 0, -1}, -- example
    ["Matemática"] = {"Matemática", 0, -1} -- example
  },
  ["Laboratório"] = {
    _title = "Laboratório de drogas",
    ["Cocaína"] = {"Cocaína", 0, -1},
    ["Maconha"] = {"Maconha", 0, -1},
    ["LSD"] = {"LSD", 0, -1}
  },
  ["Hacker"] = {
    _title = "Estuda linguagens de programação.",
    ["Lógica"] = {"Lógica", 0, -1},
    ["C++"] = {"C++", 0, -1},
    ["Lua"] = {"Lua", 0, -1},
    ["Engenharia reversa"] = {"Engenharia reversa", 0, -1}
  }
}

return cfg
