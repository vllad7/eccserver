local cfg = module("cfg/item_transformers")
local lang = vRP.lang
local transformers = {}

local function tr_remove_player(tr,player)
	local recipe = tr.players[player] or ""
	tr.players[player] = nil
	vRPclient._removeProgressBar(player,"vRP:tr:"..tr.name)
	vRP.closeMenu(player)
	if tr.itemtr.onstop then tr.itemtr.onstop(player,recipe) end
end

local function tr_add_player(tr,player,recipe)
	tr.players[player] = recipe
	vRP.closeMenu(player)
	vRPclient._setProgressBar(player,"vRP:tr:"..tr.name,"center",recipe.."...",tr.itemtr.r,tr.itemtr.g,tr.itemtr.b,0)
	if tr.itemtr.onstart then 
		tr.itemtr.onstart(player,recipe) 
	end
end

local function tr_tick(tr)
	for k,v in pairs(tr.players) do
		local user_id = vRP.getUserId(tonumber(k))
		if v and user_id then
			local recipe = tr.itemtr.recipes[v]
			if tr.units > 0 and recipe then
				local reagents_ok = true
				for l,w in pairs(recipe.reagents) do
					reagents_ok = reagents_ok and (vRP.getInventoryItemAmount(user_id,l) >= w)
				end
				local money_ok = (vRP.getMoney(user_id) >= recipe.in_money)
				local out_witems = {}
				for k,v in pairs(recipe.products) do
					out_witems[k] = {amount=v}
				end
				local in_witems = {}
				for k,v in pairs(recipe.reagents) do
					in_witems[k] = {amount=v}
				end
				local new_weight = vRP.getInventoryWeight(user_id)+vRP.computeItemsWeight(out_witems)-vRP.computeItemsWeight(in_witems)
				local inventory_ok = true
				if new_weight > vRP.getInventoryMaxWeight(user_id) then
					inventory_ok = false
					vRPclient._notify(tonumber(k), lang.inventory.full())
				end
				if not money_ok then
					vRPclient._notify(tonumber(k), lang.money.not_enough())
				end
				if not reagents_ok then
					vRPclient._notify(tonumber(k), lang.itemtr.not_enough_reagents())
				end
				if money_ok and reagents_ok and inventory_ok then
					tr.units = tr.units-1
					if recipe.in_money > 0 then vRP.tryPayment(user_id,recipe.in_money) end
					for l,w in pairs(recipe.reagents) do
						vRP.tryGetInventoryItem(user_id,l,w,true)
					end
					if recipe.out_money > 0 then vRP.giveMoney(user_id,recipe.out_money) end
					for l,w in pairs(recipe.products) do
						vRP.giveInventoryItem(user_id,l,w,true)
					end
					for l,w in pairs(recipe.aptitudes or {}) do
						local parts = splitString(l,".")
						if #parts == 2 then
							vRP.varyExp(user_id,parts[1],parts[2],w)
						end
					end
					if tr.itemtr.onstep then tr.itemtr.onstep(tonumber(k),v) end
				end
			end
		end
  	end
	for k,v in pairs(tr.players) do
		vRPclient._setProgressBarValue(k,"vRP:tr:"..tr.name,math.floor(tr.units/tr.itemtr.max_units*100.0))
		if tr.units > 0 then
			vRPclient._setProgressBarText(k,"vRP:tr:"..tr.name,v.."... "..tr.units.." de "..tr.itemtr.max_units)
		else
			vRPclient._setProgressBarText(k,"vRP:tr:"..tr.name,"espere carregar")
		end
	end
end

local function bind_tr_area(player,tr)
  	vRP.setArea(player,"vRP:tr:"..tr.name,tr.itemtr.x,tr.itemtr.y,tr.itemtr.z,tr.itemtr.radius,tr.itemtr.height,tr.enter,tr.leave)
end

local function unbind_tr_area(player,tr)
  	vRP.removeArea(player,"vRP:tr:"..tr.name)
end

function vRP.setItemTransformer(name,itemtr)
  	vRP.removeItemTransformer(name)
  	local tr = {itemtr=itemtr}
  	tr.name = name
  	transformers[name] = tr
  	tr.units = 10000
  	tr.players = {}
  	tr.menu = {name=itemtr.name,css={header_color="rgba("..itemtr.r..","..itemtr.g..","..itemtr.b..",0.99)"}}
  	for action,recipe in pairs(tr.itemtr.recipes) do
    	local info = "<br /><br />"
    	if recipe.in_money > 0 then info = info.."- "..recipe.in_money end
    	for k,v in pairs(recipe.reagents) do
      		local item_name, item_desc, item_weight = vRP.getItemDefinition(k)
      		if item_name then
        		info = info.."<br />"..v.." "..item_name
      		end
   	 	end
    	info = info.."<br /><span style=\"color: rgb(0,255,125)\">=></span>"
    	if recipe.out_money > 0 then info = info.."<br />+ "..recipe.out_money end
    	for k,v in pairs(recipe.products) do
      		local item_name, item_desc, item_weight = vRP.getItemDefinition(k)
      		if item_name then
        		info = info.."<br />"..v.." "..item_name
      		end
    	end
    	for k,v in pairs(recipe.aptitudes or {}) do
      		local parts = splitString(k,".")
      		if #parts == 2 then
        		local def = vRP.getAptitudeDefinition(parts[1],parts[2])
        		if def then
          			info = info.."<br />[EXP] "..v.." "..vRP.getAptitudeGroupTitle(parts[1]).."/"..def[1]
        		end
      		end
    	end

    	tr.menu[action] = {function(player,choice)
      		if vRP.inArea(player, "vRP:tr:"..tr.name) then
        		tr_add_player(tr,player,action)
      		end
    	end}
  	end

  	tr.enter = function(player,area)
    	local user_id = vRP.getUserId(player)
    	if user_id and vRP.hasPermissions(user_id,itemtr.permissions or {}) then
      		vRP.openMenu(player, tr.menu)
    	end
  	end

  	tr.leave = function(player,area)
    	tr_remove_player(tr, player)
  	end

  	for k,v in pairs(vRP.rusers) do
    	local source = vRP.getUserSource(k)
    	if source then
      		bind_tr_area(source,tr)
    	end
  	end
end

function vRP.removeItemTransformer(name)
	local tr = transformers[name]
	if tr then
		local players = {}
		for k,v in pairs(tr.players) do
			players[k] = v
		end
		for k,v in pairs(players) do
			tr_remove_player(tr,k)
		end
		for k,v in pairs(vRP.rusers) do
			local source = vRP.getUserSource(k)
			if source then
				unbind_tr_area(source,tr)
			end
		end
		transformers[name] = nil
	end
end

local function transformers_tick()
  	SetTimeout(0,function()
    	for k,tr in pairs(transformers) do
      		tr_tick(tr)
    	end
  	end)
  	SetTimeout(3000,transformers_tick)
end
transformers_tick()

local function transformers_regen()
  	for k,tr in pairs(transformers) do
    	tr.units = tr.units+tr.itemtr.units_per_minute
    	if tr.units >= tr.itemtr.max_units then tr.units = tr.itemtr.max_units end
  	end
  	SetTimeout(30000,transformers_regen) -- Recupera uma unidade da barra a cada 30s
end
transformers_regen()

AddEventHandler("vRP:playerSpawn",function(user_id, source, first_spawn)
  	if first_spawn then
    	for k,tr in pairs(transformers) do
     		bind_tr_area(source,tr)
    	end
  	end
end)

SetTimeout(5000,function()
  	for k,v in pairs(cfg.item_transformers) do
    	vRP.setItemTransformer("cfg:"..k,v)
  	end
end)