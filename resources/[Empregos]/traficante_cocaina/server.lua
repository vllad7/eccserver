-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- VARIÁVEIS LOCAIS
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP")
vRPps = {}
Tunnel.bindInterface("traficante_cocaina",vRPps)

function vRPps.PermissaoCoca()
    local user_id = vRP.getUserId(source)
    return vRP.hasPermission(user_id, zezao.permissao)
end

function vRPps.ChecarCocaina()
    local source = source
	local user_id = vRP.getUserId(source)
  	return vRP.getInventoryItemAmount(user_id,"cocaina")
end

RegisterServerEvent("zezao_cocaina:permissao")
AddEventHandler("zezao_cocaina:permissao", function ()
	local source = source
    local user_id = vRP.getUserId(source)
    if vRP.hasPermission(user_id,zezao.permissao) then
    	TriggerClientEvent("zezao_cocaina:entregar", source)
    else
    	TriggerClientEvent("pNotify:SendNotification", source, {
            text = "Você não é um Traficante de Cocaina",
            type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
            animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
        })
    end
end)

RegisterServerEvent("zezao_cocaina:remover")
AddEventHandler("zezao_cocaina:remover",function(cocainas)
    local source = source
    local user_id = vRP.getUserId(source)
    vRP.tryGetInventoryItem(user_id,"cocaina",cocainas,false)	    
end)

RegisterServerEvent("zezao_cocaina:pagamento")
AddEventHandler("zezao_cocaina:pagamento",function(PagamentoM)
    local source = source
    local user_id = vRP.getUserId(source)
    local PagamentoM = math.floor(PagamentoM+0.5)
    if zezao.limpo then
        vRP.giveMoney(user_id,PagamentoM)
    else
        vRP.giveInventoryItem(user_id,"Dinheiro Sujo",PagamentoM,false)
    end
    TriggerClientEvent("pNotify:SendNotification", source, {
        text = "Você recebeu " .. PagamentoM .. " reais pelo serviço",
        type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
        animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
    }) 
end)

RegisterServerEvent("zezao_cocaina:gorjeta")
AddEventHandler("zezao_cocaina:gorjeta",function(GorjetaM)
    local source = source
    local user_id = vRP.getUserId(source)
    local GorjetaM = math.floor(GorjetaM+0.5)
    vRP.giveMoney(user_id,GorjetaM)
    TriggerClientEvent("pNotify:SendNotification", source, {
        text = "Obrigado pela entrega, tome aqui " .. GorjetaM .. " reais para você",
        type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
        animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
    })   
end)