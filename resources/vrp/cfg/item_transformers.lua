local cfg = {}

cfg.item_transformers = {
    {
        name="Academia",
        r=255,g=125,b=0,
        max_units=10000,
        units_per_minute=1,
        x=-1202.96252441406,y=-1566.14086914063,z=4.61040639877319,
        radius=7.5, height=1.5,
        recipes = {
            ["Força"] = {
                description="",
                in_money=0,
                out_money=0,
                reagents={},
                products={},
                aptitudes={
                    ["fisico.forca"] = 0.5
                }
            }
        }
    },

    {
        name="Hacker",
        permissions = {"hackear.cartao.credito"},
        r=255,g=125,b=0,
        max_units=400,
        units_per_minute=1,
        x=1272.7529296875,y=-1711.9212646484,z=54.771450042725,
        radius=2, height=1.0,
        recipes = {
            ["Hackeando..."] = {
                description="",
                in_money=0,
                out_money=0,
                reagents={},
                products={
                    ["Dinheiro Sujo"] = 35
                },
                aptitudes={
                    ["Hacker.Engenharia reversa"] = 0.05
                }
            }
        }
    },

    {
        name="Lavagem de Dinheiro",
        r=0,g=125,b=255,
        max_units=500,
        units_per_minute=1,
        x=1122.0144042969,y=-3196.5224609375,z=-40.398097991943,
        radius=2, height=1,
        recipes = {
            ["Lavando Dinheiro..."] = {
                description="",
                in_money=0,
                out_money=500,
                reagents={
                    ["Dinheiro Sujo"] = 1000
                },
                products={},
                aptitudes={}
            }
        }
    },

    {
        name="Campo de Maconha",
        permissions = {"colher.maconha"},
        r=0,g=200,b=0,
        max_units=400,
        units_per_minute=1,
        x=2222.228515625,y=5577.2719726563,z=53.840530395508,
        radius=3, height=1,
        recipes = {
            ["Colhendo Maconha..."] = {
                description="",
                in_money=0,
                out_money=0,
                reagents={},
                products={
                    ["Cannabis"] = 1
                },
                aptitudes={
                    ["Laboratório.Maconha"] = 1,
                    ["Ciência.Química"] = 1
                }
            }
        }
    },

    {
        name="Processamento de Maconha",
        permissions = {"processar.maconha"},
        r=0,g=200,b=0,
        max_units=400,
        units_per_minute=1,
        x=1057.4676513672,y=-3195.8410644531,z=-39.161312103271,
        radius=2, height=1,
        recipes = {
            ["Processando Maconha..."] = {
                description="",
                in_money=0,
                out_money=0,
                reagents={
                    ["Cannabis"] = 1
                },
                products={
                    ["Maconha"] = 1
                },
                aptitudes={
                    ["Laboratório.Maconha"] = 1,
                    ["Ciência.Química"] = 1
                }
            }
        }
    },

    {
        name="Biqueira (Maconha)",
        permissions = {"vender.maconha"},
        r=0,g=255,b=0,
        max_units=300,
        units_per_minute=1,
        x=159.70980834961,y=-1223.0601806641,z=29.540618896484,
        radius=3, height=1,
        recipes = {
            ["Vendendo Maconha..."] = {
                description="",
                in_money=0, --RETIRA DINHEIRO
                out_money=0, -- DÁ DINHEIRO
                reagents={
                    ["Maconha"] = 1
                },
                products={
                    ["Dinheiro Sujo"] = 200
                },
                aptitudes={}
            }
        }
    },

    {
        name="Campo de Cocaina",
        permissions = {"colher.cocaina"},
        r=0,g=200,b=0,
        max_units=400,
        units_per_minute=1,
        x=1998.6602783203,y=4839.1049804688,z=43.548267364502,
        radius=3.5, height=1,
        recipes = {
            ["Colhendo Cocaína..."] = {
                description="",
                in_money=0,
                out_money=0,
                reagents={},
                products={
                    ["Folha de Coca"] = 1
                },
                aptitudes={
                    ["Laboratório.Cocaína"] = 1,
                    ["Ciência.Química"] = 1
                }
            }
        }
    },

    {
        name="Processamento de Cocaína",
        permissions = {"processar.cocaina"},
        r=0,g=200,b=0,
        max_units=400,
        units_per_minute=1,
        x=1099.9050292969,y=-3194.1301269531,z=-38.993461608887,
        radius=2, height=1,
        recipes = {
            ["Processando Cocaína..."] = {
                description="",
                in_money=0,
                out_money=0,
                reagents={
                    ["Folha de Coca"] = 1
                },
                products={
                    ["Cocaína"] = 1
                },
                aptitudes={
                    ["Laboratório.Cocaína"] = 1,
                    ["Ciência.Química"] = 1
                }
            }
        }
    },

    {
        name="Biqueira (Cocaína)",
        permissions = {"vender.cocaina"},
        r=0,g=255,b=0,
        max_units=300,
        units_per_minute=1,
        x=-1809.9006347656,y=-126.82835388184,z=78.786514282227,
        radius=3, height=1.5,
        recipes = {
            ["Vendendo Cocaína..."] = {
                description="",
                in_money=0, --RETIRA DINHEIRO
                out_money=0, -- DÁ DINHEIRO
                reagents={
                    ["Cocaína"] = 1
                },
                products={
                    ["Dinheiro Sujo"] = 250
                },
                aptitudes={}
            }
        }
    },

    {
        name="Coleta de Arma Enferrujada",
        permissions = {"processar.arma"},
        r=0,g=200,b=0,
        max_units=3,
        units_per_minute=1,
        x=138.78096008301,y=-3110.8371582031,z=5.8963084220886,
        radius=2, height=1.5,
        recipes = {
            ["Processando Pistola..."] = {
                description="",
                in_money=0,
                out_money=0,
                reagents={},
                products={
                    ["Pistola Enferrujada"] = 1
                }
            },
            ["Processando SMG..."] = {
                description="",
                in_money=0,
                out_money=0,
                reagents={},
                products={
                    ["SMG Enferrujada"] = 1
                }
            }
        }
    },

    {
        name="Processar Arma Enferrujada",
        permissions = {"aprimorar.arma"},
        r=0,g=200,b=0,
        max_units=3,
        units_per_minute=1,
        x=-52.597068786621,y=-2523.1508789063,z=7.4011702537537,
        radius=1, height=1.5,
        recipes = {
            ["Processando Pistola..."] = {
                description="",
                in_money=10000,
                out_money=0,
                reagents={
                    ["Pistola Enferrujada"] = 1
                },
                products={
                    ["wbody|WEAPON_PISTOL"] = 1
                }
            },
            ["Processando SMG..."] = {
                description="",
                in_money=20000,
                out_money=0,
                reagents={
                    ["SMG Enferrujada"] = 1
                },
                products={
                    ["wbody|WEAPON_SMG"] = 1
                }
            }
        }
    },

    {
        name="Procurar Cobre",
        permissions = {"pegar.cobre"},
        r=0,g=200,b=0,
        max_units=125,
        units_per_minute=1,
        x=-494.58218383789,y=-1754.4567871094,z=18.318357467651,
        radius=2, height=1.5,
        recipes = {
            ["Procurando Cobre..."] = {
                description="",
                in_money=0, --RETIRA DINHEIRO
                out_money=0, -- DÁ DINHEIRO
                reagents={},
                products={
                    ["Cobre"] = 1
                },
                aptitudes={}
            }
        }
    },

    {
        name="Fundir Aço",
        permissions = {"fundir.aço"},
        r=0,g=200,b=0,
        max_units=125,
        units_per_minute=1,
        x=1114.9576416016,y=-2003.8955078125,z=35.439395904541,
        radius=2, height=1,
        recipes = {
            ["Fundindo Aço..."] = {
                description="",
                in_money=0,
                out_money=0,
                reagents={},
                products={
                    ["Aço"] = 1
                },
                aptitudes={}
            }
        }
    },

    {
        name="Produzir Munição",
        permissions = {"produzir.balas"},
        r=0,g=200,b=0,
        max_units=100,
        units_per_minute=1,
        x=2330.2780761719,y=2572.0107421875,z=46.679504394531,
        radius=3, height=1,
        recipes = {
            ["Produzindo Munição de Pistola..."] = {
                description="",
                in_money=50,
                out_money=0,
                reagents={
                    ["Aço"] = 1,
                    ["Cobre"] = 1
                },
                products={
                    ["wammo|WEAPON_PISTOL"] = 2
                }
            },
            ["Produzindo Munição de SMG..."] = {
                description="",
                in_money=100,
                out_money=0,
                reagents={
                    ["Aço"] = 1,
                    ["Cobre"] = 1
                },
                products={
                    ["wammo|WEAPON_SMG"] = 2
                }
            }
        }
	},
	{
        name="Apanhar Tartaruga",
        permissions = {"apanhar.tartaruga"},
        r=0,g=200,b=0,
        max_units=400,
        units_per_minute=1,
        x = -2118.3271484375,y = -1218.7764892578,z = -141.22438049316,
        radius=3, height=1,
        recipes = {
            ["Coletando Tartaruga"] = {
                description="",
                in_money=50,
                out_money=0,
                reagents={
                },
                products={
                    ["tartaruga"] = 1
                }
            }
        }
	},
	{
        name="Vender Tartaruga",
        permissions = {"vender.tartaruga"},
        r=0,g=200,b=0,
        max_units=400,
        units_per_minute=1,
        x = -1816.2939453125, y = -1192.9519042969,	z = 14.305061340332,
        radius=3, height=1,
        recipes = {
            ["Vendendo Tartaruga"] = {
                description="",
                in_money=50,
                out_money=0,
                reagents={
					["tartaruga"] = 1
                },
                products={
                    ["Dinheiro Sujo"] = 100
                }
            }
        }
    },
}

return cfg
