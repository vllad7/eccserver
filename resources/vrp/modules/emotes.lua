local cfg = module("cfg/emotes")
local emotes = cfg.emotes

local function ch_emote(player,choice)
    local emote = emotes[choice]
    if emote then
        vRPclient._playAnim(player,emote[1],emote[2],emote[3])
    end
end

vRP.registerMenuBuilder("main", function(add, data)
    local choices = {}
    choices["Animações"] = {function(player, choice)
        local menu = {name="Animações",css={header_color="rgba(255,128,0,0.99)"}}
        local user_id = vRP.getUserId(player)
        if user_id then
            for k,v in pairs(emotes) do
                if vRP.hasPermissions(user_id, v.permissions or {}) then
                    menu[k] = {ch_emote}
                end
            end
        end
        menu["> Parar Animação"] = {function(player,choice)
            vRPclient._stopAnim(player,true)
            vRPclient._stopAnim(player,false)
        end}
        vRP.openMenu(player,menu)
    end}
    add(choices)
end)