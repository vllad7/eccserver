ch_player_menu = {function(player,choice)
  	Citizen.CreateThread(function()
		local user_id = vRP.getUserId(player)
		local menu = {}
		menu.name = "Menu Pessoal"
		menu.css = {header_color = "rgba(255,128,0,0.99)"}
    	menu.onclose = function(player) vRP.openMainMenu(player) end
	
    	if vRP.hasPermission(user_id,"Guardar.Armas") then
      		menu["Guardar Armas"] = choice_store_weapons
    	end

    	if vRP.hasPermission(user_id,"Revistar.Jogador") then
     		menu["Revistar"] = choice_player_check
    	end

	  	vRP.openMenu(player, menu)
    end)
end}