-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- ATIVAR/DESATIVAR MISSÕES DO VRP_BASIC_MISSION
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
local revive_seq = {
    {"amb@medic@standing@kneel@enter","enter",1},
    {"amb@medic@standing@kneel@idle_a","idle_a",1},
    {"amb@medic@standing@kneel@exit","exit",1}
}

choice_loot = {function(player,choice)
    local user_id = vRP.getUserId(player)
    if user_id ~= nil then
        local nplayer = vRPclient.getNearestPlayer(player,10)
        local nuser_id = vRP.getUserId(nplayer)
        if nuser_id ~= nil then
            local in_coma = vRPclient.isInComa(nplayer)
            if in_coma then                
                vRPclient._playAnim(player,false,revive_seq,false) -- anim
                Citizen.Wait(15000)
                vRPclient._stopAnim(player,true)
                vRPclient._stopAnim(player,false)
                local ndata = vRP.getUserDataTable(nuser_id)
                if ndata ~= nil then
                    if ndata.inventory ~= nil then
                        vRP.clearInventory(nuser_id)
                        for k,v in pairs(ndata.inventory) do
                            vRP.giveInventoryItem(user_id,k,v.amount,true)
                        end
                    end
                end
                local nmoney = vRP.getMoney(nuser_id)
                if vRP.tryPayment(nuser_id,nmoney) then
                    vRP.giveMoney(user_id,nmoney)
                end
                vRPclient.stopAnim(player,false)
            else
                TriggerClientEvent("pNotify:SendNotification", player, {
                    text = "Jogador não está em coma",
                    type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end
        else
            TriggerClientEvent("pNotify:SendNotification", player, {
				text = "Nenhum jogador próximo",
				type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
				animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
			})
        end
    end
end}

ch_hack = {function(player,choice)
    local user_id = vRP.getUserId(player)
    if user_id ~= nil then
        local nplayer = vRPclient.getNearestPlayer(player,25)
        if nplayer ~= nil then
            local nuser_id = vRP.getUserId(nplayer)
            if nuser_id ~= nil then
                local nbank = vRP.getBankMoney(nuser_id)
                local amount = math.floor(nbank*0.01)
                local nvalue = nbank - amount
                if math.random(1,100) == 7 then
                    vRP.setBankMoney(nuser_id,nvalue)
                    TriggerClientEvent("pNotify:SendNotification", nplayer, {
                        text = "Alguem hackeou " .. amount .. " reais da sua conta bancária",
                        type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                        animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                    })
                    vRP.giveInventoryItem(user_id,"Dinheiro Sujo",amount,true)
                else
                    TriggerClientEvent("pNotify:SendNotification", nplayer, {
                        text = "Estão tentando te hackear",
                        type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                        animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                    })
                    TriggerClientEvent("pNotify:SendNotification", player, {
                        text = "Você falhou na tentativa",
                        type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                        animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                    })
                    Citizen.Wait(60000)
                end
            else
                TriggerClientEvent("pNotify:SendNotification", player, {
                    text = "Nenhum jogador próximo",
                    type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end
        else
            TriggerClientEvent("pNotify:SendNotification", player, {
                text = "Nenhum jogador próximo",
                type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
            })
        end
    end
end}

ch_mug = {function(player,choice)
    local user_id = vRP.getUserId(player)
    if user_id ~= nil then
        local nplayer = vRPclient.getNearestPlayer(player,10)
        if nplayer ~= nil then
            local nuser_id = vRP.getUserId(nplayer)
            if nuser_id ~= nil then
                local nmoney = vRP.getMoney(nuser_id)
                local amount = nmoney
                if math.random(1,100) == 7 then
                    if vRP.tryPayment(nuser_id,amount) then
                        TriggerClientEvent("pNotify:SendNotification", nplayer, {
                            text = "Você roubou " .. amount .. " reais",
                            type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                            animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                        })
                        vRP.giveInventoryItem(user_id,"Dinheiro Sujo",amount,true)
                    else
                        TriggerClientEvent("pNotify:SendNotification", player, {
                            text = "Este jogador não possui dinheiro",
                            type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                            animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                        })
                    end
                else
                    TriggerClientEvent("pNotify:SendNotification", nplayer, {
                        text = "Estão tentando roubar sua carteira",
                        type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                        animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                    })
                    TriggerClientEvent("pNotify:SendNotification", player, {
                        text = "Você falhou na tentativa",
                        type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                        animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                    })
                    Citizen.Wait(60000)
                end
            else
                TriggerClientEvent("pNotify:SendNotification", player, {
                    text = "Nenhum jogador próximo",
                    type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end
        else
            TriggerClientEvent("pNotify:SendNotification", player, {
                text = "Nenhum jogador próximo",
                type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
            })
        end
    end
end}

ch_lockpickveh = {function(player,choice)
    BMclient.lockpickVehicle(player,20,true) -- 20s é o tempo
end}
