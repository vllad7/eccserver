spikes = {}
ch_spikes = {function(player,choice)
	local user_id = vRP.getUserId(player)
	local closeby = BMclient.isCloseToSpikes(player)
	if closeby and (spikes[player] or vRP.hasPermission(user_id,"pregos.admin")) then
	  	BMclient.removeSpikes(player)
	 	spikes[player] = false
	elseif closeby and not spikes[player] and not vRP.hasPermission(user_id,"pregos.admin") then
		TriggerClientEvent("pNotify:SendNotification", player, {
			text = "Você não pode carregar mais tapetes de pregos",
			type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
			animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
		})
	elseif not closeby and spikes[player] and not vRP.hasPermission(user_id,"pregos.admin") then
		TriggerClientEvent("pNotify:SendNotification", player, {
			text = "Você não colocar mais tapetes de pregos",
			type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
			animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
		})
	elseif not closeby and (not spikes[player] or vRP.hasPermission(user_id,"pregos.admin")) then
	  	BMclient.setSpikesOnGround(player)
	  	spikes[player] = true
	end
end}

ch_drag = {function(player,choice)
  	local nplayer = vRPclient.getNearestPlayer(player,5)
  	if nplayer then
    	local handcuffed = vRPclient.isHandcuffed(nplayer)
    	if handcuffed then
    		BMclient.policeDrag(nplayer, player)
		else
			TriggerClientEvent("pNotify:SendNotification", player, {
				text = "Jogador não está algemado",
				type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
				animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
			})
    	end
  	else
		TriggerClientEvent("pNotify:SendNotification", player, {
			text = "Nenhum jogador próximo de você",
			type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
			animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
		})
  	end
end}

coordenadas = {
	[1] = {["X"] = 460.20, ["Y"] = -997.76, ["Z"] = 24.91},
	[2] = {["X"] = 460.03, ["Y"] = -994.12, ["Z"] = 24.91},
	[3] = {["X"] = 459.90, ["Y"] = -1001.12, ["Z"] = 24.91},
  }

ch_jail = {function(player,choice) 
  	local nplayers = vRPclient.getNearestPlayers(player,15) 
	local user_list = ""
  	for k,v in pairs(nplayers) do
	  user_list = user_list .. "Jogadores próximos " .. vRP.getUserId(k) .. " " .. GetPlayerName(k) .. " | "
  	end 
	if user_list ~= "" then
	  local target_id = vRP.prompt(player,"Jogadores próximos " .. user_list .. " | ","") 
	  if target_id ~= nil and target_id ~= "" then 
	    local jail_time = vRP.prompt(player,"Qual o tempo da sentença ?","")
			if jail_time ~= nil and jail_time ~= "" then 
	      		local target = vRP.getUserSource(tonumber(target_id))
			  	if target ~= nil then
		      		if tonumber(jail_time) > 30 then
  			    		jail_time = 30
		      		end
		      		if tonumber(jail_time) < 1 then
		        		jail_time = 1
		      		end
		      		local handcuffed = vRPclient.isHandcuffed(target)  
          			if handcuffed then 
						BMclient.loadFreeze(target,false,true,true)
						SetTimeout(5000,function()
					  		BMclient.loadFreeze(target,false,false,false)
						end)
						local escolha = math.random(1,#coordenadas)
	  					vRPclient.teleport(target, coordenadas[escolha]["X"],coordenadas[escolha]["Y"],coordenadas[escolha]["Z"])
						vRPclient._setHandcuffed(target,false)
						TriggerClientEvent("pNotify:SendNotification", target, {
							text = "Você foi enviado para a cadeia",
							type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
							animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
						})
						TriggerClientEvent("pNotify:SendNotification", player, {
							text = "Jogador enviado para a cadeia",
							type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
							animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
						})
						vRP.setHunger(tonumber(target_id),0)
						vRP.setThirst(tonumber(target_id),0)
						jail_clock(tonumber(target_id),tonumber(jail_time))
						local user_id = vRP.getUserId(player)
					else
						TriggerClientEvent("pNotify:SendNotification", player, {
							text = "Jogador não está algemado",
							type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
							animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
						})
			    	end
				  else
					TriggerClientEvent("pNotify:SendNotification", player, {
						text = "Ação Inválida",
						type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
						animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
					})
			  	end
			else
				TriggerClientEvent("pNotify:SendNotification", player, {
					text = "Ação Inválida",
					type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
					animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
				})
			end
    	else
			TriggerClientEvent("pNotify:SendNotification", player, {
				text = "Ação Inválida",
				type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
				animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
			})
    	end 
	  else
		TriggerClientEvent("pNotify:SendNotification", player, {
			text = "Nenhum jogador próximo de você",
			type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
			animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
		})
  	end 
end}

ch_unjail = {function(player,choice) 
	local target_id = vRP.prompt(player,"ID do usuário","") 
	if target_id ~= nil and target_id ~= "" then 
	local value = vRP.getUData(tonumber(target_id),"vRP:jail:time")
		if value ~= nil then
		  	custom = json.decode(value)
			if custom ~= nil then
			  	local user_id = vRP.getUserId(player)
			  	if tonumber(custom) > 0 or vRP.hasPermission(user_id,"soltar.admin") then
	        		local target = vRP.getUserSource(tonumber(target_id))
					if target ~= nil then
	          			unjailed[target] = tonumber(target_id)
						TriggerClientEvent("pNotify:SendNotification", player, {
							text = "Você libertou o jogador",
							type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
							animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
						})

						TriggerClientEvent("pNotify:SendNotification", target, {
							text = "Sua sentença foi reduzida",
							type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
							animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
						})
					else
						TriggerClientEvent("pNotify:SendNotification", player, {
							text = "Ação Inválida",
							type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
							animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
						})
					end
			  	else
					TriggerClientEvent("pNotify:SendNotification", player, {
						text = "Ação Inválida",
						type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
						animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
					})
			  	end
			end
		end
  	else
    	TriggerClientEvent("pNotify:SendNotification", player, {
			text = "Ação Inválida",
			type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
			animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
		})
  	end 
end}

ch_handcuff = {function(player,choice)
  	local nplayer = vRPclient.getNearestPlayer(player,10)
  	local nuser_id = vRP.getUserId(nplayer)
  	if nuser_id ~= nil then
    	vRPclient.toggleHandcuff(nplayer)
	  	local user_id = vRP.getUserId(player)
    	vRP.closeMenu(nplayer)
  	else
    	TriggerClientEvent("pNotify:SendNotification", player, {
			text = "Nenhum jogador próximo de você",
			type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
			animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
		})
  	end
end}

ch_handcuff2 = {function(player,choice)
	local nplayer = vRPclient.getNearestPlayer(player,10)
	local nuser_id = vRP.getUserId(nplayer)
	local user_id = vRP.getUserId(player)
	if nuser_id ~= nil then
		vRPclient.toggleHandcuff(nplayer)
		vRP.tryGetInventoryItem(user_id,"kit de algemas",1,true)
		local user_id = vRP.getUserId(player)
		vRP.closeMenu(nplayer)
	else
		TriggerClientEvent("pNotify:SendNotification", player, {
			text = "Nenhum jogador próximo de você",
			type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
			animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
		})
	end
end}

ch_freeze = {function(player,choice) 
	local user_id = vRP.getUserId(player)
	if vRP.hasPermission(user_id,"congelar.admin") then
	  	local target_id = vRP.prompt(player,"User ID:","") 
	  	if target_id ~= nil and target_id ~= "" then 
	    	local target = vRP.getUserSource(tonumber(target_id))
		  	if target ~= nil then
		    	TriggerClientEvent("pNotify:SendNotification", player, {
					text = "Jogador congelado",
					type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
					animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
				})
		    	BMclient.loadFreeze(target,true,true,true)
		  	else
				TriggerClientEvent("pNotify:SendNotification", player, {
					text = "Ação Inválida",
					type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
					animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
				})
		  	end
    	else
			TriggerClientEvent("pNotify:SendNotification", player, {
				text = "Ação Inválida",
				type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
				animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
			})
    	end 
	else
	  	local nplayer = vRPclient.getNearestPlayer(player,10)
    	local nuser_id = vRP.getUserId(nplayer)
    	if nuser_id ~= nil then
			TriggerClientEvent("pNotify:SendNotification", player, {
				text = "Jogador descongelado",
				type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
				animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
			})
		  	BMclient.loadFreeze(nplayer,true,false,false)
    	else
			TriggerClientEvent("pNotify:SendNotification", player, {
				text = "Nenhum jogador próximo de você",
				type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
				animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
			})
    	end
	end
end}