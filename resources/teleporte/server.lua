local interiores = {
	[1] = {
		["x_fora"] = 251.61322021484, 
		["y_fora"] = -1366.37890625, 
		["z_fora"] = 39.534332275391, 
		-------------------------------
		["x_dentro"] = -449.92349243164, 
		["y_dentro"] = -340.94372558594, 
		["z_dentro"] = 34.501441955566, 
		["nome"] = 'HOSPITAL DE LOS SANTOS'
	},
	[2] = { 
		["x_fora"] = 339.46356201172, 
		["y_fora"] = -584.02459716797, 
		["z_fora"] = 74.165641784668, 
		-------------------------------
		["x_dentro"] = 249.12945556641, 
		["y_dentro"] = -1369.2115478516, 
		["z_dentro"] = 29.64799118042,
		["nome"] = 'HELIPONTO HOSPITAL'
	},
	[3] = { 
		["x_fora"] = 3574.1083984375, 
		["y_fora"] = 3736.1064453125, 
		["z_fora"] = 36.642627716064, 
		-------------------------------
		["x_dentro"] = 1088.6943359375,
		["y_dentro"] = -3188.2309570313, 
		["z_dentro"] = -38.993461608887,
		["nome"] = 'LABORATÓRIO DE COCAÍNA'
	},
	[4] = { 
		["x_fora"] = 3526.384765625,  
		["y_fora"] = 3736.9885253906, 
		["z_fora"] = 36.715198516846, 
		-------------------------------
		["x_dentro"] = 1065.7137451172, 
		["y_dentro"] = -3183.564453125, 
		["z_dentro"] = -39.163570404053,
		["nome"] = 'LABORATÓRIO DE MACONHA'
	},
	[5] = { 
		["x_fora"] = 1138.1086425781, 
		["y_fora"] = -3198.7485351563, 
		["z_fora"] = -39.665725708008, 
		-------------------------------
		["x_dentro"] = -559.16900634766, 
		["y_dentro"] = -1803.6354980469, 
		["z_dentro"] = 22.610904693604,
		["nome"] = 'LAVAGEM DE DINHEIRO'
	},
}

RegisterServerEvent("interiores:enviar_dados")
AddEventHandler("interiores:enviar_dados", function()
    TriggerClientEvent("interiores:receber_dados", source, interiores)
end)