local cfg = module("cfg/player_state")
local lang = vRP.lang

AddEventHandler("vRP:playerSpawn", function(user_id, source, first_spawn)
    local player = source
    local data = vRP.getUserDataTable(user_id)
    local tmpdata = vRP.getUserTmpTable(user_id)
    if first_spawn then
        if data.customization == nil then
            data.customization = cfg.default_customization
        end
        if not data.position and cfg.spawn_enabled then
            local x = 237.96
            local y = -407.04
            local z =  47.92
            data.position = {x=x,y=y,z=z}
            vRPclient._teleport(source,x,y,z)
        end
        if data.position then
            vRPclient.teleport(source,data.position.x,data.position.y,data.position.z)
        end
        if data.customization then
            vRPclient.setCustomization(source,data.customization)
            if data.weapons then
                vRPclient.giveWeapons(source,data.weapons,true)

                if data.health then
                    vRPclient.setHealth(source,data.health)
                    SetTimeout(5000, function()
                        if vRPclient.isInComa(player) then
                            vRPclient.killComa(player)
                        end
                    end)
                end
            end
        else
            if data.weapons then
                vRPclient.giveWeapons(source,data.weapons,true)
            end
            if data.health then
                vRPclient.setHealth(source,data.health)
            end
        end
    else
        vRP.setHunger(user_id,0)
        vRP.setThirst(user_id,0)
        vRP.setSono(user_id,0)
        vRP.setNecessidades(user_id,0)
        vRP.clearInventory(user_id)
        if cfg.clear_phone_directory_on_death then
            data.phone_directory = {}
        end
        if cfg.lose_aptitudes_on_death then
            data.gaptitudes = {}
        end
        vRP.setMoney(user_id,0)
        vRPclient._setHandcuffed(player,false)
        if cfg.spawn_enabled then
            local x = 237.96
            local y = -407.04
            local z =  47.92
            data.position = {x=x,y=y,z=z}
            vRPclient._teleport(source,x,y,z)
        end
        if data.customization then
            vRPclient._setCustomization(source,data.customization)
        end
    end
    vRPclient._playerStateReady(source, true)
end)

function tvRP.updatePos(x,y,z)
    local user_id = vRP.getUserId(source)
    if user_id then
        local data = vRP.getUserDataTable(user_id)
        local tmp = vRP.getUserTmpTable(user_id)
        if data and (not tmp or not tmp.home_stype) then -- don't save position if inside home slot
            data.position = {x = tonumber(x), y = tonumber(y), z = tonumber(z)}
        end
    end
end

function tvRP.updateWeapons(weapons)
    local user_id = vRP.getUserId(source)
    if user_id then
        local data = vRP.getUserDataTable(user_id)
        if data then
            data.weapons = weapons
        end
    end
end

function tvRP.updateCustomization(customization)
    local user_id = vRP.getUserId(source)
    if user_id then
        local data = vRP.getUserDataTable(user_id)
        if data then
            data.customization = customization
        end
    end
end

function tvRP.updateHealth(health)
    local user_id = vRP.getUserId(source)
    if user_id then
        local data = vRP.getUserDataTable(user_id)
        if data then
            data.health = health
        end
    end
end