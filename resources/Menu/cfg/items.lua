Citizen.CreateThread(function()
   	vRP.defInventoryItem("lockpick","Kit de Arrombamento","",
  		function(args)
    		local choices = {}
      		choices["Lockpick"] = {function(player,choice)
      			local user_id = vRP.getUserId(player)
      			if user_id ~= nil then
        			if vRP.tryGetInventoryItem(user_id, "lockpick", 1, true) then
      					BMclient.lockpickVehicle(player,20,true) -- 20S para destravar o carro
          				vRP.closeMenu(player)
       	 			end
      			end
    		end}  
    		return choices
  		end,
  	0.75)
  
  	vRP.defInventoryItem("scuba","Traje de Mergulho","",
  		function(args)
    		local choices = {}
      		choices["Equipar/Desequipar"] = ch_scuba
      		return choices
  		end,
  	10.00)
 
  	vRP.defInventoryItem("kit de algemas","Algemas","",
  		function(args)
    		local choices = {}
      		choices["Algemar"] = ch_handcuff2
    		return choices
  		end,
  	0.75)
end)