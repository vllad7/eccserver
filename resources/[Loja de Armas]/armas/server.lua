-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- VARIAVEIS LOCAIS DE CONEXÃO COM O VRP
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- EVENTOS SE COMUNICANDO COM O CLIENT
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
RegisterServerEvent("zezao:um")
AddEventHandler("zezao:um", function(item)
    local source = source
    local user_id = vRP.getUserId(source)
    local porte = json.decode(vRP.getSData("zezao:porte"))
    if porte >= 1 then
        if item.name == "arma-canivete" then
            if vRP.tryPayment(user_id, zezao.canivete) then
                vRP.giveInventoryItem(user_id, "wbody|WEAPON_SWITCHBLADE", 1, false)
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Você comprou um canivete e pagou " .. zezao.canivete .. " reais por esta arma",
                    type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            else
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Dinheiro Insuficiente",
                    type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end
        end

        if item.name == "arma-facao" then
            if vRP.tryPayment(user_id, zezao.facao) then
                vRP.giveInventoryItem(user_id, "wbody|WEAPON_MACHETE", 1, false)
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Você comprou um facão e pagou " .. zezao.facao .. " reais por esta arma",
                    type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            else
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Dinheiro Insuficiente",
                    type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end
        end

        if item.name == "arma-martelo" then
            if vRP.tryPayment(user_id, zezao.martelo) then
                vRP.giveInventoryItem(user_id, "wbody|WEAPON_HAMMER", 1, false)
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Você comprou um martelo e pagou " .. zezao.martelo .. " reais por esta arma",
                    type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            else
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Dinheiro Insuficiente",
                    type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end
        end

        if item.name == "arma-pe-de-cabra" then
            if vRP.tryPayment(user_id, zezao.pedecabra) then
                vRP.giveInventoryItem(user_id, "wbody|WEAPON_CROWBAR", 1, false)
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Você comprou um pé de cabra e pagou " .. zezao.pedecabra .. " reais por esta arma",
                    type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            else
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Dinheiro Insuficiente",
                    type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end
        end

        if item.name == "arma-soco-ingles" then
            if vRP.tryPayment(user_id, zezao.socoingles) then
                vRP.giveInventoryItem(user_id, "wbody|WEAPON_KNUCKLE", 1, false)
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Você comprou um soco inglês e pagou " .. zezao.socoingles .. " reais por esta arma",
                    type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            else
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Dinheiro Insuficiente",
                    type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end
        end

        if item.name == "arma-taco-de-baseball" then
            if vRP.tryPayment(user_id, zezao.tacodebaseball) then
                vRP.giveInventoryItem(user_id, "wbody|WEAPON_BAT", 1, false)
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Você comprou um taco de baseball e pagou " .. zezao.tacodebaseball .. " reais por esta arma",
                    type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            else
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Dinheiro Insuficiente",
                    type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end
        end
    else
        TriggerClientEvent("pNotify:SendNotification", source, {
            text = "Seu porte atual é de nível " .. porte .. " e não permite acesso a esta area",
            type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
            animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
        })
    end
end)

RegisterServerEvent("zezao:dois")
AddEventHandler("zezao:dois", function(item)
    local source = source
    local user_id = vRP.getUserId(source)
    local porte = json.decode(vRP.getSData("zezao:porte"))
    if porte >= 2 then
        if item.name == "arma-pistola" then
            if vRP.tryPayment(user_id, zezao.pistola) then
                vRP.giveInventoryItem(user_id, "wbody|WEAPON_PISTOL", 1, false)
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Você comprou uma pistola e pagou " .. zezao.pistola .. " reais por esta arma",
                    type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            else
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Dinheiro Insuficiente",
                    type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end
        end

        if item.name == "arma-pistola-sns" then
            if vRP.tryPayment(user_id, zezao.pistolasns) then
                vRP.giveInventoryItem(user_id, "wbody|WEAPON_SNSPISTOL", 1, false)
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Você comprou uma pistola SNS e pagou " .. zezao.pistolasns .. " reais por esta arma",
                    type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            else
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Dinheiro Insuficiente",
                    type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end
        end
    else
        TriggerClientEvent("pNotify:SendNotification", source, {
            text = "Seu porte atual é de nível " .. porte .. " e não permite acesso a esta area",
            type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
            animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
        })
    end
end)

RegisterServerEvent("zezao:tres")
AddEventHandler("zezao:tres", function(item)
    local source = source
    local user_id = vRP.getUserId(source)
    local porte = json.decode(vRP.getSData("zezao:porte"))
    if porte >= 3 then
        if item.name == "arma-12" then
            if vRP.tryPayment(user_id, zezao.doze) then
                vRP.giveInventoryItem(user_id, "wbody|WEAPON_PUMPSHOTGUN", 1, false)
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Você comprou uma 12 e pagou " .. zezao.doze .. " reais por esta arma",
                    type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            else
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Dinheiro Insuficiente",
                    type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end
        end

        if item.name == "arma-bomba" then
            if vRP.tryPayment(user_id, zezao.bomba) then
                vRP.giveInventoryItem(user_id, "wbody|WEAPON_STICKYBOMB", 1, false)
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Você comprou uma bomba caseira e pagou " .. zezao.bomba .. " reais por esta arma",
                    type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            else
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Dinheiro Insuficiente",
                    type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end
        end

        if item.name == "arma-rifle-compacto" then
            if vRP.tryPayment(user_id, zezao.riflecompacto) then
                vRP.giveInventoryItem(user_id, "wbody|WEAPON_COMPACTRIFLE", 1, false)
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Você comprou um rifle compacto e pagou " .. zezao.riflecompacto .. " reais por esta arma",
                    type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            else
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Dinheiro Insuficiente",
                    type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end
        end

        if item.name == "arma-smg" then
            if vRP.tryPayment(user_id, zezao.smg) then
                vRP.giveInventoryItem(user_id, "wbody|WEAPON_SMG", 1, false)
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Você comprou uma SMG e pagou " .. zezao.smg .. " reais por esta arma",
                    type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            else
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Dinheiro Insuficiente",
                    type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end
        end
    else
        TriggerClientEvent("pNotify:SendNotification", source, {
            text = "Seu porte atual é de nível " .. porte .. " e não permite acesso a esta area",
            type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
            animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
        })
    end
end)

RegisterServerEvent("zezao:quatro")
AddEventHandler("zezao:quatro", function(item)
    local source = source
    local user_id = vRP.getUserId(source)
    local porte = json.decode(vRP.getSData("zezao:porte"))
    if porte >= 4 then
        if item.name == "arma-ak-47" then
            if vRP.tryPayment(user_id, zezao.ak47) then
                vRP.giveInventoryItem(user_id, "wbody|WEAPON_ASSAULTRIFLE", 1, false)
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Você comprou uma AK-47 e pagou " .. zezao.ak47 .. " reais por esta arma",
                    type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            else
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Dinheiro Insuficiente",
                    type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end
        end

        if item.name == "arma-m4" then
            if vRP.tryPayment(user_id, zezao.m4) then
                vRP.giveInventoryItem(user_id, "wbody|WEAPON_CARBINERIFLE", 1, false)
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Você comprou uma M4 e pagou " .. zezao.m4 .. " reais por esta arma",
                    type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            else
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Dinheiro Insuficiente",
                    type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end
        end

        if item.name == "colete-balas" then
            if vRP.tryPayment(user_id, zezao.colete) then
                TriggerClientEvent('Zezao:ColeteZ', source)
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Você comprou um colete a prova de balas e pagou " .. zezao.colete .. " reais por esta arma",
                    type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            else
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Dinheiro Insuficiente",
                    type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end
        end

        if item.name == "arma-sniper" then
            if vRP.tryPayment(user_id, zezao.sniper) then
                vRP.giveInventoryItem(user_id, "wbody|WEAPON_HEAVYSNIPER", 1, false)
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Você comprou uma sniper e pagou " .. zezao.sniper .. " reais por esta arma",
                    type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            else
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Dinheiro Insuficiente",
                    type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end
        end
    else
        TriggerClientEvent("pNotify:SendNotification", source, {
            text = "Seu porte atual é de nível " .. porte .. " e não permite acesso a esta area",
            type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
            animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
        })
    end
end)