-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- VARIAVEIS LOCAIS
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
local options = {
    x = 0.8, 
    y = 0.2, 
    width = 0.2, 
    height = 0.04, 
    scale = 0.4, 
    font = 0, 
    color_r = 255, 
    color_g = 124, 
    color_b = 173, 
}

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- MENU
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function DisplayHelpText(str)
    SetTextComponentFormat("STRING")
    AddTextComponentString(str)
    DisplayHelpTextFromStringLabel(0, 0, 1, -1)
end

function Main()
    ClearMenu()
    Menu.addButton("Porte Nível 1", "comprarporte", {name = "porte-basico"})
    Menu.addButton("Porte Nível 2", "comprarporte", {name = "porte-intermediario"})
    Menu.addButton("Porte Nível 3", "comprarporte", {name = "porte-avancado"})
    Menu.addButton("Porte Nível 4", "comprarporte", {name = "porte-especial"})
    Menu.addButton("Qual meu porte?", "checarporte", {name = "meu-porte"})
end

function comprarporte(item)
    TriggerServerEvent("comprar:porte", item)
end

function checarporte(item)
    TriggerServerEvent("checar:porte", item)
end
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- LOCALIZAÇÃO NO MAPA
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
porte = {
    {nome = "Loja de Armas", cor = 1, id = 156, x = 819.67, y = -2155.48, z = 28.64}, 
    {nome = "Loja de Armas", cor = 1, id = 156, x = 12.42, y = -1105.45, z = 28.85}, 
    {nome = "Loja de Armas", cor = 1, id = 156, x = 840.93, y = -1031.16, z = 27.25}, 
}

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)
        local pos = GetEntityCoords(GetPlayerPed(-1), true)
        for k, v in ipairs(porte) do
            if(Vdist(pos.x, pos.y, pos.z, v.x, v.y, v.z) < 15.0)then
                DrawMarker(27, v.x, v.y, v.z, 0, 0, 0, 0, 0, 0, 0.9, 0.9, 0.6, 255, 124, 173, 255, 0, 0, 0, 0)
                if(Vdist(pos.x, pos.y, pos.z, v.x, v.y, v.z) < 1.2)then
                    DisplayHelpText("Pressione ~INPUT_CONTEXT~ para comprar porte de armas")
                    if IsControlJustReleased(1, 51) then
                        Main()
                        Menu.selection = 1
                        Menu.hidden = not Menu.hidden -- Hide/Show the menu
                    end
                    Menu.renderGUI(options) -- Draw menu on each tick if Menu.hidden = false
                elseif(Vdist(pos.x, pos.y, pos.z, v.x, v.y, v.z) > 1.0)then
                    Menu.hidden = true
                end
            end
        end
    end
end)
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- MENSAGENS
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function TextoDIVTitulo(x, y, width, height, scale, text, r, g, b, a, outline)
    SetTextFont(1)
    SetTextProportional(0)
    SetTextScale(scale, scale)
    SetTextColour(r, g, b, a)
    SetTextDropShadow(0, 0, 0, 0, 255)
    SetTextEdge(1, 0, 0, 0, 255)
    SetTextDropShadow()
    if(outline)then
        SetTextOutline()
    end
    SetTextEntry("STRING")
    AddTextComponentString(text)
    DrawText(x - width / 2, y - height / 2 + 0.005)
end

function TextoDIV(x, y, width, height, scale, text, r, g, b, a, outline)
    SetTextFont(4)
    SetTextProportional(0)
    SetTextScale(scale, scale)
    SetTextColour(r, g, b, a)
    SetTextDropShadow(0, 0, 0, 0, 255)
    SetTextEdge(1, 0, 0, 0, 255)
    SetTextDropShadow()
    if(outline)then
        SetTextOutline()
    end
    SetTextEntry("STRING")
    AddTextComponentString(text)
    DrawText(x - width / 2, y - height / 2 + 0.005)
end

function DrawR(xMin, xMax, yMin, yMax, color1, color2, color3, color4)
    DrawRect(xMin, yMin, xMax, yMax, color1, color2, color3, color4); 
end

Citizen.CreateThread(function()
    while true do
        if not Menu.hidden then
            TextoDIVTitulo(1.225, 0.752, 1.0, 1.0, 0.8, "Centro de Licenças", 255, 255, 255, 255, true)
            --[[
            if Menu.selection == 0 then
                DrawRect(0.80, 0.617, 0.3, 0.125, 0, 0, 0, 150)
                DrawRect(0.80, 0.530, 0.3, 0.05, 255, 124, 173, 150)
                TextoDIV(1.22, 1.025, 1.0, 1.05, 0.8, "ARMAS DE PORTE NÍVEL 1", 255, 255, 255, 255, true)
                TextoDIV(1.16, 1.050, 1.0, 1.0, 0.5, "Canivete, Facão, Machadinha, Martelo, Pé de Cabra, Soco Inglês e", 255, 255, 255, 255, true)
                TextoDIV(1.16, 1.080, 1.0, 1.0, 0.5, "Taco de Baseball.", 255, 255, 255, 255, true)
            end
            if Menu.selection == 1 then
                DrawRect(0.80, 0.617, 0.3, 0.125, 0, 0, 0, 150)
                DrawRect(0.80, 0.530, 0.3, 0.05, 255, 124, 173, 150)
                TextoDIV(1.22, 1.025, 1.0, 1.05, 0.8, "ARMAS DE PORTE NÍVEL 2", 255, 255, 255, 255, true)
                TextoDIV(1.16, 1.050, 1.0, 1.0, 0.5, "Canivete, Facão, Machadinha, Martelo, Pé de Cabra, Pistola, Pistola", 255, 255, 255, 255, true)
                TextoDIV(1.16, 1.080, 1.0, 1.0, 0.5, "SNS, Soco Inglês e Taco de Baseball.", 255, 255, 255, 255, true)
            end
            if Menu.selection == 2 then
                DrawRect(0.80, 0.617, 0.3, 0.125, 0, 0, 0, 150)
                DrawRect(0.80, 0.530, 0.3, 0.05, 255, 124, 173, 150)
                TextoDIV(1.22, 1.025, 1.0, 1.05, 0.8, "ARMAS DE PORTE NÍVEL 3", 255, 255, 255, 255, true)
                TextoDIV(1.16, 1.050, 1.0, 1.0, 0.5, "12, Bomba Adesiva Bérica, Canivete, Facão, Machadinha, Martelo,", 255, 255, 255, 255, true)
                TextoDIV(1.16, 1.080, 1.0, 1.0, 0.5, "Pé de Cabra, Pistola, Pistola SNS, Rifle Compacto, SMG,", 255, 255, 255, 255, true)
                TextoDIV(1.16, 1.110, 1.0, 1.0, 0.5, "Soco Inglês e Taco de Baseball.", 255, 255, 255, 255, true)
            end
            if Menu.selection == 3 then
                DrawRect(0.80, 0.617, 0.3, 0.125, 0, 0, 0, 150)
                DrawRect(0.80, 0.530, 0.3, 0.05, 255, 124, 173, 150)
                TextoDIV(1.22, 1.025, 1.0, 1.05, 0.8, "ARMAS DE PORTE NÍVEL 4", 255, 255, 255, 255, true)
                TextoDIV(1.16, 1.050, 1.0, 1.0, 0.5, "12, AK 47, Bomba Adesiva Bérica, Canivete, Facão, Machadinha,", 255, 255, 255, 255, true)
                TextoDIV(1.16, 1.080, 1.0, 1.0, 0.5, "Martelo, Pé de Cabra, Pistola, Pistola SNS, Rifle Compacto, M4", 255, 255, 255, 255, true)
                TextoDIV(1.16, 1.110, 1.0, 1.0, 0.5, "Soco Inglês, SMG e Taco de Baseball.", 255, 255, 255, 255, true)
            end
            ]]
        end
        Citizen.Wait(0)
    end
end)

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)
        if IsEntityDead(GetPlayerPed(-1)) then
            Menu.hidden = true
            Citizen.Wait(5000)
        end
    end
end)
