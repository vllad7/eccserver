-- ###################################
--
--       Credits: Shadow
--
-- ###################################
local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP")
JBserver = Tunnel.getInterface("empregos","empregos")

local menuEnabled = false

local Empregos = { 
	{nome = "Agencia de Empregos", id = 351, cor = 60, x = -1081.81, y = -248.03, z = 38.86},
} 

Citizen.CreateThread(function()
	for _, item in pairs(Empregos) do
      	item.blip = AddBlipForCoord(item.x, item.y, item.z)
      	SetBlipSprite(item.blip, item.id)
      	SetBlipColour(item.blip, item.cor)
      	SetBlipAsShortRange(item.blip, true)
      	BeginTextCommandSetBlipName("STRING")
      	AddTextComponentString(item.nome)
      	EndTextCommandSetBlipName(item.blip)
    end
end)

RegisterNetEvent("ToggleActionmenu")
AddEventHandler("ToggleActionmenu", function()
	ToggleActionMenu()
end)

function ToggleActionMenu()
	menuEnabled = not menuEnabled
	if ( menuEnabled ) then
		SetNuiFocus( true, true )
		SendNUIMessage({
			showPlayerMenu = true
		})
	else
		SetNuiFocus( false )
		SendNUIMessage({
			showPlayerMenu = false
		})
	end
end

function killTutorialMenu()
SetNuiFocus( false )
		SendNUIMessage({
			showPlayerMenu = false
		})
		menuEnabled = false

end


RegisterNUICallback('close', function(data, cb)
  ToggleActionMenu()
  cb('ok')
end)

Citizen.CreateThread(function ()
	while true do
		Citizen.Wait(0)
		DrawMarker(27, -1081.81, -248.03, 36.86, 0, 0, 0, 0, 0, 0, 1.0001,1.0001,1.0001, 255, 255, 0, 200, 0, 0, 0, 0, 0, 0, 0)
		if GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), -1081.81, -248.03, 37.76, true ) < 1 then
			DisplayHelpText("Pressione ~g~E~s~ para acessar a agencia de empregos")
		 if (IsControlJustReleased(1, 51)) then
			SetNuiFocus( true, true )
			SendNUIMessage({
				showPlayerMenu = true
			})
		 end
		end
	end
end)

RegisterNUICallback('caminhoneiro', function(data, cb)
	JBserver.jobCaminhoneiro()
  	cb('ok')
end)

RegisterNUICallback('mecanico', function(data, cb)
	JBserver.jobMecanico()
  	cb('ok')
end)

RegisterNUICallback('taxi', function(data, cb)
	JBserver.jobTaxi()
  	cb('ok')
end)

RegisterNUICallback('pescador', function(data, cb)
	JBserver.jobPescador()
  	cb('ok')
end)

RegisterNUICallback('entregador_comida', function(data, cb)
	JBserver.jobEntregadorComida()
  	cb('ok')
end)

RegisterNUICallback('desempregado', function(data, cb)
	JBserver.jobDesempregado()
  	cb('ok')
end)

RegisterNUICallback('closeButton', function(data, cb)
	killTutorialMenu()
  	cb('ok')
end)

function DrawSpecialText(m_text, showtime)
	SetTextEntry_2("STRING")
	AddTextComponentString(m_text)
	DrawSubtitleTimed(showtime, 1)
end

function DisplayHelpText(str)
	SetTextComponentFormat("STRING")
	AddTextComponentString(str)
	DisplayHelpTextFromStringLabel(0, 0, 1, -1)
end
