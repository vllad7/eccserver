-- define some basic inventory items

local items = {}

local function play_eat(player)
  local seq = {
    {"mp_player_inteat@burger", "mp_player_int_eat_burger_enter",1},
    {"mp_player_inteat@burger", "mp_player_int_eat_burger",1},
    {"mp_player_inteat@burger", "mp_player_int_eat_burger_fp",1},
    {"mp_player_inteat@burger", "mp_player_int_eat_exit_burger",1}
  }

  vRPclient._playAnim(player,true,seq,false)
end

local function play_drink(player)
  local seq = {
    {"mp_player_intdrink","intro_bottle",1},
    {"mp_player_intdrink","loop_bottle",1},
    {"mp_player_intdrink","outro_bottle",1}
  }

  vRPclient._playAnim(player,true,seq,false)
end

-- gen food choices as genfunc
-- idname
-- ftype: eat or drink
-- vary_hunger
-- vary_thirst
local function gen(ftype, vary_hunger, vary_thirst, vary_sono, vary_necessidades)
  local fgen = function(args)
    local idname = args[1]
    local choices = {}
    local act = "Unknown"
    if ftype == "eat" then act = "Eat" elseif ftype == "drink" then act = "Drink" end
    local name = vRP.getItemName(idname)

    choices[act] = {function(player,choice)
      local user_id = vRP.getUserId(player)
      if user_id ~= nil then
        if vRP.tryGetInventoryItem(user_id,idname,1,false) then
          if vary_hunger ~= 0 then vRP.varyHunger(user_id,vary_hunger) end
          if vary_thirst ~= 0 then vRP.varyThirst(user_id,vary_thirst) end

          if ftype == "drink" then
            TriggerClientEvent("pNotify:SendNotification", player, {
              text = "Bebendo " .. name,
              type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
              animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
            })
            play_drink(player)
          elseif ftype == "eat" then
            TriggerClientEvent("pNotify:SendNotification", player, {
              text = "Comendo " .. name,
              type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
              animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
            })
            play_eat(player)
          end

          vRP.closeMenu(player)
        end
      end
    end}

    return choices
  end

  return fgen
end

-- DRINKS --

items["agua"] = {"Água","", gen("drink",5,-25,0,10),0.5}
items["leite"] = {"Leite","", gen("drink",5,-5,0,10),0.5}
items["cafe"] = {"Café","", gen("drink",5,-10,25,10),0.2}
items["cha"] = {"Chá","", gen("drink",5,-15,0,10),0.2}
items["cha gelado"] = {"Chá Gelado","", gen("drink",5,-20,0,10), 0.5}
items["suco de laranja"] = {"Suco de Laranja","", gen("drink",5,-25,0,10),0.5}
items["coca cola"] = {"Coca Cola","", gen("drink",5,-35,0,10),0.3}
items["redbull"] = {"RedBull","", gen("drink",5,-400,25,10),0.3}
items["limonada"] = {"Limonada","", gen("drink",5,-450,0,10),0.3}
items["vodka"] = {"Vodka","", gen("drink",5,-65,10,10),0.5}

--FOOD

-- create Breed item
items["pao"] = {"Pão","", gen("eat",-10,10,0,10),0.5}
items["donut"] = {"Donut","", gen("eat",-15,10,0,10),0.2}
items["tacos"] = {"Tacos","", gen("eat",-20,10,0,10),0.2}
items["sanduiche"] = {"Sanduíche","", gen("eat",-25,10,5,10),0.5}
items["kebab"] = {"Kebab","", gen("eat",-45,10,0,10),0.85}
items["donut"] = {"Donut","", gen("eat",-25,10,0,10),0.5}

return items
