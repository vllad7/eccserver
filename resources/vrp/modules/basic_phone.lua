local lang = vRP.lang
local cfg = module("cfg/phone")
local htmlEntities = module("lib/htmlEntities")
local services = cfg.services
local announces = cfg.announces
local sanitizes = module("cfg/sanitizes")

function vRP.sendServiceAlert(sender, service_name,x,y,z,msg)
    local service = services[service_name]
    local answered = false
    if service then
        local players = {}
        for k,v in pairs(vRP.rusers) do
            local player = vRP.getUserSource(tonumber(k))
            if vRP.hasPermission(k,service.alert_permission) and player then
                table.insert(players,player)
            end
        end
        for k,v in pairs(players) do
            vRPclient._notify(v,service.alert_notify..msg)
            local bid = vRPclient.addBlip(v,x,y,z,service.blipid,service.blipcolor,"("..service_name..") "..msg)
            SetTimeout(service.alert_time*1000,function()
                vRPclient._removeBlip(v,bid)
            end)
            if sender ~= nil then
                async(function()
                    local ok = vRP.request(v,lang.phone.service.ask_call({service_name, htmlEntities.encode(msg)}), 30)
                    if ok then
                        if not answered then
                            vRPclient._notify(sender,service.answer_notify)
                            vRPclient._setGPS(v,x,y)
                            answered = true
                   	 	else
							TriggerClientEvent("pNotify:SendNotification", v, {
								text = "Este chamado já foi aceito",
								type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
								animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
							})
						end
                    end
                end)
            end
        end
    end
end

function vRP.sendSMS(user_id, phone, msg)
    if string.len(msg) > cfg.sms_size then
        sms = string.sub(msg,1,cfg.sms_size)
    end
    local identity = vRP.getUserIdentity(user_id)
    local dest_id = vRP.getUserByPhone(phone)
    if identity and dest_id then
        local dest_src = vRP.getUserSource(dest_id)
        if dest_src then
            local phone_sms = vRP.getPhoneSMS(dest_id)
            if #phone_sms >= cfg.sms_history then
                table.remove(phone_sms)
            end
            local from = vRP.getPhoneDirectoryName(dest_id, identity.phone).." ("..identity.phone..")"
            vRPclient._notify(dest_src,lang.phone.sms.notify({from, msg}))
            vRPclient._playAudioSource(dest_src, cfg.sms_sound, 0.5)
            table.insert(phone_sms,1,{identity.phone,msg})
            return true
        end
    end
end

function vRP.phoneCall(user_id, phone)
    local identity = vRP.getUserIdentity(user_id)
    local src = vRP.getUserSource(user_id)
    local dest_id = vRP.getUserByPhone(phone)
    if identity and dest_id then
        local dest_src = vRP.getUserSource(dest_id)
        if dest_src then
            local to = vRP.getPhoneDirectoryName(user_id, phone).." ("..phone..")"
            local from = vRP.getPhoneDirectoryName(dest_id, identity.phone).." ("..identity.phone..")"
            vRPclient._phoneHangUp(src)
            vRPclient._phoneCallWaiting(src, dest_src, true)
            vRPclient._notify(src,lang.phone.call.notify_to({to}))
            vRPclient._notify(dest_src,lang.phone.call.notify_from({from}))
            vRPclient._setAudioSource(src, "vRP:phone:dialing", cfg.dialing_sound, 0.5)
            vRPclient._setAudioSource(dest_src, "vRP:phone:dialing", cfg.ringing_sound, 0.5)
            local ok = false
            if vRP.request(dest_src, from .. " está te ligando", 30) then
                vRPclient._phoneHangUp(dest_src)
                vRPclient._connectVoice(dest_src, "phone", src)
                ok = true
            else
                TriggerClientEvent("pNotify:SendNotification", src, {
                    text = "Ligação recusada",
                    type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            vRPclient._phoneCallWaiting(src, dest_src, false)
            end
            vRPclient._removeAudioSource(src, "vRP:phone:dialing")
            vRPclient._removeAudioSource(dest_src, "vRP:phone:dialing")
            return ok
        end
    end
end

function vRP.sendSMSPos(user_id, phone, x,y,z)
    local identity = vRP.getUserIdentity(user_id)
    local dest_id = vRP.getUserByPhone(phone)
    if identity and dest_id then
        local dest_src = vRP.getUserSource(dest_id)
        if dest_src then
            local from = vRP.getPhoneDirectoryName(dest_id, identity.phone).." ("..identity.phone..")"
            vRPclient._playAudioSource(dest_src, cfg.sms_sound, 0.5)
            TriggerClientEvent("pNotify:SendNotification", dest_src, {
                text = "Minha localização é " .. from .. ".",
                type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
            })
            local bid = vRPclient.addBlip(dest_src,x,y,z,162,37,from)
            SetTimeout(cfg.smspos_duration*1000,function()
                vRPclient._removeBlip(dest_src,{bid})
            end)
            return true
        end
    end
end

function vRP.getPhoneDirectory(user_id)
    local data = vRP.getUserDataTable(user_id)
    if data then
        if data.phone_directory == nil then
            data.phone_directory = {}
        end
        return data.phone_directory
    else
        return {}
    end
end

function vRP.getPhoneDirectoryName(user_id, phone)
    local directory = vRP.getPhoneDirectory(user_id)
    for k,v in pairs(directory) do
        if v == phone then
            return k
        end
    end
    return "unknown"
end

function vRP.getPhoneSMS(user_id)
    local data = vRP.getUserTmpTable(user_id)
    if data then
        if data.phone_sms == nil then
            data.phone_sms = {}
        end
        return data.phone_sms
    else
        return {}
    end
end

local phone_menu = {name="Meu Telefone",css={header_color="rgba(255,128,0,0.99)"}}
local function ch_directory(player,choice)
    local user_id = vRP.getUserId(player)
    if user_id then
        local phone_directory = vRP.getPhoneDirectory(user_id)
        local menu = {name=choice,css={header_color="rgba(255,128,0,0.99)"}}
        local ch_add = function(player, choice)
            local phone = vRP.prompt(player,lang.phone.directory.add.prompt_number(),"")
            local name = vRP.prompt(player,lang.phone.directory.add.prompt_name(),"")
            name = sanitizeString(tostring(name),sanitizes.text[1],sanitizes.text[2])
            phone = sanitizeString(tostring(phone),sanitizes.text[1],sanitizes.text[2])
            if #name > 0 and #phone > 0 then
                phone_directory[name] = phone
                TriggerClientEvent("pNotify:SendNotification", player, {
                    text = "Contato adicionado",
                    type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            else
                TriggerClientEvent("pNotify:SendNotification", player, {
                    text = "Valor inválido",
                    type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end
        end
        local ch_entry = function(player, choice)
            local emenu = {name=choice,css={header_color="rgba(255,128,0,0.99)"}}
            local name = choice
            local phone = phone_directory[name] or ""
            local ch_remove = function(player, choice)
                phone_directory[name] = nil
                vRP.closeMenu(player)
            end
            local ch_sendsms = function(player, choice)
                local msg = vRP.prompt(player,"Qual mensagem deseja enviar?","")
                msg = sanitizeString(msg,sanitizes.text[1],sanitizes.text[2])
                if vRP.sendSMS(user_id, phone, msg) then
                    TriggerClientEvent("pNotify:SendNotification", player, {
                        text = "Mensagem de texto enviada ao N° " .. phone .. " ",
                        type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                        animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                    })
                else
                    TriggerClientEvent("pNotify:SendNotification", player, {
                        text = "SMS não enviado para o N° " .. phone .. " ",
                        type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                        animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                    })
                end
            end
            local ch_sendpos = function(player, choice)
                local x,y,z = vRPclient.getPosition(player)
                if vRP.sendSMSPos(user_id, phone, x,y,z) then
                    TriggerClientEvent("pNotify:SendNotification", player, {
                        text = "Mensagem de texto enviada ao N° " .. phone .. " ",
                        type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                        animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                    })
                else
                    TriggerClientEvent("pNotify:SendNotification", player, {
                        text = "SMS não enviado para o N° " .. phone .. " ",
                        type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                        animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                    })
                end
            end
            local ch_call = function(player, choice)
                if not vRP.phoneCall(user_id, phone) then
                    TriggerClientEvent("pNotify:SendNotification", player, {
                        text = "Chamada não completada para o N° " .. phone .. " ",
                        type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                        animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                    })
           	 	end
            end
            emenu["Ligar"] = {ch_call}
            emenu["Enviar SMS"] = {ch_sendsms}
            emenu["Enviar Posição"] = {ch_sendpos}
            emenu["Excluir Contato"] = {ch_remove}
            emenu.onclose = function() ch_directory(player,"Agenda Telefônica") end
            vRP.openMenu(player, emenu)
        end
        menu["Adicionar Contato"] = {ch_add}
        for k,v in pairs(phone_directory) do
            menu[k] = {ch_entry,v}
        end
        vRP.openMenu(player,menu)
    end
end

local function ch_sms(player, choice)
    local user_id = vRP.getUserId(player)
    if user_id then
        local phone_sms = vRP.getPhoneSMS(user_id)
        local menu = {name=choice,css={header_color="rgba(255,128,0,0.99)"}}
        for k,v in pairs(phone_sms) do
            local from = vRP.getPhoneDirectoryName(user_id, v[1]).." ("..v[1]..")"
            local phone = v[1]
            menu["#"..k.." "..from] = {function(player,choice)
                local msg = vRP.prompt(player,"Digite o texto que deseja enviar (max " .. cfg.sms_size .. " caracteres)","")
                msg = sanitizeString(msg,sanitizes.text[1],sanitizes.text[2])
                if vRP.sendSMS(user_id, phone, msg) then
                    TriggerClientEvent("pNotify:SendNotification", player, {
                        text = "mensagem de texto enviada ao N° " .. phone .. " ",
                        type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                        animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                    })
                else
                    TriggerClientEvent("pNotify:SendNotification", player, {
                        text = "mensagem de texto não enviada ao N° " .. phone .. ".",
                        type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                        animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                    })
                end
            end, lang.phone.sms.info({from,htmlEntities.encode(v[2])})}
        end
        menu.onclose = function(player) vRP.openMenu(player, phone_menu) end
        vRP.openMenu(player,menu)
    end
end

local service_menu = {name="Serviços",css={header_color="rgba(255,128,0,0.99)"}}
service_menu.onclose = function(player) vRP.openMenu(player, phone_menu) end
local function ch_service_alert(player,choice)
    local service = services[choice]
    if service then
        local x,y,z = vRPclient.getPosition(player)
        local msg = vRP.prompt(player,"Caso necessário, envie uma mensagem ao serviço","")
        msg = sanitizeString(msg,sanitizes.text[1],sanitizes.text[2])
        TriggerClientEvent("pNotify:SendNotification", player, {
            text = service.notify,
            type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
            animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
        })
        vRP.sendServiceAlert(player,choice,x,y,z,msg)
    end
end

for k,v in pairs(services) do
    service_menu[k] = {ch_service_alert}
end

local function ch_service(player, choice)
    vRP.openMenu(player,service_menu)
end

local announce_menu = {name="Anúncios",css={header_color="rgba(255,128,0,0.99)"}}

announce_menu.onclose = function(player) vRP.openMenu(player, phone_menu) end
local function ch_announce_alert(player,choice)
    local announce = announces[choice]
    local user_id = vRP.getUserId(player)
    if announce and user_id then
        if not announce.permission or vRP.hasPermission(user_id,announce.permission) then
            local msg = vRP.prompt(player,"Qual anuncio deseja fazer?","")
            msg = sanitizeString(msg,sanitizes.text[1],sanitizes.text[2])
            if string.len(msg) > 5 and string.len(msg) < 100 then
                if announce.price <= 0 or vRP.tryPayment(user_id, announce.price) then
                    TriggerClientEvent("pNotify:SendNotification", player, {
                        text = "Você pagou " ..announce.price .. " reais pelo anuncio",
                        type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                        animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                    })
                msg = htmlEntities.encode(msg)
                msg = string.gsub(msg, "\n", "<br />")
                local users = vRP.getUsers()
                for k,v in pairs(users) do
                    vRPclient._announce(v,announce.image,msg)
                end
                else
                    TriggerClientEvent("pNotify:SendNotification", player, {
                        text = "Valor insuficiente",
                        type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                        animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                    })
                end
            else
                TriggerClientEvent("pNotify:SendNotification", player, {
                    text = "Valor inválido",
                    type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end
        else
            TriggerClientEvent("pNotify:SendNotification", player, {
                text = "O minimo de caracteres é 5 e o máximo é 100",
                type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
            })
        end
    end
end

for k,v in pairs(announces) do
    announce_menu[k] = {ch_announce_alert,lang.phone.announce.item_desc({v.price,v.description or ""})}
end

local function ch_announce(player, choice)
    vRP.openMenu(player,announce_menu)
end

local function ch_hangup(player, choice)
    vRPclient._phoneHangUp(player)
end

phone_menu["Lista de Contatos"] = {ch_directory}
phone_menu["Histórico de Mensagens"] = {ch_sms}
phone_menu["Serviços"] = {ch_service}
phone_menu["Anúncios"] = {ch_announce}
phone_menu["Atender Ligação"] = {ch_hangup}

SetTimeout(10000, function()
    local menu = vRP.buildMenu("phone", {})
    for k,v in pairs(menu) do
        phone_menu[k] = v
    end
end)

vRP.registerMenuBuilder("main", function(add, data)
    local player = data.player
    local choices = {}
    choices["Meu Telefone"] = {function() vRP.openMenu(player,phone_menu) end}
    local user_id = vRP.getUserId(player)
    if user_id and vRP.hasPermission(user_id, "telefone.jogador") then
        add(choices)
    end
end)
