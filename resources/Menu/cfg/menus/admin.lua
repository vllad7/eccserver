Citizen.CreateThread(function()
    vRP.registerMenuBuilder("admin", function(add, data)
        Citizen.CreateThread(function()
            local user_id = vRP.getUserId(data.player)
            if user_id ~= nil then
                local choices = {}
     	  
     	        if vRP.hasPermission(user_id,"deletar.veiculo") then
                    choices["Deletar Veículo"] = deletar_veiculo
     	        end
     	  
     	        if vRP.hasPermission(user_id,"spawnar.veiculo") then
                    choices["Spawnar Veículo"] = spawnar_veiculo
     	        end
                 
                if vRP.hasPermission(user_id,"mostrar.jogadores") then
                    choices["Mostrar Jogadores"] = mostrar_jogadores
                end
            
                if vRP.hasPermission(user_id,"teleportar.marcacao") then
                    choices["Teleportar até Marcação"] = teleportar_marcacao
                end
            
                if vRP.hasPermission(user_id,"soltar.prisao") then
                    choices["Libertar Jogador"] = ch_unjail
                end
            
                if vRP.hasPermission(user_id,"colocar.obstaculos") then
                    choices["Colocar / Retirar Obstáculos"] = ch_spikes
                end
            
                if vRP.hasPermission(user_id,"congelar.jogador") then
                    choices["Congelar Jogador"] = ch_freeze
                end
                add(choices)
            end
        end)
    end)
end)