-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- VARIAVEIS LOCAIS DE CONEXÃO COM O VRP
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP", "porte")

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- AQUI COMEÇA A PUTARIA
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
RegisterServerEvent("checar:porte")
AddEventHandler("checar:porte", function(item)
    local source = source
    local user_id = vRP.getUserId(source)
    local porte = json.decode(vRP.getSData("zezao:porte"))
    if not porte then
        vRP.setSData("zezao:porte",json.encode(0))
    end
    if porte == 0 and item.name == "meu-porte" then
        TriggerClientEvent("pNotify:SendNotification", source, {
            text = "Você não possui um porte de armas",
            type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
            animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
        })
    end

    if porte == 1 and item.name == "meu-porte" then
        TriggerClientEvent("pNotify:SendNotification", source, {
            text = "Seu porte atual é de nivel 1",
            type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
            animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
        })
    end

    if porte == 2 and item.name == "meu-porte" then
        TriggerClientEvent("pNotify:SendNotification", source, {
            text = "Seu porte atual é de nivel 2",
            type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
            animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
        })
    end

    if porte == 3 and item.name == "meu-porte" then
        TriggerClientEvent("pNotify:SendNotification", source, {
            text = "Seu porte atual é de nivel 3",
            type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
            animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
        })
    end

    if porte == 4 and item.name == "meu-porte" then
        TriggerClientEvent("pNotify:SendNotification", source, {
            text = "Seu porte atual é de nivel 4",
            type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
            animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
        })
    end
end)

RegisterServerEvent("comprar:porte")
AddEventHandler("comprar:porte", function(item)
    local source = source
    local user_id = vRP.getUserId(source)
    local porte = json.decode(vRP.getSData("zezao:porte"))
    if porte == 0 then
        if item.name == "porte-intermediario" or item.name == "porte-avancado" or item.name == "porte-especial" then
            TriggerClientEvent("pNotify:SendNotification", source, {
                text = "Você deve comprar um porte de nível 1 para ter acesso aos outros",
                type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
            })
        else
            if vRP.tryPayment(user_id, 10000) then
                if item.name == "porte-basico" then
                    vRP.setSData("zezao:porte",json.encode(1))
                    TriggerClientEvent("pNotify:SendNotification", source, {
                        text = "Parabéns pela aquisição, agora você possui uma licença de nível 1",
                        type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                        animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                    })
                end
            else
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Você não possui dinheiro suficiente",
                    type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end
        end
    end

    if porte == 1 then
        if item.name == "porte-basico" then
            TriggerClientEvent("pNotify:SendNotification", source, {
                text = "Você já possui esta licença",
                type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
            })
        else
            if item.name == "porte-avancado" or item.name == "porte-especial" then
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Você deve comprar um porte de nível 2 para ter acesso aos outros",
                    type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            else
                if vRP.tryPayment(user_id, 30000) then
                    if item.name == "porte-intermediario" then
                        vRP.setSData("zezao:porte",json.encode(2))
                        TriggerClientEvent("pNotify:SendNotification", source, {
                            text = "Parabéns pela aquisição, agora você possui uma licença de nível 2",
                            type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                            animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                        })
                    end
                else
                    TriggerClientEvent("pNotify:SendNotification", source, {
                        text = "Você não possui dinheiro suficiente",
                        type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                        animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                    })
                end
            end
        end
    end

    if porte == 2 then
        if item.name == "porte-basico" or item.name == "porte-intermediario" then
            TriggerClientEvent("pNotify:SendNotification", source, {
                text = "Você já possui esta licença",
                type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
            })
        else
            if item.name == "porte-especial" then
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Você deve comprar um porte de nível 3 para ter acesso aos outros",
                    type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            else
                if vRP.tryPayment(user_id, 60000) then
                    if item.name == "porte-avancado" then
                        vRP.setSData("zezao:porte",json.encode(3))
                        TriggerClientEvent("pNotify:SendNotification", source, {
                            text = "Parabéns pela aquisição, agora você possui uma licença de nível 3",
                            type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                            animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                        })
                    end
                else
                    TriggerClientEvent("pNotify:SendNotification", source, {
                        text = "Você não possui dinheiro suficiente",
                        type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                        animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                    })
                end
            end
        end
    end

    if porte == 3 then
        if item.name == "porte-basico" or item.name == "porte-intermediario" or item.name == "porte-avancado" then
            TriggerClientEvent("pNotify:SendNotification", source, {
                text = "Você já possui esta licença",
                type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
            })
        else
            if vRP.tryPayment(user_id, 90000) then
                if item.name == "porte-especial" then
                    vRP.setSData("zezao:porte",json.encode(4))
                    TriggerClientEvent("pNotify:SendNotification", source, {
                        text = "Parabéns pela aquisição, agora você possui uma licença de nível 4",
                        type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                        animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                    })
                end
            else
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Você não possui dinheiro suficiente",
                    type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end
        end
    end

    if porte == 4 then
        if item.name == "porte-basico" or item.name == "porte-intermediario" or item.name == "porte-avancado" or item.name == "porte-especial" then
            TriggerClientEvent("pNotify:SendNotification", source, {
                text = "Você já possui esta licença",
                type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
            })
        end
    end
end)

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- FUNÇÃO QUE VERIFICA QUAL A LICENÇA DO JOGADOR NO BANCO
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
local verificar_licenca = {function(player,choice)
    local nplayer = vRPclient.getNearestPlayer(player,10)
    local nuser_id = vRP.getUserId(nplayer)
    if nuser_id then
        if vRP.request(nplayer,"Quer mostrar sua licença de armas?",15) then
            local porte = json.decode(vRP.getSData("zezao:porte"))
            if porte == 0 then
                vRPclient.notify(player,"Este jogador não possui porte de armas")
            end

            if porte == 1 then
                vRPclient.notify(player,"Este jogador possui porte de armas de nivel 1")
            end

            if porte == 2 then
                vRPclient.notify(player,"Este jogador possui porte de armas de nivel 2")
            end

            if porte == 3 then
                vRPclient.notify(player,"Este jogador possui porte de armas de nivel 3")
            end

            if porte == 4 then
                vRPclient.notify(player,"Este jogador possui porte de armas de nivel 4")
            end
        else
            vRPclient.notify(player,"Este jogador se recusou a mostrar seu porte")
        end
    else
        vRPclient.notify(player,"Nenhum jogador por perto")
    end
end}


Citizen.CreateThread(function()
    vRP.registerMenuBuilder("police", function(add, data)
        local user_id = vRP.getUserId(data.player)
        if user_id then
            local choices = {}
            if vRP.hasPermission(user_id,"Verificar.Porte") then
                choices["Verificar Porte"] = verificar_licenca
            end

            add(choices)
        end
    end)
end)