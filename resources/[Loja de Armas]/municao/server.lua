-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- VARIAVEIS LOCAIS DE CONEXÃO COM O VRP
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- EVENTOS SE COMUNICANDO COM O CLIENT
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
RegisterServerEvent("zezao:doisM")
AddEventHandler("zezao:doisM", function(item)
    local source = source
    local user_id = vRP.getUserId(source)
    local porte = json.decode(vRP.getSData("zezao:porte"))
    if porte >= 2 then
        if item.name == "municao-pistola" then
            if vRP.tryPayment(user_id, zezao.Mpistola) then
                vRP.giveInventoryItem(user_id, "wammo|WEAPON_PISTOL", 12, false)
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Você comprou um pente de pistola e pagou " .. zezao.Mpistola .. " reais",
                    type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            else
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Dinheiro Insuficiente",
                    type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end
        end

        if item.name == "municao-pistola-sns" then
            if vRP.tryPayment(user_id, zezao.Mpistolasns) then
                vRP.giveInventoryItem(user_id, "wammo|WEAPON_PISTOL", 6, false)
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Você comprou um pente de pisstola SNS e pagou " .. zezao.Mpistolasns .. " reais",
                    type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            else
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Dinheiro Insuficiente",
                    type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end
        end
    else
        TriggerClientEvent("pNotify:SendNotification", source, {
            text = "Seu porte atual é de nível " .. porte .. " e não permite acesso a esta area",
            type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
            animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
        })
    end
end)

RegisterServerEvent("zezao:tresM")
AddEventHandler("zezao:tresM", function(item)
    local source = source
    local user_id = vRP.getUserId(source)
    local porte = json.decode(vRP.getSData("zezao:porte"))
    if porte >= 3 then
        if item.name == "municao-12" then
            if vRP.tryPayment(user_id, zezao.Mdoze) then
                vRP.giveInventoryItem(user_id, "wammo|WEAPON_PUMPSHOTGUN", 8, false)
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Você comprou um pente de 12 e pagou " .. zezao.Mdoze .. " reais",
                    type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            else
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Dinheiro Insuficiente",
                    type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end
        end

        if item.name == "municao-rifle-compacto" then
            if vRP.tryPayment(user_id, zezao.Mriflecompacto) then
                vRP.giveInventoryItem(user_id, "wammo|WEAPON_COMPACTRIFLE", 30, false)
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Você comprou um pente de rifle compacto e pagou " .. zezao.Mriflecompacto .. " reais",
                    type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            else
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Dinheiro Insuficiente",
                    type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end
        end

        if item.name == "municao-smg" then
            if vRP.tryPayment(user_id, zezao.Msmg) then
                vRP.giveInventoryItem(user_id, "wammo|WEAPON_SMG", 30, false)
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Você comprou um pente de SMG e pagou " .. zezao.Msmg .. " reais",
                    type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            else
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Dinheiro Insuficiente",
                    type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end
        end
    else
        TriggerClientEvent("pNotify:SendNotification", source, {
            text = "Seu porte atual é de nível " .. porte .. " e não permite acesso a esta area",
            type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
            animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
        })
    end
end)

RegisterServerEvent("zezao:quatroM")
AddEventHandler("zezao:quatroM", function(item)
    local source = source
    local user_id = vRP.getUserId(source)
    local porte = json.decode(vRP.getSData("zezao:porte"))
    if porte >= 4 then
        if item.name == "municao-AK-47" then
            if vRP.tryPayment(user_id, zezao.Mak47) then
                vRP.giveInventoryItem(user_id, "wammo|WEAPON_ASSAULTRIFLE", 30, false)
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Você comprou um pente de AK-47 e pagou " .. zezao.Mak47 .. " reais",
                    type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            else
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Dinheiro Insuficiente",
                    type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end
        end

        if item.name == "municao-m4" then
            if vRP.tryPayment(user_id, zezao.Mm4) then
                vRP.giveInventoryItem(user_id, "wammo|WEAPON_CARBINERIFLE", 30, false)
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Você comprou um pente de rifle compacto e pagou " .. zezao.Mm4 .. " reais",
                    type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            else
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Dinheiro Insuficiente",
                    type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end
        end

        if item.name == "municao-sniper" then
            if vRP.tryPayment(user_id, zezao.Msniper) then
                vRP.giveInventoryItem(user_id, "wammo|WEAPON_HEAVYSNIPER", 10, false)
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Você comprou um pente de SMG e pagou " .. zezao.Msniper .. " reais",
                    type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            else
                TriggerClientEvent("pNotify:SendNotification", source, {
                    text = "Dinheiro Insuficiente",
                    type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end
        end
    else
        TriggerClientEvent("pNotify:SendNotification", source, {
            text = "Seu porte atual é de nível " .. porte .. " e não permite acesso a esta area",
            type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
            animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
        })
    end
end)
