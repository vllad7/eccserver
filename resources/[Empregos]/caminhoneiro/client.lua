local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP")
TJserver = Tunnel.getInterface("caminhoneiro")

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- BLIPS
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
local caminhoneiro = {
	{nome = "Caminhoneiro", id = 477, cor = 2, x = 917.46, y = -3238.43, z = 5.89},
}

Citizen.CreateThread(function()
	for _, item in pairs(caminhoneiro) do
      	item.blip = AddBlipForCoord(item.x, item.y, item.z)
      	SetBlipSprite(item.blip, item.id)
      	SetBlipColour(item.blip, item.cor)
      	SetBlipAsShortRange(item.blip, true)
      	BeginTextCommandSetBlipName("STRING")
      	AddTextComponentString(item.nome)
      	EndTextCommandSetBlipName(item.blip)
    end
end)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- TEXTO MISSÕES
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
RegisterNetEvent("mt:missiontext")
AddEventHandler("mt:missiontext", function(text, time)
        ClearPrints()
        SetTextEntry_2("STRING")
        AddTextComponentString(text)
        DrawSubtitleTimed(time, 1)
end)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- TEXTO 3D TRABALHO
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Citizen.CreateThread(function ()
	while true do
		Citizen.Wait(0)
		local source = source
		local Coordenadas = GetEntityCoords(GetPlayerPed(-1))			
		local Distancia = GetDistanceBetweenCoords(Coordenadas.x, Coordenadas.y, Coordenadas.z, 917.46,-3238.43,4.90, true)
		if Distancia < 10.0 then
			Opacidade = math.floor(255 - (Distancia * 20))
			Texto3D(917.46,-3238.43,4.90+1.5, "PRESSIONE ~y~[E] ~w~PARA TRABALHAR", Opacidade)
			DrawMarker(27,917.46,-3238.43,4.90, 0, 0, 0, 0, 0, 0, 1.5001, 1.5001, 0.6001,255,255,51, 200, 0, 0, 0, 0)
		end
	end
end)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- SPAWN DOS CAMINHÕES e CAMINHÕES | ADICIONE AQUI OS LUGARES QUE DESEJA QUE O CAMINHÃO SPAWNE
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
local Truck = {"HAULER", "PACKER", "PHANTOM"}

local Caminhoes = {    
	[0] = {896.515625,-3209.4768066406,5.9006600379944},
	[1] = {900.33807373047,-3209.7341308594,5.9006576538086},
	[2] = {904.64135742188,-3209.6059570313,5.9006628990173},
	[3] = {908.84924316406,-3209.8083496094,5.9006624221802},
	[4] = {912.83850097656,-3209.8349609375,5.9006624221802},
	[5] = {925.02655029297,-3187.11328125,5.9008007049561}
}
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- SPAWN DAS CARGAS e Cargas | ADICIONE AQUI OS LUGARES QUE DESEJA QUE A CARGA SPAWNE
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
local Cargas = {
	[0] = {901.17413330078,-3187.548828125,5.8968682289124},
	[1] = {919.04156494141,-3186.5439453125,5.9008054733276},
	[2] = {937.94885253906,-3189.6315917969,5.9008030891418},
	[3] = {1004.8592529297,-3184.8276367188,5.9008407592773},
	[4] = {934.02270507813,-3153.1806640625,5.9008083343506},
	[5] = {901.2294921875,-3151.6574707031,5.9008040428162},
	[6] = {937.51428222656,-3131.3200683594,5.9007978439331},
	[7] = {965.09466552734,-3131.2133789063,5.9008045196533},
	[8] = {1054.2518310547,-3184.8232421875,5.9011402130127}
}

local Trailer = {"TANKER", "TRAILERS", "TRAILERS2", "TRAILERLOGS"}

-- Militar: ArmyTanker
-- Feno: BaleTrailer
-- Porto (Docas): DockTrailer
-- Porto (Lancha): TR3
-- Concenssionária: TR4
-- Musical: TVTrailer
-- Posto de Combustivel: Tanker
-- Madeireira: TrailerLogs
-- Carga Roubada: Trailers
-- Carga Comercial: Trailers2

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- LOCAL DE ENTREGAS | ADICIONE AQUI OS LUGARES QUE DESEJA QUE ENTREGUEM AS CARGAS
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
local PontosdeEntrega = {
    -- x, y, z, pagamento
    [0] = {1212.4463, 2667.4351, 38.79, 500}, --x,y,z,money
    [1] = {-410.6452331543,1177.8696289063,325.64175415039, 700}, --x,y,z,money
    [2] = {2574.5144, 328.5554, 108.45, 1000},
    [3] = {-2565.0894, 2345.8904, 33.06, 1200},
    [4] = {1706.7966, 4943.9897, 42.16, 1400},
    [5] = {196.5617, 6631.0967, 31.53, 1700},
    [6] = {-567.66717529297,5356.8525390625,70.21849822998, 1900},
    [7] = {-1038.5166015625,4921.43359375,206.30116271973, 2100}
}

local MISSION = {}
MISSION.start = false
MISSION.tailer = false
MISSION.truck = false

MISSION.hashTruck = 0
MISSION.hashTrailer = 0

local currentMission = -1

local GUI = {}
GUI.loaded          = false
GUI.showStartText   = false
GUI.showMenu        = false
GUI.selected        = {}
GUI.menu            = -1

GUI.title           = {}
GUI.titleCount      = 0

GUI.desc            = {}
GUI.descCount       = 0

GUI.button          = {}
GUI.buttonCount     = 0

GUI.time            = 0

--text for mission
local text1 = false
local text2 = false

--blips
local BLIP = {}

BLIP.trailer = {}
BLIP.trailer.i = 0

BLIP.destination = {}
BLIP.destination.i = 0
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- DELETAR CARGA E CAMINHÃO APÓS A ENTREGA
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function ResetarMissao()    
    MISSION.start = false
    SetBlipRoute(BLIP.destination[BLIP.destination.i], false) 
    SetEntityAsNoLongerNeeded(BLIP.destination[BLIP.destination.i])
    
    if (DoesEntityExist(MISSION.trailer)) then
        SetEntityAsNoLongerNeeded(MISSION.trailer)
        
    end

    if (DoesEntityExist(MISSION.truck)) then
        SetEntityAsNoLongerNeeded(MISSION.truck)
    end

    SetEntityAsMissionEntity( GetVehiclePedIsIn(GetPlayerPed(-1),false), true, true )
    Citizen.InvokeNative( 0xEA386986E786A54F, Citizen.PointerValueIntInitialized(GetVehiclePedIsIn(GetPlayerPed(-1),false)))
    Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(MISSION.trailer))
    Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(MISSION.truck))
    currentMission = -1
end

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- MAIN THREAD
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)
        if ( type(BLIP.trailer[BLIP.trailer.i]) == "boolean" ) then
        elseif( BLIP.trailer[BLIP.trailer.i] == nil ) then
        else
            BLIP.trailer[BLIP.trailer.i] = BLIP.trailer[BLIP.trailer.i]
            BLIP.destination[BLIP.destination.i] = BLIP.destination[BLIP.destination.i]
        end

        --Exibe o menu quando o jogador está perto
        if( MISSION.start == false) then
            if( GetDistanceBetweenCoords( GetEntityCoords(GetPlayerPed(-1), 0), 917.46,-3238.43,5.89 ) < 2) then
                if(GUI.showStartText == false) then
                    GUI.drawStartText()
                    SetEntityAsMissionEntity( vehicle, true, true )
                end

                if(IsControlPressed(1,38) and GUI.showMenu == false) then
                    if TJserver.checkJOB() then
                        GUI.showMenu = true
                        GUI.menu = 0
                    else
                        TriggerEvent("chatMessage", 'Sistema', { 255,0,0}, 'Você não é um caminhoneiro.')
                    end
                end

                if(IsControlPressed(1, 177) and GUI.showMenu == true) then
                    GUI.showMenu = false
                end
            else
                GUI.showStartText = false
                GUI.showMenu = false
            end

            --menu
            if( GUI.loaded == false ) then
                GUI.init()
            end

            if( GUI.showMenu == true and GUI.menu ~= -1) then
                if( GUI.time == 0) then
                    GUI.time = GetGameTimer()
                end
                if( (GetGameTimer() - GUI.time) > 10) then
                    GUI.updateSelectionMenu(GUI.menu)
                    GUI.time = 0
                end
                GUI.renderMenu(GUI.menu)
            end
        elseif( MISSION.start == true ) then
            MISSION.markerUpdate(IsEntityAttached(MISSION.trailer))
            if( IsEntityAttached(MISSION.trailer) and text1 == false) then
                TriggerEvent("mt:missiontext", "Dirija ate o destino ~g~marcado~w~.", 10000)
                text1 = true
            elseif( not IsEntityAttached(MISSION.trailer) and text2 == false ) then
                TriggerEvent("mt:missiontext", "Anexe ao ~o~carga~w~.", 15000)
                text2 = true
            end
            Wait(2000)
            local trailerCoords = GetEntityCoords(MISSION.trailer, 0)
            if ( GetDistanceBetweenCoords(currentMission[1], currentMission[2], currentMission[3], trailerCoords ) < 25 and  not IsEntityAttached(MISSION.trailer)) then
                TriggerEvent("mt:missiontext", "Você recebeu ~g~" ..currentMission[4] .. " reais ~w~por esta entrega.", 15000)
                MISSION.removeMarker()
                MISSION.destroytrailer()
                TriggerServerEvent('truckerJob:success',currentMission[4])
                ResetarMissao()
            elseif ( GetDistanceBetweenCoords(currentMission[1], currentMission[2], currentMission[3], trailerCoords ) < 20 and IsEntityAttached(MISSION.trailer) ) then
                TriggerEvent("mt:missiontext", "Você chegou ao destino, pressione ~r~H ~w~para desacoplar", 15000)
            end

            if ( IsEntityDead(MISSION.trailer) or IsEntityDead(MISSION.truck) ) then
                MISSION.removeMarker()
                ResetarMissao()
            end
        end
    end
end)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- GUI
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function GUI.optionMisson(trailerN)
    MISSION.hashTrailer = GetHashKey(Trailer[trailerN + 1])
    RequestModel(MISSION.hashTrailer)
    while not HasModelLoaded(MISSION.hashTrailer) do
        Wait(1)
    end

    local randomTruck = GetRandomIntInRange(1, #Truck)
    MISSION.hashTruck = GetHashKey(Truck[randomTruck])
	RequestModel(MISSION.hashTruck)
    while not HasModelLoaded(MISSION.hashTruck) do
        Wait(1)
    end
end

function GUI.mission(missionN)
    BLIP.trailer.i = BLIP.trailer.i + 1
    BLIP.destination.i = BLIP.destination.i + 1
    currentMission = PontosdeEntrega[missionN]
    GUI.showMenu = false
    MISSION.start = true
    MISSION.spawnTruck()
    MISSION.spawnTrailer()
end

function MISSION.spawnTruck()
    local rand = math.random(#Caminhoes)
    MISSION.truck = CreateVehicle(MISSION.hashTruck, Caminhoes[rand][1], Caminhoes[rand][2], Caminhoes[rand][3], 0.0, true, false)
    SetVehicleOnGroundProperly(MISSION.trailer)
    SetVehicleNumberPlateText(MISSION.truck, "Caminhoneiro")
    SetVehicleEngineOn(MISSION.truck, true, false, false)
	SetVehicleOnGroundProperly(MISSION.truck)
end

function MISSION.spawnTrailer()
	local rand = math.random(#Cargas)
	MISSION.trailer = CreateVehicle(MISSION.hashTrailer, Cargas[rand][1], Cargas[rand][2], Cargas[rand][3], 0.0, true, false)
    SetVehicleOnGroundProperly(MISSION.trailer)
    MISSION.trailerMarker()
end

local oneTime = false

function MISSION.trailerMarker()
    BLIP.trailer[BLIP.trailer.i] = AddBlipForEntity(MISSION.trailer)
    SetBlipSprite(BLIP.trailer[BLIP.trailer.i], 1)
    SetBlipColour(BLIP.trailer[BLIP.trailer.i], 17)
    SetBlipRoute(BLIP.trailer[BLIP.trailer.i], false)
    Wait(50)
end

function MISSION.markerUpdate(trailerAttached)
    if( not BLIP.destination[BLIP.destination.i] and trailerAttached) then
       -- BLIP.destination.i = BLIP.destination.i + 1 this happens in GUI.mission()
        BLIP.destination[BLIP.destination.i]  = AddBlipForCoord(currentMission[1], currentMission[2], currentMission[3])
        SetBlipSprite(BLIP.destination[BLIP.destination.i], 1)
        SetBlipColour(BLIP.destination[BLIP.destination.i], 2)
        SetBlipRoute(BLIP.destination[BLIP.destination.i], true)
    end
    if( trailerAttached ) then
        SetBlipSprite(BLIP.trailer[BLIP.trailer.i], 2) --invisible
    elseif ( not trailerAttached and BLIP.trailer[BLIP.trailer.i]) then
        SetBlipSprite(BLIP.trailer[BLIP.trailer.i], 1) --visible
        SetBlipColour(BLIP.trailer[BLIP.trailer.i], 17)
    end
    Wait(50)
end

function MISSION.destroytrailer()
	SetEntityAsMissionEntity(MISSION.trailer, true, true)
	Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(MISSION.trailer))
	MISSION.trailer = nil
end

function MISSION.removeMarker()
    SetBlipSprite(BLIP.destination[BLIP.destination.i], 2)--invisible
    SetBlipSprite(BLIP.trailer[BLIP.trailer.i], 2) --invisible
end

function MISSION.getMoney()
   TriggerEvent("es:addedMoney", currentMission[4])
    TriggerEvent("truckerJob:getMoney", currentMission[4])
end
---------------------------------------
---------------------------------------
---------------------------------------
-----------------MENU------------------
---------------------------------------   
---------------------------------------  
---------------------------------------
function GUI.drawStartText()
end

function GUI.renderMenu(menu)
    GUI.renderTitle()
    GUI.renderButtons(menu)
end

function GUI.init()
    GUI.loaded = true
    GUI.addTitle("NOME DA EMPRESA AQUI", 0.5, 0.19, 0.3, 0.07 )
    GUI.addButton(0, "RON Tanker trailer", GUI.optionMisson, 0.5, 0.25, 0.3, 0.05 )
    GUI.addButton(0, "Container trailer", GUI.optionMisson, 0.5, 0.30, 0.3, 0.05 )
    GUI.addButton(0, "Articulated trailer", GUI.optionMisson, 0.5, 0.35, 0.3, 0.05 )
    GUI.addButton(0, "Log trailer", GUI.optionMisson, 0.5, 0.40, 0.3, 0.05 )
    GUI.addButton(0, "Fechar", GUI.exit, 0.5, 0.45, 0.3, 0.05 )
    
    GUI.buttonCount = 0
    GUI.addButton(1, "Entrega 1 [ R$ 100.00 ]", GUI.mission, 0.5, 0.25, 0.3, 0.05)
    GUI.addButton(1, "Entrega 2 [ R$ 150.00 ]", GUI.mission, 0.5, 0.30, 0.3, 0.05)
    GUI.addButton(1, "Entrega 3 [ R$ 200.00 ]", GUI.mission, 0.5, 0.35, 0.3, 0.05)
    GUI.addButton(1, "Entrega 4 [ R$ 250.00 ]", GUI.mission, 0.5, 0.40, 0.3, 0.05)
    GUI.addButton(1, "Entrega 5 [ R$ 300.00 ]", GUI.mission, 0.5, 0.45, 0.3, 0.05)
    GUI.addButton(1, "Entrega 6 [ R$ 350.00 ]", GUI.mission, 0.5, 0.50, 0.3, 0.05)
    GUI.addButton(1, "entrega 7 [ R$ 400.00 ]", GUI.mission, 0.5, 0.55, 0.3, 0.05)
    GUI.addButton(1, "Entrega 8 [ R$ 450.00 ]", GUI.mission, 0.5, 0.60, 0.3, 0.05)
    GUI.addButton(1, "Fechar", GUI.exit, 0.5, 0.65, 0.3, 0.05)
end

--Render stuff
function GUI.renderTitle()
    for id, settings in pairs(GUI.title) do
        local screen_w = 0
        local screen_h = 0
        screen_w, screen_h = GetScreenResolution(0,0)
        boxColor = {0,0,0,200}
		SetTextFont(4)
		SetTextScale(0.0, 0.7)
		SetTextColour(255, 255, 255, 255)
		SetTextCentre(true)
		SetTextDropshadow(0, 0, 0, 0, 0)
		SetTextEdge(0, 0, 0, 0, 0)
		SetTextEntry("STRING")
        AddTextComponentString(settings["name"])
        DrawText((settings["xpos"] + 0.001), (settings["ypos"] - 0.015))
        GUI.renderBox(
            settings["xpos"], settings["ypos"], settings["xscale"], settings["yscale"],
            boxColor[1], boxColor[2], boxColor[3], boxColor[4]
        )
    end
end

function GUI.renderDesc()
		for id, settings in pairs(GUI.desc) do
		local screen_w = 0
		local screen_h = 0
		screen_w, screen_h =  GetScreenResolution(0, 0)
		boxColor = {0,0,0,200}
		SetTextFont(6)
		SetTextScale(0.0, 0.6)
		SetTextColour(255, 255, 255, 255)
		SetTextDropShadow(0, 0, 0, 0, 0)
		SetTextEdge(0, 0, 0, 0, 0)
		SetTextEntry("STRING")
		DrawText((settings["xpos"] - 0.06), (settings["ypos"] - 0.13))
		AddTextComponentString(settings["name"])
		GUI.renderBox(
            settings["xpos"], settings["ypos"], settings["xscale"], settings["yscale"],
            boxColor[1], boxColor[2], boxColor[3], boxColor[4]
        )
		end
end

function GUI.renderButtons(menu)
	for id, settings in pairs(GUI.button[menu]) do
		local screen_w = 0
		local screen_h = 0
		screen_w, screen_h =  GetScreenResolution(0, 0)
		boxColor = {0,0,0,100}
        if(settings["active"]) then
			boxColor = {255,155,0,200}
		end
		SetTextFont(6)
		SetTextScale(0.0, 0.58)
		SetTextColour(255, 255, 255, 255)
		SetTextCentre(true)
		SetTextDropShadow(0, 0, 0, 0, 0)
		SetTextEdge(0, 0, 0, 0, 0)
		SetTextEntry("STRING")
		AddTextComponentString(settings["name"])
		DrawText((settings["xpos"] + 0.001), (settings["ypos"] - 0.015))
		--AddTextComponentString(settings["name"])
		GUI.renderBox(
            settings["xpos"], settings["ypos"], settings["xscale"],
            settings["yscale"], boxColor[1], boxColor[2], boxColor[3], boxColor[4]
        )
	 end     
end

function GUI.renderBox(xpos, ypos, xscale, yscale, color1, color2, color3, color4)
	DrawRect(xpos, ypos, xscale, yscale, color1, color2, color3, color4);
end

--adding stuff
function GUI.addTitle(name, xpos, ypos, xscale, yscale)
	GUI.title[GUI.titleCount] = {}
	GUI.title[GUI.titleCount]["name"] = name
	GUI.title[GUI.titleCount]["xpos"] = xpos
	GUI.title[GUI.titleCount]["ypos"] = ypos 	
	GUI.title[GUI.titleCount]["xscale"] = xscale
	GUI.title[GUI.titleCount]["yscale"] = yscale
end

function GUI.addButton(menu, name, func, xpos, ypos, xscale, yscale)
    if(not GUI.button[menu]) then
        GUI.button[menu] = {}
        GUI.selected[menu] = 0
    end
    GUI.button[menu][GUI.buttonCount] = {}
	GUI.button[menu][GUI.buttonCount]["name"] = name
	GUI.button[menu][GUI.buttonCount]["func"] = func
	GUI.button[menu][GUI.buttonCount]["xpos"] = xpos
	GUI.button[menu][GUI.buttonCount]["ypos"] = ypos 	
	GUI.button[menu][GUI.buttonCount]["xscale"] = xscale
	GUI.button[menu][GUI.buttonCount]["yscale"] = yscale
    GUI.button[menu][GUI.buttonCount]["active"] = 0
    GUI.buttonCount = GUI.buttonCount + 1
end

function GUI.null()
end

function GUI.exit()
    GUI.showMenu = false
end

function GUI.updateSelectionMenu(menu)
    if( IsControlPressed(0, 173) ) then
        if( GUI.selected[menu] < #GUI.button[menu] ) then
            GUI.selected[menu] = GUI.selected[menu] + 1
        end
    elseif( IsControlPressed(0, 27) ) then
        if( GUI.selected[menu] > 0 ) then
            GUI.selected[menu] = GUI.selected[menu] - 1 
        end
    elseif( IsControlPressed(0, 18) ) then
        if( type(GUI.button[menu][GUI.selected[menu]]["func"]) == "function" ) then
            --remember variable GUI.selected[menu]
            
            --call mission functions
            GUI.button[menu][GUI.selected[menu]]["func"](GUI.selected[menu])
            
            GUI.menu = 1
            GUI.selected[menu] = 0
            if( not GUI.menu ) then
                GUI.menu = -1
            end
            Wait(100)
            
            --GUI.button[menu][GUI.selected[menu]]["func"](GUI.selected[menu])
        else
        end
        GUI.time = 0
    end
    local i = 0
    for id, settings in ipairs(GUI.button[menu]) do
        GUI.button[menu][i]["active"] = false
        if( i == GUI.selected[menu] ) then
            GUI.button[menu][i]["active"] = true
        end
        i = i + 1
    end
end

function Texto3D(x,y,z, text, Opacidade)
    local onScreen,_x,_y=World3dToScreen2d(x,y,z)
    local px,py,pz=table.unpack(GetGameplayCamCoords())
    if onScreen then
        SetTextScale(0.54, 0.54)
        SetTextFont(4)
        SetTextProportional(1)
        SetTextColour(255, 255, 255, Opacidade)
        SetTextDropshadow(0, 0, 0, 0, Opacidade)
        SetTextEdge(2, 0, 0, 0, 150)
        SetTextDropShadow()
        SetTextOutline()
        SetTextEntry('STRING')
        SetTextCentre(1)
        AddTextComponentString(text)
        DrawText(_x,_y)
     end
end