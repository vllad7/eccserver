-- ###################################
--
--       CREDITOS: Sighmir and Shadow
--
-- ###################################

local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP")
HUDserver = Tunnel.getInterface("Barras")
vRPhud = {}
Tunnel.bindInterface("Barras",vRPhud)

fome = 0
sede = 0
dormir = 0
banheiro = 0

Citizen.CreateThread(function()
	while true do 
		Citizen.Wait(1)	
		drawRct(0.0149, 0.9677, 0.1408,0.028,20,20,20,255) -- FUNDO PRETO
		drawRct(0.0163, 0.97, 0.06880,0.01,188,188,188,80) -- VIDA	
		drawRct(0.0865, 0.97, 0.06795,0.01,188,188,188,80) -- COLETE
		drawRct(0.0165, 0.982, 0.0335,0.01,188,188,188,80) -- FOME
		drawRct(0.0510, 0.982, 0.0335,0.01,188,188,188,80) -- SEDE
		drawRct(0.0865, 0.982, 0.0335,0.01,188,188,188,80) -- SONO	
		drawRct(0.1210, 0.982, 0.0335,0.01,188,188,188,80) -- NECESSIDADES

		-- Vida
		local health = GetEntityHealth(GetPlayerPed(-1)) - 100			
		local varSet = 0.06880 * (health / 100)			
		drawRct(0.0163, 0.97, varSet,0.01,55,115,55,255)
		drawTxt(0.545, 1.4612, 1.0,1.0,0.18, "Vida", 255, 255, 255, 255)			
			
		-- Colete
		armor = GetPedArmour(GetPlayerPed(-1))
		if armor > 100.0 then armor = 100.0 end
		local varSet = 0.06795 * (armor / 100)			
		drawRct(0.0865, 0.97, varSet,0.01,75,75,255,250)
		drawTxt(0.61, 1.461, 1.0,1.0,0.18, "Colete", 255, 255, 255, 255)
		
		-- Fome
		if fome > 100.0 then fome = 100.0 end
		local varSet = 0.0335 * (fome / 100)			
		drawRct(0.0165, 0.982, varSet,0.01,217,217,25,250)
		drawTxt(0.525, 1.474, 1.0,1.0,0.18, "Fome", 255, 255, 255, 255)	
		
		
		-- Sede
		if sede > 100.0 then sede = 100.0 end
		local varSet = 0.0335 * (sede / 100)		
		drawRct(0.0510, 0.982, varSet,0.01,50,153,204,250)
		drawTxt(0.56, 1.474, 1.0,1.0,0.18, "Sede", 255, 255, 255, 255)	

		-- Sono
        if dormir > 100.0 then dormir = 100.0 end
        local varSet = 0.0335 * (dormir / 100)            
		drawRct(0.0865, 0.982, varSet,0.01,120,77,98,250)
		drawTxt(0.595, 1.474, 1.0,1.0,0.18, "Sono", 255, 255, 255, 255)	

        -- Necessidades
        if banheiro > 100.0 then banheiro = 100.0 end
        local varSet = 0.0335 * (banheiro / 100)            
		drawRct(0.1210, 0.982, varSet,0.01,184,115,51,250)
		drawTxt(0.625, 1.474, 1.0,1.0,0.18, "Banheiro", 255, 255, 255, 255)	
	
	end
end)

function vRPhud.setHunger(hunger)
    fome = hunger
end

function vRPhud.setThirst(thirst)
    sede = thirst
end

function vRPhud.setSono(sono)
    dormir = sono
end

function vRPhud.setNecessidades(necessidades)
    banheiro = necessidades
end

function drawRct(x,y,width,height,r,g,b,a)
	DrawRect(x + width/2, y + height/2, width, height, r, g, b, a)
end

function drawTxt(x,y ,width,height,scale, text, r,g,b,a, outline)
    SetTextFont(0)
    SetTextProportional(0)
    SetTextScale(scale, scale)
    SetTextColour(r, g, b, a)
    SetTextDropShadow(0, 0, 0, 0,255)
    SetTextEdge(1, 0, 0, 0, 255)
    SetTextDropShadow()
    SetTextEntry("STRING")
    AddTextComponentString(text)
    DrawText(x - width/2, y - height/2 + 0.005)
end