local options = {
    x = 0.8, 
    y = 0.080, 
    width = 0.2, 
    height = 0.04, 
    scale = 0.4, 
    font = 0, 
    color_r = 255, 
    color_g = 124, 
    color_b = 173, 
}

local notificationParam = 1 
local texto = 0

function DisplayHelpText(str)
    SetTextComponentFormat("STRING")
    AddTextComponentString(str)
    DisplayHelpTextFromStringLabel(0, 0, 1, -1)
end


function drawTxt(text, font, centre, x, y, scale, r, g, b, a)
    SetTextFont(font)
    SetTextProportional(0)
    SetTextScale(scale, scale)
    SetTextColour(r, g, b, a)
    SetTextDropShadow(0, 0, 0, 0, 255)
    SetTextEdge(1, 0, 0, 0, 255)
    SetTextDropShadow()
    SetTextOutline()
    SetTextCentre(centre)
    SetTextEntry("STRING")
    AddTextComponentString(text)
    DrawText(x, y)
end

function drawTxt2(x, y, width, height, scale, text, r, g, b, a, outline)
    SetTextFont(0)
    SetTextProportional(0)
    SetTextScale(scale, scale)
    SetTextColour(r, g, b, a)
    SetTextDropShadow(0, 0, 0, 0, 255)
    SetTextEdge(1, 0, 0, 0, 255)
    SetTextDropShadow()
    if(outline)then
        SetTextOutline()
    end
    SetTextEntry("STRING")
    AddTextComponentString(text)
    DrawText(x - width / 2, y - height / 2 + 0.005)
end

function Main()
    ClearMenu()
    if texto == 0 then
        texto = 1 
        Menu.addButton("Porte Básico", "level1", nil)
    end

    if texto == 1 then
        texto = 2
        Menu.addButton("Porte Intermediário", "level2", nil)
    end

    if texto == 2 then
        texto = 3
        Menu.addButton("Porte Avançado", "level3", nil)
    end

    if texto == 3 then
        texto = 0
        Menu.addButton("Porte Especial", "level4", nil)
    end
end

function level1()
    ClearMenu()
    Menu.previous = "Main"
    Menu.addButton("Canivete", "porteum", {name = "arma-canivete"})
    Menu.addButton("Facão", "porteum", {name = "arma-facao"})
    Menu.addButton("Martelo", "porteum", {name = "arma-martelo"})
    Menu.addButton("Pé de Cabra", "porteum", {name = "arma-pe-de-cabra"})
    Menu.addButton("Soco Inglês", "porteum", {name = "arma-soco-ingles"})
    Menu.addButton("Taco de Baseball", "porteum", {name = "arma-taco-de-baseball"})

end

function level2()
    ClearMenu()
    Menu.previous = "Main"
    Menu.addButton("Pistola", "portedois", {name = "arma-pistola"})
    Menu.addButton("Pistola SNS", "portedois", {name = "arma-pistola-sns"})
end

function level3() 
    ClearMenu()
    Menu.previous = "Main" 
    Menu.addButton("12", "portetres", {name = "arma-12"}) 
    Menu.addButton("Bomba Adesiva Bérica", "portetres", {name = "arma-bomba"})
    Menu.addButton("Rifle Compacto", "portetres", {name = "arma-rifle-compacto"})
    Menu.addButton("SMG", "portetres", {name = "arma-smg"})
end

function level4()
    ClearMenu()

    Menu.previous = "Main"
    Menu.addButton("AK-47", "portequatro", {name = "arma-ak-47"})
    Menu.addButton("M4", "portequatro", {name = "arma-m4"})
    Menu.addButton("Sniper", "portequatro", {name = "arma-sniper"})
    Menu.addButton("Colete a Prova de Balas", "portequatro", {name = "colete-balas"})
end 

function porteum(item)
    TriggerServerEvent("zezao:um", item)
end

function portedois(item)
    TriggerServerEvent("zezao:dois", item)
end

function portetres(item)
    TriggerServerEvent("zezao:tres", item)
end

function portequatro(item)
    TriggerServerEvent("zezao:quatro", item)
end

RegisterNetEvent("Zezao:ColeteZ")
AddEventHandler("Zezao:ColeteZ", function()
    SetPedArmour(GetPlayerPed(-1), 100)
end)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- MENU PARA COMPRAR
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
armas = {
    {name = "comprar Armas (Sul)", colour = 1, id = 110, x = 810.31, y = -2157.67, z = 28.64}, 
    {name = "Comprar Armas (Concessionária)", colour = 1, id = 110, x = 22.12, y = -1107.00, z = 28.85}, 
    {name = "Comprar Armas (Posto de Gasolina)", colour = 1, id = 110, x = 844.32, y = -1033.90, z = 27.29}, 
}

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(1)
        local pos = GetEntityCoords(GetPlayerPed(-1), true)
        for k, v in ipairs(armas) do
            if(Vdist(pos.x, pos.y, pos.z, v.x, v.y, v.z) < 15.0)then
                DrawMarker(27, v.x, v.y, v.z, 0, 0, 0, 0, 0, 0, 0.9, 0.9, 0.6, 255, 124, 173, 255, 0, 0, 0, 0)
                if(Vdist(pos.x, pos.y, pos.z, v.x, v.y, v.z) < 1.2)then
                    DisplayHelpText("Pressione ~INPUT_CONTEXT~ para comprar armas")
                    if IsControlJustReleased(1, 51) then
                        Main()
                        Menu.selection = 1
                        Menu.hidden = not Menu.hidden -- Hide/Show the menu
                    end
                    Menu.renderGUI(options) -- Draw menu on each tick if Menu.hidden = false
                elseif(Vdist(pos.x, pos.y, pos.z, v.x, v.y, v.z) > 1.0)then
                    Menu.hidden = true
                end
            end
        end
    end
end)
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--Help messages
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)
        if IsEntityDead(GetPlayerPed(-1)) then
            Menu.hidden = true
            Citizen.Wait(5000)
        end
    end
end)
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- MENSAGENS
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function tituloDIV(x, y, width, height, scale, text, r, g, b, a, outline)
    SetTextFont(1)
    SetTextProportional(0)
    SetTextScale(scale, scale)
    SetTextColour(r, g, b, a)
    SetTextDropShadow(0, 0, 0, 0, 255)
    SetTextEdge(1, 0, 0, 0, 255)
    SetTextDropShadow()
    if(outline)then
        SetTextOutline()
    end
    SetTextEntry("STRING")
    AddTextComponentString(text)
    DrawText(x - width / 2, y - height / 2 + 0.005)
end

function TextoDIV(x, y, width, height, scale, text, r, g, b, a, outline)
    SetTextFont(4)
    SetTextProportional(0)
    SetTextScale(scale, scale)
    SetTextColour(r, g, b, a)
    SetTextDropShadow(0, 0, 0, 0, 255)
    SetTextEdge(1, 0, 0, 0, 255)
    SetTextDropShadow()
    if(outline)then
        SetTextOutline()
    end
    SetTextEntry("STRING")
    AddTextComponentString(text)
    DrawText(x - width / 2, y - height / 2 + 0.005)
end

function DrawR(xMin, xMax, yMin, yMax, color1, color2, color3, color4)
    DrawRect(xMin, yMin, xMax, yMax, color1, color2, color3, color4); 
end

Citizen.CreateThread(function()
    while true do
        if not Menu.hidden then 
            tituloDIV(1.23, 0.632, 1.0, 1.0, 0.8, "Qual seu porte?", 255, 255, 255, 255, true)
            --[[
            if texto == 0 then
                DrawRect(0.80, 0.6, 0.3, 0.25, 0, 0, 0, 150) -- Preços
                DrawRect(0.80, 0.450, 0.3, 0.05, 255, 124, 173, 150) -- Titulo
                tituloDIV(1.195, 0.9425, 1.0, 1.05, 0.8, "Tabela de Preços (Armas)", 255, 255, 255, 255, true)
                TextoDIV(1.16, 0.97, 1.0, 1.0, 0.5, "Canivete: ~g~R$ ~r~210,00", 255, 255, 255, 255, true)
                TextoDIV(1.16, 1.00, 1.0, 1.0, 0.5, "Facão: ~g~R$ ~r~250,00", 255, 255, 255, 255, true)
                TextoDIV(1.16, 1.03, 1.0, 1.0, 0.5, "Machadinha: ~g~R$ ~r~260,00", 255, 255, 255, 255, true)
                TextoDIV(1.16, 1.06, 1.0, 1.0, 0.5, "Martelo: ~g~R$ ~r~160,00", 255, 255, 255, 255, true)
                TextoDIV(1.16, 1.09, 1.0, 1.0, 0.5, "Pé de Cabra: ~g~R$ ~r~240,00", 255, 255, 255, 255, true)
                TextoDIV(1.16, 1.12, 1.0, 1.0, 0.5, "Soco Inglês: ~g~R$ ~r~350,00", 255, 255, 255, 255, true)
                TextoDIV(1.16, 1.15, 1.0, 1.0, 0.5, "Taco de Baseball: ~g~R$ ~r~160,00", 255, 255, 255, 255, true)
                TextoDIV(1.16, 1.18, 1.0, 1.0, 0.5, "Pistola: ~g~R$ ~r~10.000,00", 255, 255, 255, 255, true)
                TextoDIV(1.29, 0.97, 1.0, 1.0, 0.5, "Pistola SNS: ~g~R$ ~r~7.200,00", 255, 255, 255, 255, true)
                TextoDIV(1.29, 1.00, 1.0, 1.0, 0.5, "12: ~g~R$ ~r~16.000,00", 255, 255, 255, 255, true)
                TextoDIV(1.29, 1.03, 1.0, 1.0, 0.5, "Bomba Adesiva Bérica: ~g~R$ ~r~40.000,00", 255, 255, 255, 255, true)
                TextoDIV(1.29, 1.06, 1.0, 1.0, 0.5, "Rifle Compacto: ~g~R$ ~r~35.000,00", 255, 255, 255, 255, true)
                TextoDIV(1.29, 1.09, 1.0, 1.0, 0.5, "SMG: ~g~R$ ~r~30.000,00", 255, 255, 255, 255, true) 
                TextoDIV(1.29, 1.12, 1.0, 1.0, 0.5, "AK-47: ~g~R$ ~r~50.000,00", 255, 255, 255, 255, true)
                TextoDIV(1.29, 1.15, 1.0, 1.0, 0.5, "M4: ~g~R$ ~r~60.000,00", 255, 255, 255, 255, true)
                --TextoDIV(1.23, 1.218, 1.0, 1.0, 0.5, "OBS: Valores sujeito a mudanças", 255, 255, 255, 255, true)
            end
            ]]
        end
        Citizen.Wait(0)
    end
end)
Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)
        if IsEntityDead(GetPlayerPed(-1)) then
            Menu.hidden = true
            Citizen.Wait(5000)
        end
    end
end)