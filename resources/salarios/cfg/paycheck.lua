local cfg = {}

cfg.salario = {
  ["salario.capitao"] = 1000,
  ["salario.sargento"] = 1000,
  ["salario.cadete"] = 1000,
  ["salario.samu"] = 1000
}

cfg.imposto = {
  ["contas.cidadao"] = 100
}

return cfg