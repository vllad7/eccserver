server_scripts {
	'@vrp/lib/utils.lua',
	'server.lua'
}

client_script {
	'client.lua',
	'@vrp/lib/utils.lua',
	'GUI.lua'
}